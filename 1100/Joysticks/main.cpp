#include <bits/stdc++.h> 

using namespace std;

int main()
{
    int a,b;

    cin >> a >> b;

    int minutes = 0;

    while(a != 0 && b != 0)
    {
        if(a < b)
        {
            a++;
            b-=2;
        }
        else
        {
            b++;
            a-=2;
        }
        if(a >= 0 && b >= 0)
            minutes++;
        else
            break;
    }
    cout << minutes << endl;
}
