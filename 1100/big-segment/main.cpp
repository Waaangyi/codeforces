#include <bits/stdc++.h>

using namespace std;

int main()
{
    int sc;
    cin >> sc;
    vector<int> x;
    vector<int> y;
    for(int i = 0; i < sc; i++)
    {
        int a,b;
        cin >> a >> b;
        x.push_back(a);
        y.push_back(b);

    }
    int ma = *max_element(begin(y), end(y));
    int mi = *min_element(begin(x), end(x));
    for(int i = 0; i < sc; i++)
    {
        if(x[i] <= mi && y[i] >= ma)
        {
            cout << i + 1 << endl;
            return 0;
        }

    }
    cout << -1 << endl;
}
