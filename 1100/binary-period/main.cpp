#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    while(tc--)
    {
        string t;
        cin >> t;

        int zeros = 0, ones = 0;

        for(int i = 0; i < t.length(); i++)
        {
            if(t[i] == '0')
                zeros++;
            else
                ones++;
        }
        if(zeros == 0 || ones == 0)
        {
            cout << t << endl;
        }
        else
        {
            string ans = "10";
            if(t[0] == '0')
                ans = "01";

            for(int i = 0; i < t.length(); i++)
                cout << ans;
            cout << endl;
        }
    }
}
