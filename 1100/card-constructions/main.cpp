#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while(tc--)
    {
        int ncard;
        cin >> ncard;

        int level = 0;
        int num = 0;
        while(true)
        {
            if(ncard - (level + 1) * 2 - (level) >= 0)
            {
                ncard = ncard - (level + 1) * 2 - level;
                level++;
            }
            else
            {
                if(level != 0)
                    num++;
                level = 0;
                if(ncard < 2)
                    break;

            }        
        }
        cout << num << endl;
    }
}
