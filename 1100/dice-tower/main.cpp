#include <bits/stdc++.h>

using namespace std;

int main()
{
    int num;
    cin >> num;

    int top;
    cin >> top;
    int bottom = 7-top;
    for(int i = 0; i < num; i++)
    {
        int a,b;
        cin >> a >> b;
        vector<int> faces;
        faces.push_back(a);
        faces.push_back(b);
        faces.push_back(7 - a);
        faces.push_back(7 - b);

        if(find(faces.begin(), faces.end(), bottom) == faces.end())
        {
            bottom = 7 - bottom;
        }
        else
        {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;
}
