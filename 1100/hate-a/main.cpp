#include <bits/stdc++.h>

using namespace std;

int main()
{
    string originial;
    cin >> originial;

    string removed = "";
    for(int i = 0; i < originial.length(); i++)
    {
        if(originial[i] != 'a')
            removed += originial[i];
    }
    if(removed.length() % 2 != 0)
    {
        cout << ":(" << endl;
        return 0;
    }
    int var = 0;
    for(int i = originial.length() - removed.length() / 2; i < originial.length(); i++)
    {
        if(removed[var] != originial[i])
        {
            cout << ":(" << endl;
            return 0;
        }
        var++;
    }
    for(int i = 0; i < originial.length() - removed.length() / 2; i++)
        cout << originial[i];
    cout << endl;
}
