#include <bits/stdc++.h>

using namespace std;

int main()
{
    int a,b,c;
    cin >> a >> b >> c;

    if(c == 0)
    {
        if(a == b)
        cout << "YES" << endl;
        else
            cout << "NO" << endl;
        return 0;
    }
    if(b - a < 0 && c > 0 || b - a > 0 && c < 0)
    {
        cout << "NO" << endl;
        return 0;
    }
    if((b - a) % c == 0)
        cout << "YES" << endl;
    else
        cout << "NO" << endl;
}
