#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    vector<int> p;
    vector<int> q;
    while(tc--)
    {
        int a, b;
        cin >> a >> b;
        p.push_back(a);
        q.push_back(b);
    }
    bool happy = false;
    for(int a = 0; a < q.size(); a++)
        if(q[a] < p[a])
            happy = true;
    if(happy)
        cout << "Happy Alex" << endl;
    else
        cout << "Poor Alex" << endl;
}
