#include <bits/stdc++.h>

using namespace std;

int main()
{
    long long l1, r1, l2, r2, k;
    cin >> l1 >> r1 >> l2 >> r2 >> k;

    long long wake = max(l1, l2);

    long long sleep = min(r1, r2);

    long long minute = sleep - wake;

    if(sleep < k || wake > k)
        minute++;
    if(minute < 0)
        minute = 0;
    cout << minute << endl;
}
