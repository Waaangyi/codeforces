#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    while(tc--)
    {
        int p, m;
        cin >> p >> m;
        vector<long long> moneyar;
        for(int i = 0; i < p; i++)
        {
            int a;
            cin >> a;
            moneyar.push_back(a);
        }
        sort(moneyar.rbegin(), moneyar.rend());
        for(int i = 0; i < p - 1; i++)
        {
            if(moneyar[i] > m)
            {
                moneyar[i + 1] += moneyar[i] - m;
            }
        }
        int richnum = 0;
        for(int i = 0; i < p; i++)
        {
            if(moneyar[i] >= m)
                richnum++;
        }
        cout << richnum << endl;
    }
}
