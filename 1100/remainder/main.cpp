#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n,x,y;
    cin >> n >> x >> y;

    string num;
    cin >> num;
    int actions = 0;
    for(int i = n - x; i < num.length(); i++)
    {
        if(i == n - y - 1)
        {
            if(num[i] == '0')
                actions++;
        }
        else
        {
            if(num[i] == '1')
                actions++;
        }
    }
    cout << actions << endl;
}
