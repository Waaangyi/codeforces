#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    while(tc--)
    {
        long long a,b;
        cin >> a >> b;
        
        long long area = a * a - b * b;

        bool ok = true;
        for(long long i = 2; i <= sqrt(a + b); i++)
        {
            if((a + b) % i == 0)
            {
                ok = false;
                break;
            }
        }
        if(ok && a - b == 1)
        cout << "YES" << endl;
        else
            cout << "NO" << endl;
        
    }
}
