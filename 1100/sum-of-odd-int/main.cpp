#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int tc;
    cin >> tc;

    while(tc--)
    {
        long long a,b;
        cin >> a >> b;
        if(a % 2 == b % 2 && b * b <= a)
            cout << "YES\n"; 
        else 
            cout << "NO\n";
    }
}
