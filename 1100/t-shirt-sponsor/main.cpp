#include <bits/stdc++.h>

using namespace std;

int main()
{
    vector<int> shirts;
    for(int i = 0; i < 5; i++)
    {
        int in;
        cin >> in;
        shirts.push_back(in);
    }
    int people;
    cin >> people;
    vector<string> size_num= {"S", "M", "L", "XL", "XXL"};

    int psize;
    for(int i = 0; i < people; i++)
    {
        string tsize;
        cin >> tsize;
        for(int a = 0; a < 5; a++)
        {
            if(tsize == size_num[a])
            {
                psize=a;
            }
        }

        int change = 1;
        if(shirts[psize] != 0)
        {
            cout << size_num[psize] << endl;
            shirts[psize]--;
            continue;
        }
        else
        {
            while(true)
            {
                if(psize + change < shirts.size())
                {
                    if(shirts[psize + change] != 0)
                    {
                        cout << size_num[psize + change] << endl;
                        shirts[psize + change]--;
                        break;
                    }
                }
                if(psize - change >= 0)
                {
                    if(shirts[psize - change] != 0)
                    {
                        cout << size_num[psize - change] << endl;
                        shirts[psize - change]--;
                        break;
                    }
                }
                change++;
                
            }
        }
    }

}
