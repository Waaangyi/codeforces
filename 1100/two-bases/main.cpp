#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, xbase;
    cin >> n >> xbase;
    vector<int> x;
    for(int i = 0; i < n; i++)
    {
        int a;
        cin >> a;
        x.push_back(a);
    }
    
    int ybase;
    cin >> n >> ybase;
    vector<int> y;
    for(int i = 0; i < n; i++)
    {
        int a; 
        cin >> a;
        y.push_back(a);
    }

    long long xsum = 0;
    for(int i = 0; i < x.size(); i++)
    {
        xsum += x[x.size() - i - 1] * pow(xbase, i);
    }

    long long ysum = 0;
    for(int i = 0; i < y.size(); i++)
    {
        ysum += y[y.size() - i - 1] * pow(ybase, i);
    }
    if(xsum == ysum)
        cout << "=" << endl;

    if(xsum > ysum)
        cout << ">" << endl;
    if(xsum < ysum)
        cout << "<" << endl;
}
