#include <bits/stdc++.h>

using namespace std;

int main()
{
    string line;
    cin >> line;

    if(line.length() == 1)
    {
        cout << 0 << endl;
        return 0;
    }
    vector<int> usedpos;
    int vkcount = 0;
    for(int i = 0; i < line.length() - 1; i++)
        if(line[i] == 'V' && line[i + 1] == 'K')
        {
            vkcount++;
            usedpos.push_back(i);
            usedpos.push_back(i + 1);
        }
    for(int i = 0; i < line.length() - 1; i++)
        if(line[i] == 'V' && line[i + 1] != 'K' && !count(usedpos.begin(), usedpos.end(), i) && !count(usedpos.begin(), usedpos.end(), i + 1))
        {
            cout << vkcount + 1 << endl;
            return 0;
        }
    for(int i = 0; i < line.length() - 1; i++)
        if(line[i+1] == 'K' && line[i] != 'V' && !count(usedpos.begin(), usedpos.end(), i) && !count(usedpos.begin(), usedpos.end(), i + 1))
        {
            cout << vkcount + 1 << endl;
            return 0;
        }
    cout << vkcount << endl;

}
