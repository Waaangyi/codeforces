#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int len, subnum;
        cin >> len >> subnum;

        vector<int> numlist(len);
        for (int i = 0; i < len; i++)
            cin >> numlist[i];
    }
}