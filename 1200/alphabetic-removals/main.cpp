#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len, k;
    cin >> len >> k;

    string s;
    cin >> s;
    vector<int> alpha(26);
    for (int i = 0; i < len; i++)
    {
        alpha[int(s[i]) - 97]++;
    }
    for (int i = 0; i < 26; i++)
    {
        if (alpha[i] <= k)
        {
            replace(s.begin(), s.end(), char(i + 97), ' ');
            k -= alpha[i];
            if (k == 0)
                break;
        }
        else
        {
            int replaced = 0;
            for (int j = 0; j < len; j++)
            {
                if (s[j] == char(i + 97))
                {
                    s[j] = ' ';
                    replaced++;
                }
                if (replaced == k)
                    break;
            }
            if (replaced == k)
                break;
            else
                k -= replaced;
        }
    }
    s.erase(remove(s.begin(), s.end(), ' '), s.end());
    cout << s << endl;
}