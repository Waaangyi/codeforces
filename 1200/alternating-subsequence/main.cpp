#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while (tc--)
    {
        int len;
        cin >> len;

        vector<long long> numlist(len);
        for (long long i = 0; i < len; i++)
        {
            cin >> numlist[i];
        }
        long long lastflipped = 0;
        bool negative = numlist[0] < 0;
        long long largestsub = 0;
        vector<long long> largest_subsequence;
        for (long long i = 0; i < len; i++)
        {
            if (numlist[i] > 0 && negative || numlist[i] < 0 && !negative)
            {
                if (negative == true)
                    negative = false;
                else
                    negative = true;
                //cout << lastflipped << " " << i << endl;
                largestsub = *max_element(numlist.begin() + lastflipped, numlist.begin() + i);

                largest_subsequence.push_back(largestsub);
                lastflipped = i;
            }
        }
        largestsub = *max_element(numlist.begin() + lastflipped, numlist.end());

        largest_subsequence.push_back(largestsub);

        cout << accumulate(largest_subsequence.begin(), largest_subsequence.end(), (long long)0) << endl;
    }
}