#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
    {
        cin >> numlist[i];
    }
    int answer = 1;
    sort(numlist.begin(), numlist.end());
    for (int i = 0; i < len; i++)
    {
        if (numlist[i] >= answer)
            answer++;
    }
    cout << answer << endl;
}