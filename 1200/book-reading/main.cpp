#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        long long n, m;
        cin >> n >> m;
        int digit = 0;
        digit = m % 10;
        vector<int> repeat;
        if (digit != 0)
        {
            int temp = digit;
            for (int i = 0; i < 10; i++)
            {
                repeat.push_back(((i + 1) * m) % 10);
            }
            long long sum = n / m / 10 * accumulate(repeat.begin(), repeat.end(), 0LL);
            for (int i = 0; i < (n / m) % 10; i++)
            {
                sum += repeat[i];
            }
            cout << sum << endl;
        }
        else
        {
            cout << 0 << endl;
        }
    }
}