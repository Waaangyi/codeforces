#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, s;
    cin >> n >> s;
    vector<pair<int, int>> price(n);

    for (int i = 0; i < n; i++)
    {
        cin >> price[i].first >> price[i].second;
    }
    int answer = 100;
    bool flag = 0;
    for (int i = 0; i < n; i++)
    {
        if (price[i].first < s || price[i].first == s && price[i].second == 0)
            flag = 1;
        if (price[i].first < s && price[i].second)
            answer = min(answer, price[i].second);
    }
    if (!flag)
        cout << -1 << endl;
    else
        cout << 100 - answer << endl;
}