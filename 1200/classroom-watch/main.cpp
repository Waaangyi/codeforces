#include <bits/stdc++.h>

using namespace std;
long long sumdigits(long long number)
{
    long long sum = 0;
    while (number > 0)
    {
        sum += number % 10;
        number /= 10;
    }
    return sum;
}
int main()
{
    long long n;

    cin >> n;
    vector<long long> answer;
    for (long long i = n; i >= n - 100; i--)
    {
        if (i + sumdigits(i) == n)
        {
            answer.push_back(i);
        }
        if (i <= 0)
            break;
    }
    cout << answer.size() << endl;
    sort(answer.begin(), answer.end());
    for (long long i = 0; i < answer.size(); i++)
    {
        cout << answer[i] << " " << endl;
    }
}