#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    multiset<string> list;
    for (int i = 0; i < len; i++)
    {
        string a;
        cin >> a;
        list.insert(a);
    }
    for (int i = 0; i < len; i++)
    {
        string a;
        cin >> a;
        auto l = list.find(a);
        if (l != list.end())
            list.erase(l);
    }
    cout << list.size() << endl;
}