#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, k;
    cin >> n >> k;
    vector<pair<int, int>> price(n);
    int totalprice = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> price[i].first;
        totalprice += price[i].first;
    }
    vector<int> difference(n);
    for (int i = 0; i < n; i++)
    {
        cin >> price[i].second;
        difference[i] = price[i].first - price[i].second;
    }
    vector<int> unsorted_difference = difference;
    sort(difference.rbegin(), difference.rend());
    int buy = 0;
    int breaklocation = n - 1;
    for (int i = 0; i < n; i++)
    {
        if (difference[n - i - 1] <= 0)
        {
            buy++;
        }
        else
        {
            breaklocation = n - i - 1;
            break;
        }
    }
    if (buy < k)
    {
        buy = k;
    }
    for (int i = 0; i < n - buy; i++)
    {
        totalprice -= difference[i];
    }
    cout << totalprice << endl;
}