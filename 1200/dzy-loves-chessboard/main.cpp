#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, m;
    cin >> n >> m;

    vector<string> board;
    for (int i = 0; i < n; i++)
    {
        string line;
        cin >> line;
        board.push_back(line);
    }
    for (int x = 0; x < n; x++)
    {
        for (int y = 0; y < m; y++)
        {
            if (board[x][y] == '.')
            {
                if ((x + y) & 1)
                {
                    cout << "W";
                }
                else
                {
                    cout << "B";
                }
            }
            else
            {
                cout << "-";
            }
        }
        cout << endl;
    }
}