#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len, time;
    cin >> len >> time;

    vector<int> tickets(len);
    vector<int> timerequired(len);
    cin >> tickets[0];
    timerequired[0] = tickets[0];
    for (int i = 1; i < len; i++)
    {
        cin >> tickets[i];
        timerequired[i] = timerequired[i - 1] + tickets[i];
    }
    for (int i = 0; i < len; i++)
    {
        int timeleft = time;
        int requiredtime = timerequired[i];
        if (requiredtime <= timeleft)
        {
            cout << 0 << endl;
        }
        else
        {
            vector<int> subarray(tickets.begin(), tickets.begin() + i);
            sort(subarray.rbegin(), subarray.rend());
            int counter = 0;
            for (int j = 0; j < i; j++)
            {
                requiredtime -= subarray[j];
                counter++;
                if (requiredtime <= timeleft)
                {
                    cout << counter << endl;
                    break;
                }
            }
        }
    }
}