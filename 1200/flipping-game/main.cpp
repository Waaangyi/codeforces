#include <bits/stdc++.h>

using namespace std;

int main()
{
    int len;
    cin >> len;
    int c = 0, x = 0, r = -1, m = 0;
    for (int i = 0; i < len; i++)
    {
        int num;
        cin >> num;
        c += num;
        x += 1 - num * 2;
        r = max(r, x - m);
        m = min(x, m);
    }
    cout << r + c << endl;
}
