#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    vector<pair<int, int>> numlist(len);
    vector<pair<int, int>> colors(100005);
    for (int i = 0; i < len; i++)
    {
        cin >> numlist[i].first >> numlist[i].second;
        colors[numlist[i].first - 1].first++;
        colors[numlist[i].second - 1].second++;
    }
    for (int i = 0; i < len; i++)
    {
        cout << len - 1 + colors[numlist[i].second - 1].first << " " << (len - 1) * 2 - (len - 1 + colors[numlist[i].second - 1].first) << endl;
    }
}