#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int min, prepared;
    cin >> min >> prepared;

    vector<int> mindiff(min);
    for (int i = 0; i < min; i++)
    {
        cin >> mindiff[i];
    }

    vector<int> prediff(prepared);
    for (int i = 0; i < prepared; i++)
    {
        cin >> prediff[i];
    }
    int additional = 0;
    for (int i = 0; i < min; i++)
    {
        int found = false;
        for (int j = 0; j < prepared; j++)
        {
            if (mindiff[i] <= prediff[j])
            {
                found = true;
                prediff[j] = 0;
                break;
            }
        }
        if (!found)
        {
            additional++;
        }
    }
    if (prepared + additional < min)
    {
        cout << min - prepared << endl;
        return 0;
    }
    cout << additional << endl;
}