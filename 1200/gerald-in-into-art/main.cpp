#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int a1, b1, a2, b2, a3, b3;
    cin >> a1 >> b1 >> a2 >> b2 >> a3 >> b3;

    if ((a2 * b2 + a3 * b3) > a1 * b1)
    {
        cout << "NO\n";
        return 0;
    }
    if (b2 <= a1 && a2 <= b1)
        if ((a3 <= a1 && b3 <= b1 - a2) || (a3 <= b1 - a2 && b3 <= a1) || (a3 <= b1 && b3 <= a1 - b2) || (b3 <= b1 && a3 <= a1 - b2))
        {
            cout << "YES\n";
            return 0;
        }
    if (a2 <= a1 && b2 <= b1)
        if ((a3 <= a1 && b3 <= b1 - b2) || (a3 <= b1 - b2 && b3 <= a1) || (a3 <= a1 - a2 && b3 <= b1) || (a3 <= b1 && b3 <= a1 - a2))
        {
            cout << "YES\n";
            return 0;
        }

    cout << "NO" << endl;
}