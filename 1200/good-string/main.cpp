#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;
    while (tc--)
    {
        int len;
        cin >> len;
        string line;
        cin >> line;
        if (line[0] == '>' && line[len - 1] == '<')
        {
            cout << 0 << endl;
        }
        else
        {
            int answer = len - 1;
            for (int i = 0; i < len; i++)
            {
                if (line[i] == '>' || line[len - i - 1] == '<')
                {
                    answer = min(answer, i);
                }
            }
            cout << answer << endl;
        }
    }
}