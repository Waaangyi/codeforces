#include <bits/stdc++.h>

using namespace std;
int binaryToDecimal(int x)
{
    int ans = 0, p = 1;
    while (x > 0)
    {
        ans += (x % 2) * p;
        x /= 2;
        p *= 10;
    }
    return ans;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int ans = 0;
    int i = 1;
    while (binaryToDecimal(i) <= n)
    {
        ans++;
        i++;
    }
    cout << ans;
}