#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll l, r, x, y, k;
    cin >> l >> r >> x >> y >> k;

    for (ll i = x; i <= y; i++)
    {
        if (k * i >= l && k * i <= r)
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
}