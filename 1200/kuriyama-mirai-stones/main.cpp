#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int stonesct;
    cin >> stonesct;

    vector<long long> price(stonesct);

    for (int i = 0; i < stonesct; i++)
        cin >> price[i];

    int questionum;
    cin >> questionum;

    vector<long long> ascending_price = price;
    sort(ascending_price.begin(), ascending_price.end());
    vector<long long> osum(stonesct + 1), asum(stonesct + 1);
    for (int i = 1; i <= stonesct; i++)
    {
        osum[i] = price[i - 1] + osum[i - 1];
        asum[i] = ascending_price[i - 1] + asum[i - 1];
    }
    for (int i = 0; i < questionum; i++)
    {
        long long type, l, r;
        cin >> type >> l >> r;
        if (type == 1)
        {
            cout << osum[r] - osum[l - 1] << endl;
        }
        else
        {
            cout << asum[r] - asum[l - 1] << endl;
        }
    }
}