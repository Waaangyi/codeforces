#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> alist(n - 1);
    vector<int> blist(n - 1);
    vector<int> mainlist(n);
    for (int i = 0; i < n - 1; i++)
    {
        cin >> alist[i];
    }
    for (int i = 0; i < n - 1; i++)
    {
        cin >> blist[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> mainlist[i];
    }
    vector<int> routes;
    for (int i = n - 1; i >= 0; i--)
    {
        int cnt = 0;
        for (int j = n - 2; j >= i; j--)
        {
            cnt += blist[j];
        }
        cnt += mainlist[i];
        for (int j = i - 1; j >= 0; j--)
        {
            cnt += alist[j];
        }
        routes.push_back(cnt);
    }
    sort(routes.begin(), routes.end());
    cout << routes[0] + routes[1] << endl;
}