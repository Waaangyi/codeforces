#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    bool started = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (int(s[i]) > 97)
        {
            s[i]--;
            started = 1;
        }
        else
        {
            if (started)
                break;
        }
    }
    if (!started)
    {
        if (int(s[s.length() - 1]) > 97)
            s[s.length() - 1]--;
        else
            s[s.length() - 1] = 122;
    }
    cout << s << endl;
}