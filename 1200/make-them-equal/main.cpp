#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;

    vector<int> numlist(len);
    for (int i = 0; i < len; i++)
        cin >> numlist[i];
    sort(numlist.begin(), numlist.end());
    vector<int> uniquelist;
    for (int i = 0; i < len - 1; i++)
    {
        if (numlist[i] != numlist[i + 1])
        {
            uniquelist.push_back(numlist[i]);
            if (i == len - 2)
                uniquelist.push_back(numlist[i + 1]);
        }
    }
    if (uniquelist.size() == 0)
    {
        cout << 0 << endl;
        return 0;
    }
    if (numlist[len - 1] != uniquelist[uniquelist.size() - 1])
        uniquelist.push_back(numlist[len - 1]);
    if (uniquelist.size() > 3)
    {
        cout << -1 << endl;
    }
    else
    {
        if (uniquelist.size() == 2)
        {
            if (abs(uniquelist[0] - uniquelist[1]) / 2 == abs(uniquelist[0] - uniquelist[1]) / 2.0)
            {
                cout << abs(uniquelist[0] - uniquelist[1]) / 2 << endl;
            }
            else
            {
                cout << abs(uniquelist[0] - uniquelist[1]) << endl;
            }
            return 0;
        }
        if (uniquelist.size() == 3)
        {
            if (abs(uniquelist[0] - uniquelist[1]) == abs(uniquelist[1] - uniquelist[2]))
            {
                cout << abs(uniquelist[0] - uniquelist[1]) << endl;
            }
            else
            {
                cout << -1 << endl;
            }
            return 0;
        }
    }
}