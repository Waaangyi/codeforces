#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int len;
        cin >> len;
        set<int> numlist;
        for (int i = 0; i < len; i++)
        {
            int a;
            cin >> a;
            if (a % 2 == 0)
            {
                numlist.insert(a);
            }
        }
        int op = 0;
        while (!numlist.empty())
        {
            op++;
            int d = *numlist.rbegin();
            numlist.erase(d);

            if ((d / 2) % 2 == 0)
                numlist.insert(d / 2);
        }
        cout << op << endl;
    }
}