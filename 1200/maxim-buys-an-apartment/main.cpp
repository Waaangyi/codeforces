#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    if (n == k || k == 0)
    {
        cout << "0 0" << endl;
        return 0;
    }

    cout << 1 << " ";
    if (n / 3 >= k)
    {
        cout << 2 * k << endl;
    }
    else
    {
        cout << n - k << endl;
    }
}