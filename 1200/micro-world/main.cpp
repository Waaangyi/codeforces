#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }

    sort(nlist.begin(), nlist.end());
    nlist.push_back(INT_MAX);
    int p = 0;
    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        while (p < n && nlist[i] == nlist[p])
        {
            p++;
        }
        if (nlist[p] <= nlist[i] + k)
            ;
        else
        {
            ans++;
        }
    }
    cout << ans << endl;
}