#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<string> board(n);
    pair<int, int> startpos;
    pair<int, int> endpos;
    for (int i = 0; i < n; i++)
    {
        cin >> board[i];
        size_t pos1 = board[i].find('S');
        size_t pos2 = board[i].find('E');
        if (pos1 != string::npos)
        {
            startpos.first = i;
            startpos.second = int(pos1);
        }
        if (pos2 != string::npos)
        {
            endpos.first = i;
            endpos.second = int(pos2);
        }
    }
    vector<int> dir_num = {0, 1, 2, 3};
    vector<int> xm = {1, 0, -1, 0};
    vector<int> ym = {0, 1, 0, -1};
    string instru;
    cin >> instru;
    int ans = 0;
    do
    {
        pair<int, int> pos = startpos;
        bool ok = 0;
        for (int i = 0; i < instru.size(); i++)
        {
            pos.first += xm[dir_num[int(instru[i]) - 48]];
            pos.second += ym[dir_num[int(instru[i]) - 48]];
            if (pos.first >= n || pos.first < 0 || pos.second >= m || pos.second < 0)
            {
                break;
            }
            if (board[pos.first][pos.second] == '#')
            {
                break;
            }
            if (board[pos.first][pos.second] == 'E')
            {
                ok = 1;
                break;
            }
        }
        if (ok)
            ans++;
    } while (next_permutation(dir_num.begin(), dir_num.end()));
    cout << ans << endl;
}