#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, m;
    cin >> n >> m;

    while (m--)
    {
        ll x, y;
        cin >> x >> y;

        if (n % 2 == 0)
        {
            if ((x + y) % 2 == 0)
            {
                if (y % 2 != 0)
                    y++;
                ll c = n / 2 * (x - 1);
                cout << c + y / 2 << endl;
            }
            else
            {
                if (y % 2 != 0)
                    y++;
                ll c = n / 2 * (x - 1) + n * n / 2;
                cout << c + y / 2 << endl;
            }
        }
        else
        {
            if ((x + y) % 2 == 0)
            {
                if (y % 2 != 0)
                    y++;
                ll c = n / 2 * (x - 1) + x / 2;
                cout << c + y / 2 << endl;
            }
            else
            {
                if (y % 2 != 0)
                    y++;
                ll c = n / 2 * (x - 1) + x / 2 + n * n / 2 + 1;
                if (x % 2 == 0)
                    c--;
                cout << c + y / 2 << endl;
            }
        }
    }
}