#include <bits/stdc++.h>

using namespace std;

int main()
{
    int len;
    cin >> len;
    string s, ss, st, t;
    cin >> s >> t;

    ss = s;
    st = t;
    sort(ss.begin(), ss.end());
    sort(st.begin(), st.end());
    if (st != ss)
    {
        cout << -1 << endl;
        return 0;
    }
    vector<int> answer;
    for (int i = 0; i < len; i++)
    {
        if (s[i] != t[i])
        {
            size_t position = s.find(t[i], i);
            if (position == string::npos)
            {
                cout << -1 << endl;
                return 0;
            }
            for (int j = position; j > i; j--)
            {
                swap(s[j], s[j - 1]);
                answer.push_back(j);
            }
        }
    }
    cout << answer.size() << endl;
    for (int i = 0; i < answer.size(); i++)
    {
        cout << answer[i] << " ";
    }
    cout << endl;
}