#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int len;
    cin >> len;
    vector<string> room(len);
    for (int i = 0; i < len; i++)
    {
        cin >> room[i];
    }
    int ans = 0;
    for (int x = 0; x < len; x++)
    {
        int temp = 0;
        for (int y = 0; y < len; y++)
        {
            if (room[x] == room[y])
                temp++;
        }
        ans = max(temp, ans);
    }
    cout << ans << endl;
}