#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int p, f;
    cin >> p >> f;

    vector<int> food(100);
    for (int i = 0; i < f; i++)
    {
        int a;
        cin >> a;
        food[a - 1]++;
    }
    for (int i = 100; i >= 1; i--)
    {
        vector<int> f(food);

        int k = 0;
        for (int j = 0; j < 100; j++)
        {
            while (f[j] >= i)
            {
                k++;
                f[j] -= i;
            }
        }
        if (k >= p)
        {
            cout << i << endl;
            return 0;
        }
    }
    cout << 0 << endl;
}