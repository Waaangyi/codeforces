#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int len, days;
    cin >> len >> days;

    vector<int> problems(len), sortedproblems;

    for (int i = 0; i < len; i++)
    {
        cin >> problems[i];
    }
    sortedproblems = problems;
    sort(sortedproblems.rbegin(), sortedproblems.rend());
    vector<int> toplist(2000);
    for (int i = 0; i < days; i++)
    {
        toplist[sortedproblems[i] - 1]++;
    }
    int last = 0;
    vector<int> dayslist(days);
    for (int i = 0; i < days - 1; i++)
    {
        for (int j = last; j < len - (days - i - 1); j++)
        {
            if (toplist[problems[j] - 1] != 0)
            {
                toplist[problems[j] - 1]--;
                dayslist[i] = j - last + 1;
                last = j + 1;
                break;
            }
        }
    }
    int profit = accumulate(sortedproblems.begin(), sortedproblems.begin() + days, 0);
    int sum = accumulate(dayslist.begin(), dayslist.end(), 0);
    dayslist.back() = len - sum;
    cout << profit << endl;
    for (int i = 0; i < days; i++)
    {
        cout << dayslist[i] << " ";
    }
    cout << endl;
}