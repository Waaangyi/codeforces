#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int len;
    cin >> len;
    int cc = 0;
    int sc = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (s[i] == '?')
            cc++;
        if (s[i] == '*')
            sc++;
    }
    if (s.length() - sc - cc == len)
    {
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '?')
                continue;
            if (s[i] == '*')
                continue;
            cout << s[i];
        }
        cout << endl;
        return 0;
    }
    if (s.length() - sc - cc < len)
    {
        if (sc == 0)
        {
            cout << "Impossible" << endl;
            return 0;
        }
        int loc = 0;
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '*')
            {
                loc = i;
                s[i] = s[i - 1];
                break;
            }
        }
        string add(len - (s.length() - sc - cc) - 1, s[loc - 1]);
        s.insert(loc, add);
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '?')
                continue;
            if (s[i] == '*')
                continue;
            cout << s[i];
        }
        cout << endl;
        return 0;
    }
    else
    {
        if (int(s.length()) - sc * 2 - cc * 2 - len > 0)
        {
            cout << "Impossible" << endl;
            return 0;
        }
        else
        {
            int a = 0;
            int sub = 0;
            while (s.length() - sub - sc - cc != len)
            {
                if (s[a] == '?' || s[a] == '*')
                {
                    s[a - 1] = '*';
                    sub++;
                    a++;
                }
                else
                {
                    a++;
                }
            }
        }
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '?')
                continue;
            if (s[i] == '*')
                continue;
            cout << s[i];
        }
        cout << endl;
        return 0;
    }
}