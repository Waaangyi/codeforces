#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        int c, m, x;
        cin >> c >> m >> x;

        int totalteams = 0;
        int minskilled = min(c, m);
        int mintotal = min(minskilled, x);
        totalteams = mintotal;
        x -= mintotal;
        c -= mintotal;
        m -= mintotal;
        if (min(c, m) >= 0 && c + m >= 3)
        {
            if (c > m)
            {
                mintotal = c - m;
                x += mintotal;
                c -= mintotal;
            }
            else
            {
                if (c < m)
                {
                    mintotal = m - c;
                    x += mintotal;
                    m -= mintotal;
                }
            }
            mintotal = min(min(c, m), x);
            totalteams += mintotal;

            c -= mintotal;
            m -= mintotal;
            x -= mintotal;

            totalteams += (c + m) / 3;
        }

        cout << totalteams << endl;
    }
}