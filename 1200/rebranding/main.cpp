#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    //vector<pair<char, char>> r(m);
    vector<char> sample(26);
    for (int i = 0; i < 26; i++)
        sample[i] = i + 97;
    string name;
    cin >> name;
    for (int i = 0; i < m; i++)
    {
        char a, b;
        cin >> a >> b;
        for (int j = 0; j < 26; j++)
        {
            if (sample[j] == a)
                sample[j] = b;
            else if (sample[j] == b)
                sample[j] = a;
        }
    }
    for (int i = 0; i < n; i++)
    {
        name[i] = sample[name[i] - 97];
    }
    cout << name << endl;
}