#include <bits/stdc++.h>

using namespace std;

long long digits(long long number)
{
    long long maxmium = INT_MIN;
    long long minimum = INT_MAX;
    while (number > 0)
    {
        long long remainder = number % 10;
        maxmium = max(maxmium, remainder);
        minimum = min(minimum, remainder);

        number /= 10;
    }
    return minimum * maxmium;
}
int main()
{
    int tc;
    cin >> tc;
    while (tc--)
    {
        long long number, k;
        cin >> number >> k;
        for (long long i = 0; i < k - 1; i++)
        {
            long long d = digits(number);
            if (d == 0)
                break;
            number += d;
        }
        cout << number << endl;
    }
}