#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, h;
    cin >> n >> m >> h;

    vector<int> front(m);
    for (int i = 0; i < m; i++)
    {
        cin >> front[i];
    }
    vector<int> side(n);
    for (int i = 0; i < n; i++)
    {
        cin >> side[i];
    }
    vector<vector<int>> top;
    vector<int> temp(m);
    for (int j = 0; j < n; j++)
    {
        for (int i = 0; i < m; i++)
        {
            cin >> temp[i];
        }
        top.push_back(temp);
    }
    vector<vector<int>> answer;
    for (int x = 0; x < n; x++)
    {
        vector<int> layer(m);
        for (int y = 0; y < m; y++)
        {
            if (top[n - x - 1][y] == 1)
            {
                layer[y] = min(side[n - x - 1], front[y]);
            }
        }
        answer.push_back(layer);
    }
    for (int x = n - 1; x >= 0; x--)
    {
        for (int y = 0; y < m; y++)
        {
            cout << answer[x][y] << " ";
        }
        cout << endl;
    }
}