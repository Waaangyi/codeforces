#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int shower[5][5];
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            cin >> shower[i][j];

    vector<int> seq(5);
    for (int i = 0; i < 5; i++)
    {
        seq[i] = i;
    }

    int ans = 0;
    do
    {
        int a = shower[seq[0]][seq[1]] + shower[seq[1]][seq[0]];
        a += shower[seq[2]][seq[3]] + shower[seq[3]][seq[2]];
        a += shower[seq[4]][seq[3]] + shower[seq[3]][seq[4]];
        a += shower[seq[1]][seq[2]] + shower[seq[2]][seq[1]];
        a += shower[seq[2]][seq[3]] + shower[seq[3]][seq[2]];
        a += shower[seq[4]][seq[3]] + shower[seq[3]][seq[4]];
        ans = max(ans, a);
    } while (next_permutation(seq.begin(), seq.end()));
    cout << ans << endl;
}