#include <bits/stdc++.h>

using namespace std;

int finddiff(int a, int b, const vector<int> &numlist)
{
    return abs(numlist[a] - numlist[b]);
}
int main()
{
    int ts;
    cin >> ts;
    while(ts--)
    {
        int n;
        cin >> n;

        vector<int> numlist;
        for(int i = 0; i < n; i++)
        {
            int a;
            cin >> a;

            numlist.push_back(a);
        }

        sort(numlist.begin(), numlist.end());

        vector<int> answers;

        if(n == 3)
        {
            cout << numlist[1] << " " << numlist[2] << " " << numlist[0] << endl;
            continue;
        }
        vector<int> diffcan(2,0);

        diffcan[0] = finddiff(0,numlist.size() - 2, numlist);
        diffcan[1] = finddiff(numlist.size() - 1, 1, numlist);

        if(diffcan[0] > diffcan[1])
        {
            answers.push_back(numlist[numlist.size() - 1]);
            answers.push_back(numlist[0]);
            answers.push_back(numlist[numlist.size() - 2]);
            answers.push_back(numlist[1]);
        }
        else
        {
            answers.push_back(numlist[0]);
            answers.push_back(numlist[numlist.size() - 1]);
            answers.push_back(numlist[1]);
            answers.push_back(numlist[numlist.size() - 2]);
        }
        int l = n;
        if(l % 2 != 0)
            l--;
        for(int i = 4; i < l; i+=2)
        {
            diffcan[0] = abs(answers[i - 1] - numlist[i / 2]);
            diffcan[1] = abs(answers[i - 1] - numlist[numlist.size() - i / 2 - 1]);
            
            if(diffcan[0] > diffcan[1])
            {
                answers.push_back(numlist[i / 2]);
                answers.push_back(numlist[numlist.size() - 1 - i / 2]);
            }
            else
            {
                answers.push_back(numlist[numlist.size() - 1 - i / 2]);
                answers.push_back(numlist[i / 2]);
            }

        }
        if(answers.size() != n)
        {
            answers.push_back(numlist[l / 2]);
        }
        for(int i = 0; i < n; i++)
            cout << answers[answers.size() - i - 1] << " ";
        cout << endl;


    }
}
