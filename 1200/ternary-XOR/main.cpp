#include <bits/stdc++.h>

using namespace std;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        int len;
        cin >> len;

        string line;
        cin >> line;
        string l1(len, '0');
        l1[0] = '1';
        string l2 = l1;
        int bias = 0;
        int stopped_position = 0;
        for (int i = 1; i < len; i++)
        {
            if (line[i] == '1')
            {
                l1[i] = '1';
                bias = 1;
                stopped_position = i + 1;
                break;
            }
            if (line[i] == '2')
            {
                l1[i] = '1';
                l2[i] = '1';
            }
        }
        string zeros(len - stopped_position, 0);
        if (bias == 1)
        {
            l1 += zeros;
            l2 = l2.substr(0, stopped_position) + line.substr(stopped_position, len - 1);
        }
        cout << l1.substr(0, len) << endl
             << l2.substr(0, len) << endl;
    }
}