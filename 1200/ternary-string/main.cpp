#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;
    while (tc--)
    {
        string line;
        cin >> line;
        size_t p1 = line.find("1");
        size_t p2 = line.find("2");
        size_t p3 = line.find("3");

        if (p1 == string::npos || p2 == string::npos || p3 == string::npos)
        {
            cout << 0 << endl;
            continue;
        }
        vector<char> charlist;
        vector<int> freq;

        int p = 0;
        for (int i = 0; i < line.length(); i++)
        {
            if (i == 0 || charlist[p - 1] != line[i])
            {
                charlist.push_back(line[i]);
                freq.push_back(1);
                p++;
            }
            else
            {
                freq[p - 1]++;
            }
        }
        int minimum = INT_MAX;
        for (int i = 1; i < charlist.size() - 1; i++)
        {
            if (charlist[i - 1] != charlist[i] && charlist[i] != charlist[i + 1] && charlist[i - 1] != charlist[i + 1])
            {
                minimum = min(minimum, 2 + freq[i]);
            }
        }
        cout << minimum << endl;
    }
}