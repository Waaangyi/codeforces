#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, a, b;
    cin >> n >> a >> b;
    int answer = 1;
    for (int i = 1; i < max(a, b); i++)
    {
        if (n - i != 0)
            answer = max(answer, min(a / i, b / (n - i)));
    }

    cout << answer << endl;
}