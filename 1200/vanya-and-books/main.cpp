#include <bits/stdc++.h>

using namespace std;

int main()
{
    string numstr;
    cin >> numstr;
    long long num;
    istringstream(numstr) >> num;

    long long total = numstr.length() * num + numstr.length() - 1;
    if (num < 10)
    {
        cout << num << endl;
        return 0;
    }
    long long p = 1;
    for (long long i = 0; i < numstr.length() - 1; i++)
    {
        p = p * 10;
        total -= p;
    }
    cout << total << endl;
}