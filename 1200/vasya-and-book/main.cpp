#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, x, y, d;
        cin >> n >> x >> y >> d;

        if (abs(x - y) % d == 0)
        {
            cout << abs(x - y) / d << endl;
        }
        else
        {
            int z = int(ceil(x / double(d)));
            if ((y - 1) % d == 0)
            {
                z += (y - 1) / d;
            }
            else
            {
                z = -1;
            }
            int u = int(ceil((n - x) / double(d)));
            if ((n - y) % d == 0)
            {
                u += (n - y) / d;
            }
            else
            {
                u = -1;
            }
            if (z == u == -1)
            {
                cout << -1 << endl;
            }
            else
            {
                if (min(z, u) == -1)
                {
                    cout << max(z, u) << endl;
                }
                else
                {
                    cout << min(z, u) << endl;
                }
            }
        }
    }
}