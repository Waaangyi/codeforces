#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string a, b;
    cin >> a >> b;
    if (a.length() < b.length())
    {
        cout << 0 << endl;
        return 0;
    }
    int ans = 0;
    for (int i = 0; i < a.length() - b.length() + 1; i++)
    {
        if (a.substr(i, b.length()) == b)
        {
            ans++;
            a[i + b.length() - 1] = '#';
        }
    }
    cout << ans << endl;
}