#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    while (tc--)
    {
        int len;
        cin >> len;

        vector<int> numlist(len);
        for (int i = 0; i < len; i++)
        {
            cin >> numlist[i];
        }
        sort(numlist.begin(), numlist.end());

        int groups = 0, counter = 0;

        for (int i = 0; i < len; i++)
        {
            counter++;
            if (counter == numlist[i])
            {
                groups++;
                counter = 0;
            }
        }
        cout << groups << endl;
    }
}