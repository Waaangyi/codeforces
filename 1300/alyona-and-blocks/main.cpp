#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, a, b, c;
    cin >> n >> a >> b >> c;

    if (n % 4 == 0)
    {
        cout << 0 << endl;
        return 0;
    }
    ll l = 4 - n % 4;
    if (l == 1)
    {
        cout << min({a, 3 * c, c + b});
    }
    if (l == 2)
    {
        cout << min({a * 2, b, c * 2});
    }
    if (l == 3)
    {
        cout << min({a * 3, c, b + a});
    }
}