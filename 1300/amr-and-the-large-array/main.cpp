#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> occurance(1000005);
    vector<pair<int, int>> occurpos(1000005, make_pair(INT_MAX, -1));
    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
        occurpos[nlist[i] - 1].first = min(occurpos[nlist[i] - 1].first, i);
        occurpos[nlist[i] - 1].second = max(occurpos[nlist[i] - 1].second, i);
        occurance[nlist[i] - 1]++;
    }
    int ma = *max_element(occurance.begin(), occurance.end());
    vector<int> maxlist;
    for (int i = 0; i < 1000005; i++)
    {
        if (ma == occurance[i])
            maxlist.push_back(i + 1);
    }
    int l = 0, r = 0;
    int d = INT_MAX;
    for (int i = 0; i < maxlist.size(); i++)
    {
        int first = occurpos[maxlist[i] - 1].first;
        int last = occurpos[maxlist[i] - 1].second;
        int di = last - first;
        if (di < d)
        {
            d = di;
            l = first + 1;
            r = last + 1;
        }
    }
    cout << l << " " << r << endl;
}