#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    bool ok = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (int(s[i]) % 2 == 0)
        {
            ok = 1;
            break;
        }
    }
    if (!ok)
    {
        cout << -1 << endl;
        return 0;
    }
    for (int i = 0; i < s.length(); i++)
    {
        if (int(s[i]) % 2 == 0 && s[i] <= s[s.length() - 1])
        {
            char temp = s[s.length() - 1];
            s[s.length() - 1] = s[i];
            s[i] = temp;
            cout << s << endl;
            return 0;
        }
    }
    for (int i = s.length() - 1; i >= 0; i--)
    {
        if (int(s[i]) % 2 == 0)
        {
            char temp = s[s.length() - 1];
            s[s.length() - 1] = s[i];
            s[i] = temp;
            cout << s << endl;
            return 0;
        }
    }
}