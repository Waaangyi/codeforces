#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n,m,k;
    cin >> n >> m >> k;

    vector<pair<int, pair<int,int> > > nlist;
    for(int i = 0; i < m; i++)
    {
        int c1,c2,d;
        cin >> c1 >> c2 >> d;
        nlist.push_back(make_pair(d, make_pair(c1,c2)));
    }
    set<int> scities;
    for(int i = 0; i < k; i++)
    {
        int a;
        cin >> a;
        scities.insert(a);
    }
    sort(nlist.begin(), nlist.end());
    for(int i = 0; i < m; i++)
    {
        if(scities.find(nlist[i].second.first) == scities.end() && scities.find(nlist[i].second.second) != scities.end())
        {
            cout << nlist[i].first << endl;
            return 0;
        }
        if(scities.find(nlist[i].second.second) == scities.end() && scities.find(nlist[i].second.first) != scities.end())
        {
            cout << nlist[i].first << endl;
            return 0;
        }
    }
    cout << -1 << endl;

}
