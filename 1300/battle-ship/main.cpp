#include <bits/stdc++.h>

#define ll long long

using namespace std;
int n, k;
int geth(int i, int j, vector<string> &v)
{
    if (v[i][j] != '.')
        return 0;
    int l = j;
    int r = j;
    while (l > 0 && v[i][l - 1] == '.' && abs(l - j) < k - 1)
    {
        l--;
    }
    while (r < n - 1 && v[i][r + 1] == '.' && abs(r - j) < k - 1)
    {
        r++;
    }
    return r - l + 1;
}
int getv(int i, int j, vector<string> &v)
{
    if (v[i][j] != '.')
        return 0;
    int l = i;
    int r = i;
    while (l > 0 && v[l - 1][j] == '.' && abs(l - i) < k - 1)
    {
        l--;
    }
    while (r < n - 1 && v[r + 1][j] == '.' && abs(r - i) < k - 1)
    {
        r++;
    }
    return r - l + 1;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n >> k;

    vector<string> board;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        board.push_back(s);
    }
    int ans = 0;
    int x = 0;
    int y = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            int t = max(geth(i, j, board) - k + 1, 0) + max(getv(i, j, board) - k + 1, 0);
            if (t > ans)
            {
                t = ans;
                x = i;
                y = j;
            }
        }
    }
    cout << x + 1 << " " << y + 1 << endl;
}