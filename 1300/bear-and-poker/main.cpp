#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    for (int i = 0; i < n; i++)
    {
        while (a[i] % 3 == 0 || a[i] % 2 == 0)
        {
            if (a[i] % 3 == 0)
                a[i] /= 3;
            else
                a[i] /= 2;
        }
    }
    sort(a.begin(), a.end());
    if (a.front() == a.back())
        cout << "YES" << endl;
    else
    {
        cout << "NO" << endl;
    }
}