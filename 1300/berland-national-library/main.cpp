#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int ma = 0;
    int noc = 0;
    set<int> room;
    for (int i = 0; i < n; i++)
    {
        char a;
        int b;
        cin >> a >> b;
        if (a == '+')
            room.insert(b);
        else
        {
            if (!room.count(b))
            {
                ma++;
            }
            else
            {
                room.erase(b);
            }
        }
        ma = max(ma, int(room.size()));
    }
    cout << ma << endl;
}