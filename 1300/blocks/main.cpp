#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    string s;
    cin >> s;
    string p = s;

    ll ans1 = 0, ans2 = 0;
    vector<ll> v1, v2;
    for (int i = 0; i < n - 1; i++)
    {
        if (s[i] == 'W')
        {
            s[i] = 'B';
            if (s[i + 1] == 'B')
            {
                s[i + 1] = 'W';
            }
            else
                s[i + 1] = 'B';
            v1.push_back(i);
        }
    }
    for (int i = 0; i < n; i++)
    {
        if (s[i] == 'B')
            continue;
        else
        {
            ans1 = -1;
            break;
        }
    }
    if (ans1 != -1)
    {
        cout << v1.size() << endl;
        for (auto x : v1)
            cout << x + 1 << " ";
        cout << endl;
        return 0;
    }
    s = p;
    if (ans1 == -1)
    {
        for (int i = 0; i < n - 1; i++)
        {
            if (s[i] == 'B')
            {
                s[i] = 'W';
                if (s[i + 1] == 'B')
                {
                    s[i + 1] = 'W';
                }
                else
                    s[i + 1] = 'B';
                v2.push_back(i);
            }
        }
        for (int i = 0; i < n; i++)
        {
            if (s[i] == 'W')
                continue;
            else
            {
                ans2 = -1;
                break;
            }
        }
    }
    if (ans2 != -1)
    {
        cout << v2.size() << endl;
        for (auto x : v2)
            cout << x + 1 << " ";
        cout << endl;
    }
    else
        cout << "-1" << endl;

    return 0;
}