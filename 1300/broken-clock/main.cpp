#include <bits/stdc++.h>

#define ll long long

using namespace std;
int toint(char a) { return a - 48; }
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;

    if (toint(s[0]) > 2)
        s[0] = '1';
    if (n == 24)
    {
        if (toint(s[0]) == 2 && toint(s[1]) > 3)
        {
            s[0] = '0';
        }
    }
    else
    {
        if (toint(s[0]) >= 1)
        {
            s[0] = '1';
            if (toint(s[1]) > 2)
            {
                s[0] = '0';
            }
        }
        else if (toint(s[1]) == 0)
            s[0] = '1';
    }
    if (toint(s[3]) > 5)
        s[3] = '0';
    cout << s << endl;
}