#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int k, p;
    cin >> k >> p;
    ll ans = 0;
    for (int i = 1; i <= k; i++)
    {
        string a = to_string(i);
        string b = to_string(i);
        reverse(b.begin(), b.end());
        string s = a + b;
        ll temp = 0;
        sscanf(s.c_str(), "%lld", &temp);
        ans += temp;
    }
    cout << ans % p << endl;
}