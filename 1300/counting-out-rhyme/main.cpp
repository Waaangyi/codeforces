#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        nlist[i] = i + 1;
    }
    int leader = 0;
    for (int i = 0; i < k; i++)
    {
        int instruction;
        cin >> instruction;
        int kill = (instruction + leader) % nlist.size();
        leader = kill;
        cout << nlist[kill] << " ";
        nlist.erase(nlist.begin() + kill, nlist.begin() + kill + 1);
    }
    cout << endl;
}