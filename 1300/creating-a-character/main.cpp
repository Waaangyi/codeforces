#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int strength, inte, exp;
        cin >> strength >> inte >> exp;
        cout << max(min((strength + exp - inte + 1) / 2, exp + 1), 0) << endl;
    }
}