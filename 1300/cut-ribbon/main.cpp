#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, a, b, c;
    cin >> n >> a >> b >> c;
    int ans = 0;
    if (n % min(a, min(b, c)) == 0)
        ans = n / min(a, min(b, c));
    else
        for (int i = 0; i * a <= n; i++)
        {
            for (int j = 0; j * b + i * a <= n; j++)
            {
                for (int k = 0; k * c + j * b + i * a <= n; k++)
                {
                    if (i * a + j * b + c * k == n)
                    {
                        ans = max(ans, i + j + k);
                        break;
                    }
                }
            }
        }
    cout << ans << endl;
}