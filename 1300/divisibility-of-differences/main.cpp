#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k, m;
    cin >> n >> k >> m;

    vector<int> nlist(n);
    vector<int> numbers(n);
    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a;
        numbers[i] = a;
        nlist[i] = a % m;
    }
    sort(nlist.begin(), nlist.end());
    sort(numbers.begin(), numbers.end());
    int c = 0;
    int num = nlist[0];
    int remainder = -1;
    for (int i = 0; i < n; i++)
    {
        if (num == nlist[i])
        {
            c++;
        }
        if (num != nlist[i] || i == n - 1)
        {
            if (c >= k)
            {
                cout << "Yes" << endl;
                remainder = nlist[i - 1];
                break;
            }
            c = 0;
            num = nlist[i];
        }
    }
    if (remainder == -1)
    {
        cout << "No" << endl;
    }
    else
    {
        int counter = 0;
        for (int i = 0; i < n; i++)
        {
            if (numbers[i] % m == remainder)
            {
                cout << numbers[i] << " ";
                counter++;
                if (counter == k)
                    break;
            }
        }
        cout << endl;
    }
}