#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s1, s2;
    cin >> s1 >> s2;

    int p1 = 0;
    int uncertain = 0;
    int p2 = 0;
    for (int i = 0; i < s1.length(); i++)
    {
        if (s1[i] == '+')
            p1++;
        else
            p1--;
        if (s2[i] == '+')
            p2++;
        if (s2[i] == '-')
            p2--;
        if (s2[i] == '?')
            uncertain++;
    }
    if (uncertain == 0)
    {
        if (p1 == p2)
        {
            cout << 1.0 << endl;
        }
        else
        {
            cout << 0.0 << endl;
        }
        return 0;
    }

    vector<int> moves(uncertain, 1);
    int possible = 0;
    int total = 0;
    for (int i = 0; i < uncertain + 1; i++)
    {
        vector<int> bmoves = moves;
        do
        {
            total++;
        } while (next_permutation(bmoves.begin(), bmoves.end()));
        bmoves = moves;
        if (p2 + accumulate(moves.begin(), moves.end(), 0) == p1)
        {
            do
            {
                possible++;
            } while (next_permutation(bmoves.begin(), bmoves.end()));
        }
        if (i != uncertain)
        {
            moves[i] = -1;
        }
    }
    printf("%.9lf\n", possible / double(total));
}