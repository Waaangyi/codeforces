#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    set<ll> factors;
    for (ll i = 1; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            if (n / i == i)
                factors.insert(i);
            else
            {
                factors.insert(i);
                factors.insert(n / i);
            }
        }
    }
    while (!factors.empty())
    {
        ll a = *factors.rbegin();
        factors.erase(a);
        ll t = 2;
        bool ok = true;
        while (t * t <= a)
        {
            if (a % (t * t) == 0)
            {
                ok = false;
                break;
            }
            t++;
        }
        if (ok)
        {
            cout << a << endl;
            return 0;
        }
    }
}