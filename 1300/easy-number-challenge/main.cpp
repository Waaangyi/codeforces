#include <bits/stdc++.h>

#define ll long long

using namespace std;
int factors(int x)
{
    int num = 0;
    for (int i = 1; i * i <= x; i++)
    {
        if (x % i == 0)
        {
            num++;
            if (x / i != i)
            {
                num++;
            }
        }
    }
    return num;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    int a, b, c, sum = 0;
    cin >> a >> b >> c;

    for (int x = 1; x <= a; x++)
        for (int y = 1; y <= b; y++)
            for (int z = 1; z <= c; z++)
                sum += factors(x * y * z);
    cout << sum << '\n';
}