#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    int h = 1234567;
    int car = 123456;
    int com = 1234;
    //8101
    //811
    for (int i = 0; i <= 811; i++)
    {
        for (int j = 0; j <= 8101; j++)
        {
            int remain = n - (i * h + j * car);
            if (remain % com == 0 && remain >= 0)
            {
                cout << "YES" << endl;
                return 0;
            }
        }
    }
    cout << "NO" << endl;
}