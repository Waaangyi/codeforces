#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string a, b;
    cin >> a >> b;
    int answer = 0;
    int diff = 0;
    for (int i = 0; i < n; i++)
    {
        if (a[i] != b[i])
        {
            diff++;
        }
    }
    for (int i = 0; i < n; i++)
    {
        if (a[i] != b[i])
        {
            if (i + 1 != n && a[i] != a[i + 1] && a[i + 1] != b[i + 1])
            {
                answer++;
                swap(a[i], a[i + 1]);
                diff -= 2;
            }
            else
            {
                answer++;
                diff--;
            }
        }
        if (!diff)
            break;
    }
    cout << answer + diff << endl;
}