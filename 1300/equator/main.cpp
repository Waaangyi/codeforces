#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
        sum += nlist[i];
    }
    int a = 0;
    for (int i = 0; i < n; i++)
    {
        a += nlist[i];
        if (a * 2 >= sum)
        {
            cout << i + 1 << endl;
            return 0;
        }
    }
}