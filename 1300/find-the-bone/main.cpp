#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, k;
    cin >> n >> m >> k;
    set<int> holes;
    for (int i = 0; i < m; i++)
    {
        int a;
        cin >> a;
        holes.insert(a);
        if (a == 1)
        {
            cout << 1 << endl;
            return 0;
        }
    }
    int pos = 1;
    for (int i = 0; i < k; i++)
    {
        int a, b;

        cin >> a >> b;
        if (pos == a)
            pos = b;
        else if (pos == b)
        {
            pos = a;
        }
        if (holes.count(pos))
        {
            cout << pos << endl;
            return 0;
        }
    }
    cout << pos << endl;
}