#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        while (true)
        {
            bool ok = 1;
            int temp = n;
            while (temp > 0)
            {
                if (ok && temp % 3 == 2)
                    ok = 0;
                temp /= 3;
            }
            if (ok)
                break;
            n++;
        }
        cout << n << endl;
    }
}