#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n);
    int even = 0, odd = 0;
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
        if (nlist[i] % 2 == 0)
            even++;
        else
            odd++;
    }
    if (odd == 1)
    {
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] % 2 != 0)
            {
                cout << i + 1 << endl;
                return 0;
            }
        }
    }
    else
    {
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] % 2 == 0)
            {
                cout << i + 1 << endl;
                return 0;
            }
        }
    }
}