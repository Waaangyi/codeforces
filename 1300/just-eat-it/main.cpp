#include <bits/stdc++.h>

using namespace std;
long long maxSubArraySum(vector<long long> a)
{
    long long max_so_far = INT_MIN, max_ending_here = 0;

    for (long long i = 0; i < a.size(); i++)
    {
        max_ending_here = max_ending_here + a[i];
        if (max_so_far < max_ending_here)
            max_so_far = max_ending_here;

        if (max_ending_here < 0)
            max_ending_here = 0;
    }
    return max_so_far;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    long long tc;
    cin >> tc;

    while (tc--)
    {
        long long n;
        cin >> n;
        vector<long long> nlist(n);
        for (long long i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        long long sum = accumulate(nlist.begin(), nlist.end(), (long long)0);
        vector<long long> bc(nlist.begin() + 1, nlist.end()), ec(nlist.begin(), nlist.end() - 1);
        long long psum = max(maxSubArraySum(bc), maxSubArraySum(ec));
        if (sum > psum)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
}