#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    long long x, y, n;
    cin >> x >> y >> n;

    long long ans[6] = {x - y, x, y, y - x, -x, -y};
    cout << (ans[n % 6] % 1000000007 + 1000000007) % 1000000007 << endl;
}