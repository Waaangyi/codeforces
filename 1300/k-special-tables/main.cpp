#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    int a = 1;
    vector<vector<int>> board;
    vector<int> row(n, 0);
    for (int i = 0; i < n; i++)
    {
        board.push_back(row);
    }
    for (int x = 0; x < n; x++)
    {
        for (int y = 0; y < k - 1; y++)
        {
            board[x][y] = a++;
        }
    }
    int sum = 0;
    for (int x = 0; x < n; x++)
    {
        for (int y = k - 1; y < n; y++)
        {
            board[x][y] = a++;
        }
        sum += board[x][k - 1];
    }
    cout << sum << endl;
    for (int x = 0; x < n; x++)
    {
        for (int y = 0; y < n; y++)
        {
            cout << board[x][y] << " ";
        }
        cout << endl;
    }
}