#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);
    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, k;
        cin >> n >> k;
        int a = 1, b = 0;
        for (int i = 1; i < k; i++)
        {
            if (a - 1 == b)
            {
                b = 0;
                a++;
            }
            else
            {
                b++;
            }
        }
        string ans(n, 'a');
        ans[n - 1 - a] = 'b';
        ans[n - 1 - b] = 'b';
        cout << ans << endl;
    }
}