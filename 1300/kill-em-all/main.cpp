#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, r;
        cin >> n >> r;
        set<int> uniquelist;
        for (int i = 0; i < n; i++)
        {
            int a;
            cin >> a;
            uniquelist.insert(a);
        }
        int ans = 0;
        int subtract = 0;
        while (!uniquelist.empty())
        {
            ans++;
            int a = *uniquelist.rbegin();
            uniquelist.erase(a);
            subtract += r;
            while (*uniquelist.begin() - subtract <= 0 && !uniquelist.empty())
            {
                int d = *uniquelist.begin();
                uniquelist.erase(d);
            }
        }
        cout << ans << endl;
    }
}