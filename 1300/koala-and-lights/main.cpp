#include <bits/stdc++.h>

#define ll long long

using namespace std;
void reverse(char &c)
{
    if (c == '1')
        c = '0';
    else
        c = '1';
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;
    vector<pair<int, int>> a(n);
    for (auto &t : a)
    {
        cin >> t.first >> t.second;
    }
    auto ok = [&](int t, int a, int b, char c) {
        t -= b;
        if (t < 0)
        {
            return (int)(c == '1');
        }
        else
        {
            int cnt = ((t / a));
            int alr = ((cnt + 1) % 2) ^ (c == '1');
            return alr;
        }
    };
    int ans = 0;
    for (int i = 0; i < 10000; i++)
    {
        int cur = 0;
        for (int j = 0; j < n; j++)
        {
            cur += ok(i, a[j].first, a[j].second, s[j]);
        }
        ans = max(ans, cur);
    }
    cout << ans << endl;
}