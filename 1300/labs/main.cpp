#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int b = n;
    n = n * n;
    vector<vector<int>> ans;
    vector<int> empty(b, 0);
    for (int i = 0; i < b; i++)
    {
        ans.push_back(empty);
    }
    int d = 1;
    int x = 0, y = 0;
    while (y < b)
    {
        if (d == 1)
        {
            while (x < b)
            {
                ans[x][y] = n--;
                x += d;
            }
            x = b - 1;
        }
        else
        {
            while (x >= 0)
            {
                ans[x][y] = n--;
                x += d;
            }
            x = 0;
        }
        d = -d;
        y++;
    }
    for (int i = 0; i < b; i++)
    {
        for (int j = 0; j < b; j++)
        {
            cout << ans[i][j] << " ";
        }
        cout << endl;
    }
}