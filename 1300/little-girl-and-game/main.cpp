#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    vector<int> list(26);
    for (int i = 0; i < s.length(); i++)
    {
        list[s[i] - 'a']++;
    }
    int oc = 0;
    for (int i = 0; i < 26; i++)
    {
        if (list[i] % 2 == 1)
            oc++;
    }

    if (oc <= 1)
    {
        cout << "First" << endl;
        return 0;
    }
    if (oc % 2 == 0)
    {
        cout << "Second" << endl;
        return 0;
    }
    cout << "First" << endl;
}