#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int ans = 0;
    for (int a = 1; a <= n; a++)
        for (int b = a; b <= n; b++)
        {
            if ((a ^ b) < a + b && a + (a ^ b) > b && b + (a ^ b) > a && a <= b && b <= (a ^ b) && (a ^ b) <= n)
                ans++;
        }
    cout << ans << endl;
}