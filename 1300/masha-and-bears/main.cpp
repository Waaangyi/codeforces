#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int a, b, c, d;
    cin >> a >> b >> c >> d;
    for (int i = 0; i <= 200; i++)
    {
        for (int j = i + 1; j <= 200; j++)
        {
            for (int k = j + 1; k <= 200; k++)
            {
                if (i >= c && 2 * c >= i && j >= b && 2 * b >= j && k >= a && 2 * a >= k)
                {
                    if (i >= d && j >= d && k >= d && 2 * d >= i && 2 * d < j && 2 * d < k)
                    {
                        cout << k << endl;
                        cout << j << endl;
                        cout << i << endl;
                        return 0;
                    }
                }
            }
        }
    }
    cout << -1 << endl;
}