#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<ll> blist(n / 2);
    vector<ll> alist(n);
    for (int i = 0; i < n / 2; i++)
    {
        cin >> blist[i];
    }
    alist[0] = 0;
    alist.back() = blist[0];
    int l = 1, r = n - 2;
    for (int i = 1; i < n / 2; i++)
    {
        ll left = alist[l - 1];
        ll right = alist[r + 1];
        if (right + left >= blist[i])
        {
            alist[l++] = left;
            alist[r--] = blist[i] - left;
        }
        else
        {
            alist[r--] = right;
            alist[l++] = blist[i] - right;
        }

        //for (int j = 0; j < blist[i]; j++)
        //{
        //    if (j >= alist[l - 1] && blist[i] - j <= alist[r + 1])
        //    {
        //        alist[l++] = j;
        //        alist[r--] = blist[i] - j;
        //        break;
        //    }
        //}
    }
    for (int i = 0; i < n; i++)
    {
        cout << alist[i] << " ";
    }
    cout << endl;
}