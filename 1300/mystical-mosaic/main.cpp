#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<string> board;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        board.push_back(s);
    }
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i + 1; j < n; j++)
        {
            bool same = true, no_intersect = true;
            for (int k = 0; k < m; k++)
            {
                if (board[i][k] != board[j][k])
                    same = false;
                if (board[i][k] == '#' && board[j][k] == '#')
                    no_intersect = false;
            }
            if (!same && !no_intersect)
            {
                cout << "No" << endl;
                return 0;
            }
        }
    }
    cout << "Yes" << endl;
}