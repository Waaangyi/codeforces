#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    for (int g = 0; g < m; g++)
    {
        int k;
        cin >> k;
        set<int> group;
        for (int i = 0; i < k; i++)
        {
            int a;
            cin >> a;
            group.insert(a);
        }
        bool ok = false;
        for (auto a : group)
        {
            if (group.count(-a))
            {
                ok = true;
                break;
            }
        }
        if (!ok)
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
}