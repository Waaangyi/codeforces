#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll l;
    cin >> l;
    ll mi = INT_MAX;
    ll ma = INT_MIN;
    vector<ll> nlist(l);
    for (ll i = 0; i < l; i++)
    {
        ll a;
        cin >> a;
        mi = min(mi, a);
        ma = max(ma, a);
        nlist[i] = a;
    }
    if (mi == ma)
    {
        cout << "0 " << l * (l - 1) / 2 << endl;
    }
    else
    {
        ll mac = 0, mic = 0;
        for (ll i = 0; i < l; i++)
        {
            if (nlist[i] == ma)
                mac++;
            if (nlist[i] == mi)
                mic++;
        }
        cout << ma - mi << " " << mac * mic << endl;
    }
}