#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    if (k > n)
    {
        cout << -1 << endl;
        return 0;
    }
    if (n == 1 && k == 1)
    {
        cout << 'a' << endl;
        return 0;
    }
    if (k == 1)
    {
        cout << -1 << endl;
        return 0;
    }
    for (int i = 0; i < n - k + 2; i++)
    {
        if (i % 2)
        {
            cout << 'b';
        }
        else
        {
            cout << 'a';
        }
    }
    char c = 'c';
    for (int i = 0; i < k - 2; i++)
    {
        cout << c;
        c++;
    }
    cout << endl;
}