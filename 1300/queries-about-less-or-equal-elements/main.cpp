#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<int> nlist(n), mlist(m);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < m; i++)
    {
        cin >> mlist[i];
    }
    sort(nlist.begin(), nlist.end());
    for (int i = 0; i < m; i++)
    {
        cout << upper_bound(nlist.begin(), nlist.end(), mlist[i]) - nlist.begin() << " ";
    }
    cout << endl;
}