#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    sort(nlist.begin(), nlist.end());
    int ans = 1;
    long long waittime = nlist[0];
    for (int i = 1; i < n; i++)
    {
        if (waittime <= nlist[i])
        {
            ans++;
            waittime += nlist[i];
        }
    }
    cout << ans << endl;
}