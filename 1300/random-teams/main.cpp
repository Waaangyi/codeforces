#include <bits/stdc++.h>

#define ll long long

using namespace std;
ll f(ll a)
{
    return a * (a - 1) / 2;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, m;
    cin >> n >> m;
    cout << f(n / m) * (m - n % m) + f(n / m + 1) * (n % m) << " " << f(n - m + 1) << endl;
}