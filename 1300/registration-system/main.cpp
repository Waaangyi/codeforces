#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<string> words;
    vector<int> freq;
    for (int i = 0; i < n; i++)
    {
        string line;
        cin >> line;
        int counter;
        auto position = find(words.begin(), words.end(), line);
        if (position != words.end())
        {
            cout << line + to_string(++freq[distance(words.begin(),position)]) << endl;
        }
        else
        {
            cout << "OK" << endl;
            words.push_back(line);
            freq.push_back(0);

        }
    }
}
