#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    sort(nlist.rbegin(), nlist.rend());
    if (nlist[0] == 1)
        nlist[0] = 2;
    else
    {
        nlist[0] = 1;
    }
    sort(nlist.begin(), nlist.end());
    for (int i = 0; i < n; i++)
    {
        cout << nlist[i] << " ";
    }
    cout << endl;
}