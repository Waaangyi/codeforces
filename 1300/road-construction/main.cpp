#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    set<int> forbidden;
    for (int i = 0; i < m; i++)
    {
        int a, b;
        cin >> a >> b;
        forbidden.insert(a);
        forbidden.insert(b);
    }
    int counter = 1;
    while (!forbidden.empty())
    {
        int a = *forbidden.begin();
        forbidden.erase(a);
        if (a != counter)
        {
            break;
        }
        counter++;
    }
    if (n == 1)
    {
        cout << 0 << endl;
    }
    else
    {
        cout << n - 1 << endl;
        for (int i = 1; i < n + 1; i++)
        {
            if (i != counter)
                cout << counter << " " << i << endl;
        }
    }
}