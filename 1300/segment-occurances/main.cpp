#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, q;
    cin >> n >> m >> q;

    string s, t;
    cin >> s >> t;
    vector<pair<int, bool>> aux(n, make_pair(-1, 0));
    if (n < m)
    {
        while (q--)
        {
            cout << 0 << endl;
        }
        return 0;
    }
    if (n == m)
    {
        while (q--)
        {
            int a, b;
            cin >> a >> b;
            if (a == 1 && b == n && s == t)
                cout << 1 << endl;
            else
            {
                cout << 0 << endl;
            }
        }
        return 0;
    }
    int counter = 0;
    for (int i = 0; i < n - m + 1; i++)
    {
        //if (aux[i].first != -1)
        //    continue;
        if (s.substr(i, m) == t)
        {
            counter++;
            aux[i].second = 1;
            //int a = 1;
            //while (a < m)
            //{
            //    aux[i + a].first = counter;
            //    aux[i + a].second = 0;
            //    a++;
            //}
        }
        else
        {
            aux[i].second = 0;
        }

        aux[i].first = counter;
    }
    for (int i = n - m; i < n; i++)
    {
        aux[i].first = counter;
        aux[i].second = 0;
    }
    while (q--)
    {
        int a, b;
        cin >> a >> b;
        if (b - a < m - 1)
        {
            cout << 0 << endl;
            continue;
        }
        int be = aux[a - 1].first;
        if (aux[a - 1].second)
            be--;
        int en = aux[b - 1].first;

        if (aux[b - m].first != aux[b - 1].first)
            en = aux[b - m].first;
        if (en - be <= 0)
            cout << 0 << endl;
        else
            cout << en - be << endl;
        continue;
    }
}