#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    m = 2 * m + 1;
    int len = (n + m - 1) / m;
    cout << len << endl;
    int rem = len * m - n;
    int pos = 1;
    for (int i = 0; i < len; ++i, pos += m)
    {
        cout << pos + (m - 1) / 2 - rem / 2 << " ";
    }
    cout << endl;
}