#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, sx, sy;
    cin >> n >> sx >> sy;
    int u = 0, l = 0, r = 0, d = 0;
    while (n--)
    {
        int x, y;
        cin >> x >> y;
        if (y < sy)
        {
            u++;
        }
        if (y > sy)
        {
            d++;
        }
        if (x < sx)
        {
            l++;
        }
        if (x > sx)
        {
            r++;
        }
    }
    int ma = max({u, l, r, d});
    cout << ma << endl;
    if (u == ma)
    {
        cout << sx << " " << sy - 1 << endl;
        return 0;
    }
    if (l == ma)
    {
        cout << sx - 1 << " " << sy << endl;
        return 0;
    }
    if (d == ma)
    {
        cout << sx << " " << sy + 1 << endl;
        return 0;
    }
    if (r == ma)
    {
        cout << sx + 1 << " " << sy << endl;
        return 0;
    }
}