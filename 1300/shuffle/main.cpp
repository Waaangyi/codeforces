#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, x, m;
        cin >> n >> x >> m;
        int l = x, r = x;
        for (int i = 0; i < m; i++)
        {
            int left, right;
            cin >> left >> right;
            if (max(l, left) <= min(r, right))
            {
                l = min(left, l);
                r = max(right, r);
            }
        }
        cout << r - l + 1 << endl;
    }
}