#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    if (n % 2 == 1 && (n + 1) / 2 == m)
    {
        if (n != 1)
            cout << m - 1 << endl;
        else
            cout << 1 << endl;

        return 0;
    }
    if (n % 2 == 0 && n / 2 == m)
    {
        cout << m + 1 << endl;
        return 0;
    }
    if (n / 2.0 > m)
    {
        cout << m + 1 << endl;
    }
    else
    {
        cout << m - 1 << endl;
    }
}