#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist(n);
    vector<int> reference(n);

    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    reference = nlist;
    sort(reference.begin(), reference.end());

    if (nlist == reference)
    {
        cout << "yes\n1 1" << endl;
        return 0;
    }
    vector<int> revlist(nlist.rbegin(), nlist.rend());
    if (revlist == reference)
    {
        cout << "yes" << endl
             << 1 << " " << n << endl;
        return 0;
    }

    bool changed = false;
    int ans1 = 0;
    int ans2 = 0;
    for (int i = 0; i < n; i++)
    {
        if (nlist[i] != reference[i])
        {
            if (changed == 1)
            {
                cout << "no" << endl;
                return 0;
            }
            changed = 1;
            int goodpos = -1;
            for (int j = i + 1; j < n; j++)
            {
                if (nlist[j - 1] > nlist[j])
                {
                    goodpos = j;
                }
                else
                    break;
            }
            ans1 = i;
            ans2 = goodpos;
            reverse(nlist.begin() + i, nlist.begin() + goodpos + 1);
        }
    }
    cout << "yes" << endl
         << ans1 + 1 << " " << ans2 + 1 << endl;
}