#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, a, b;
    cin >> n >> a >> b;
    string s;
    cin >> s;
    vector<int> emptyspace;
    int counter = 0;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == '.')
            counter++;
        if (s[i] == '*' || i == n - 1)
        {
            if (counter != 0)
                emptyspace.push_back(counter);
            counter = 0;
        }
    }
    if (emptyspace.size() == 0)
    {
        cout << 0 << endl;
        return 0;
    }

    int ans = 0;

    for (int i = 0; i < emptyspace.size(); i++)
    {
        if (a > 0 && b > 0)
        {
            ans += emptyspace[i];
            if (emptyspace[i] % 2 == 1)
            {
                if (a > b)
                {
                    a -= emptyspace[i] / 2 + 1;
                    b -= emptyspace[i] / 2;
                }
                else
                {
                    b -= emptyspace[i] / 2 + 1;
                    a -= emptyspace[i] / 2;
                }
            }
            else
            {
                a -= emptyspace[i] / 2;
                b -= emptyspace[i] / 2;
            }
            if (a < 0)
                ans += a;
            if (b < 0)
                ans += b;
        }
        else
        {
            if (b > a)
                swap(a, b);

            ans += min(a, int(ceil(emptyspace[i] / 2.0)));
            a -= int(ceil(emptyspace[i] / 2.0));
        }

        if (a <= 0 && b <= 0)
            break;
    }
    cout << ans << endl;
}