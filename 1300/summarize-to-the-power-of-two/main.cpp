#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    if (n == 1)
    {
        cout << 1 << endl;
        return 0;
    }
    vector<int> nlist(n);
    map<int, int> nmap;
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
        nmap[nlist[i]]++;
    }
    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        bool ok = false;
        for (int j = 0; j < 31; j++)
        {
            int a = int(pow(2, j)) - nlist[i];
            if (nmap.count(a) && (nmap[a] > 1 || nmap[a] == 1 && nlist[i] != a))
            {
                ok = true;
                break;
            }
        }
        if (!ok)
            ans++;
    }
    cout << ans << endl;
}