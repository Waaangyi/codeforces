#include <bits/stdc++.h>

#define ll long long

using namespace std;
bool is_prime(ll n)
{
    if (n == 1)
    {
        return false;
    }

    ll i = 2;
    while (i * i <= n)
    {
        if (n % i == 0)
        {
            return false;
        }
        i += 1;
    }
    return true;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        ll num;
        cin >> num;

        if (num < 4)
        {
            cout << "NO" << endl;
            continue;
        }
        if (num == 4)
        {
            cout << "YES" << endl;
            continue;
        }
        if (num % 2 == 0)
        {
            cout << "NO" << endl;
            continue;
        }
        if (sqrt(num) == int(sqrt(num)))
        {
            if (is_prime(sqrt(num)))
            {
                cout << "YES" << endl;
            }
            else
            {
                cout << "NO" << endl;
            }
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}