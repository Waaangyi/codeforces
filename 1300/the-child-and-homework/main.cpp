#include <bits/stdc++.h>

#define ll long long
#define mp make_pair
using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string a, b, c, d;
    cin >> a >> b >> c >> d;
    vector<pair<int, int>> len;
    len.push_back(mp(int(a.length()) - 2, 1));
    len.push_back(mp(int(b.length()) - 2, 2));
    len.push_back(mp(int(c.length()) - 2, 3));
    len.push_back(mp(int(d.length()) - 2, 4));
    sort(len.begin(), len.end());
    int p = 0;
    char ans;
    if (len[0].first * 2 <= len[1].first)
    {
        p++;
        ans = len[0].second + 64;
    }
    if (len[3].first >= len[2].first * 2)
    {
        p++;
        ans = len[3].second + 64;
    }
    if (p != 1)
    {
        cout << "C" << endl;
    }
    else
    {
        cout << ans << endl;
    }
}