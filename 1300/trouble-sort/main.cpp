#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> l(n);
        bool z = 0;
        bool o = 0;
        for (int i = 0; i < n; i++)
        {
            cin >> l[i];
        }
        for (int i = 0; i < n; i++)
        {
            int a;
            cin >> a;
            if (a == 0)
                z = 1;
            else
                o = 1;
        }

        vector<int> sortedlist = l;
        sort(sortedlist.begin(), sortedlist.end());
        if (sortedlist == l)
        {
            cout << "YES" << endl;
            continue;
        }
        if (z && o)
        {
            cout << "YES" << endl;
            continue;
        }
        cout << "NO" << endl;
    }
}
