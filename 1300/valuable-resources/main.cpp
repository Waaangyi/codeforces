#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    //vector<pair<int, int>> mines(n);
    ll minx = INT_MAX;
    ll maxx = INT_MIN;
    ll miny = INT_MAX;
    ll maxy = INT_MIN;
    for (int i = 0; i < n; i++)
    {
        ll a, b;
        cin >> a >> b;
        minx = min(a, minx);
        maxx = max(a, maxx);
        miny = min(b, miny);
        maxy = max(b, maxy);
    }
    cout << abs(max(maxx - minx, maxy - miny)) * abs(max(maxx - minx, maxy - miny)) << endl;
}