#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string ta, tb;
    cin >> ta >> tb;
    int tc;
    cin >> tc;
    vector<pair<int, char>> matcht;
    vector<pair<int, char>> matchpc;
    vector<int> homeaux(100);
    vector<int> awayaux(100);
    vector<bool> homered(100, 0);
    vector<bool> awayred(100, 0);
    while (tc--)
    {
        int m, n;
        char t, ct;
        cin >> m >> t >> n >> ct;
        if (t == 'h')
            homeaux[n - 1]++;
        if (ct == 'r' && t == 'h')
            homeaux[n - 1]++;
        if (t == 'a')
            awayaux[n - 1]++;
        if (ct == 'r' && t == 'a')
            awayaux[n - 1]++;

        if (t == 'h' && !homered[n - 1] || t == 'a' && !awayred[n - 1])
        {
            matcht.push_back(make_pair(m, t));
            matchpc.push_back(make_pair(n, ct));
        }
        if (awayaux[n - 1] >= 2)
            awayred[n - 1] = 1;
        if (homeaux[n - 1] >= 2)
            homered[n - 1] = 1;
    }

    set<int> homeredcards;
    set<int> awayredcards;
    for (int i = 0; i < 100; i++)
    {
        if (awayaux[i] >= 2)
        {
            awayredcards.insert(i + 1);
        }
        if (homeaux[i] >= 2)
        {
            homeredcards.insert(i + 1);
        }
    }
    reverse(matcht.begin(), matcht.end());
    reverse(matchpc.begin(), matchpc.end());
    vector<pair<int, pair<int, string>>> answer;
    for (int i = 0; i < matcht.size(); i++)
    {
        if (matcht[i].second == 'h')
            if (homeredcards.count(matchpc[i].first))
            {
                string team = ta;

                answer.push_back(make_pair(matcht[i].first, make_pair(matchpc[i].first, team)));
                homeredcards.erase(matchpc[i].first);
            }
        if (matcht[i].second == 'a')
            if (awayredcards.count(matchpc[i].first))
            {
                string team = tb;

                answer.push_back(make_pair(matcht[i].first, make_pair(matchpc[i].first, team)));
                awayredcards.erase(matchpc[i].first);
            }
    }
    sort(answer.begin(), answer.end());
    for (int i = 0; i < answer.size(); i++)
    {
        cout << answer[i].second.second << " " << answer[i].second.first << " " << answer[i].first << endl;
    }
}