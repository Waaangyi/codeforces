#include <bits/stdc++.h>

#define ll long long

using namespace std;
int strsum(string s)
{
    int ans = 0;
    for (int i = 0; i < s.length(); i++)
    {
        ans += int(s[i]) - 48;
    }
    return ans;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;
    string ns = "";
    for (int i = 0; i < n; i++)
    {
        if (s[i] != '0')
            ns += s[i];
    }
    if (ns.length() == 0)
    {
        cout << "YES" << endl;
        return 0;
    }
    for (int i = 1; i < ns.length(); i++)
    {
        int fsum = strsum(ns.substr(0, i));
        int tempsum = 0;
        bool ok;
        for (int j = i; j < ns.length(); j++)
        {
            tempsum += int(ns[j]) - 48;
            if (tempsum == fsum)
            {
                tempsum = 0;
                ok = true;
            }
            else
            {
                ok = false;
            }

            if (tempsum > fsum)
            {
                ok = false;
                break;
            }
        }
        if (ok)
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
}