#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, s;
        cin >> n >> s;

        vector<int> nlist(n);
        int j = 0;
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] > nlist[j])
                j = i;
            s -= nlist[i];
            if (s < 0)
                break;
        }
        if (s >= 0)
            cout << 0 << endl;
        else
        {
            cout << j + 1 << endl;
        }
    }
}