#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    ll mi = INT_MAX;
    ll ma = 0;
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i];
        mi = min(mi, nlist[i]);
        ma = max(ma, nlist[i]);
    }
    if (ma == mi)
    {
        cout << ma * n << endl;
        return 0;
    }
    ll first = INT_MAX;
    ll last = 0;
    ll ans = mi * n;
    ll cnt = 0;
    vector<ll> loc;
    for (ll i = 0; i < n; i++)
    {
        if (nlist[i] == mi)
        {
            first = min(first, i);
            last = max(last, i);
            loc.push_back(i);
            cnt++;
        }
    }
    ll distance = first - 0 + n - 1 - last;
    for (ll i = 0; i < cnt - 1; i++)
    {
        distance = max(loc[i + 1] - loc[i] - 1, distance);
    }
    cout << ans + distance << endl;
}