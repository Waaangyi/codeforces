#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<int> mlist(m);
    for (int i = 0; i < m; i++)
    {
        cin >> mlist[i];
    }
    vector<pair<int, int>> ans(n);
    vector<int> pos(n);
    vector<int> nlist(n);
    for (int i = 0; i < n; i++)
    {
        ans[i].first = i + 1;
        ans[i].second = i + 1;
        pos[i] = i + 1;
        nlist[i] = i + 1;
    }
    for (int i = 0; i < m; i++)
    {
        int cpos = pos[mlist[i]];

        if (cpos - 1 > -1)
        {
            swap(nlist[pos[mlist[i]] - 1], nlist[pos[mlist[i]] - 2]);
            pos[mlist[i]]--;
            pos[nlist[i]]++;
        }
    }
    for (int i = 0; i < n; i++)
    {
        cout << nlist[i] << " ";
    }
}
