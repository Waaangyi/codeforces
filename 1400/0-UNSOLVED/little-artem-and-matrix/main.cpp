#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, q;
    cin >> n >> m >> q;
    vector<int> rstatus(n, 0);
    vector<int> cstatus(m, 0);
    vector<vector<int>> board;
    {
        vector<int> line(m, 0);
        for (int i = 0; i < n; i++)
        {
            board.push_back(line);
        }
    }

    while (q--)
    {
        int t, r, c, x;
        cin >> t;
        if (t == 1)
        {
            cin >> r;
            cstatus[r - 1]++;
            int last = board[r - 1][m - 1];
            int temp = board[r - 1][0];
            board[r - 1][0] = last;
            for (int i = 1; i < m; i++)
            {
                int t = board[r - 1][i];
                board[r - 1][i] = temp;
                temp = t;
            }
        }
        if (t == 2)
        {
            cin >> c;
            rstatus[c - 1]++;
            int last = board[n - 1][c - 1];
            int temp = board[0][c - 1];
            board[0][c - 1] = last;
            for (int i = 1; i < n; i++)
            {
                int t = board[i][c - 1];
                board[i][c - 1] = temp;
                temp = t;
            }
        }
        if (t == 3)
        {
            cin >> r >> c >> x;
            board[(rstatus[r - 1] + r) % n][(cstatus[c - 1] + c) % m] = x;
        }
    }
    for (int i = 0; i < n; i++)
    {
        for (auto j : board[i])
        {
            cout << j << " ";
        }
        cout << endl;
    }
}