#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<vector<int>> nlist;
        for (int i = 0; i < n; ++i)
        {
            vector<int> line(4);
            for (int j = 0; j < 4; ++j)
            {
                char c;
                cin >> c;
                line[i] = c - '0';
                cout << line[i];
            }
            cout << endl;
            nlist.push_back(line);
        }
    }
}
