#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<string> bad;
    int good = 0;
    for (int i = 0; i < n; ++i)
    {
        string s;
        cin >> s;
        if (s[0] == ')' || s[s.length() - 1] == '(')
        {
            bad.push_back(s);
            continue;
        }
        int a, b;
        a = b = 0;
        for (int j = 0; j < s.length(); ++j)
        {
            if (s[i] == '(')
                a++;
            else
                b++;
        }
        if (a == b)
            good++;
        else
            bad.push_back(s);
    }
    int ans = 0;
    if (good > 0)
        ans = good * (good - 1) / 2;
    cout << ans << endl;
    vector<bool> used(bad.size(), 0);
    for (int i = 0; i < bad.size() - 1; ++i)
    {
        if (used[i])
            continue;
        for (int j = i + 1; j < bad.size(); ++j)
        {
            if (used[j])
                continue;
            string c, d;
            c = bad[i] + bad[j];
            d = bad[j] + bad[i];
            bool flag = 1;
            if (c[0] == ')' || c[c.length() - 1] == '(')
            {
                flag = 0;
            }

            if (d[0] == ')' || d[d.length() - 1] == '(')
            {
                if (flag == 0)
                    continue;
            }
            int a, b;
            a = b = 0;
            for (int k = 0; k < c.length(); ++k)
            {
                if (c[k] == '(')
                    a++;
                else
                    b++;
            }
            if (a == b)
            {
                used[i] = 1;
                used[j] = 1;
                cout << c << " " << d << endl;
                ++ans;
                break;
            }
        }
    }
    cout << ans << endl;
}