#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);

        int sum = 0;
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
            sum += nlist[i];
        }
        int ans;
        for (int i = 0; i < n; ++i) {
            int partitioncnt = n - i;
            bool ok = 1;
            int loc = 0;
            // cout << partitioncnt << endl;
            if (sum % partitioncnt != 0) continue;
            for (int j = 0; j < partitioncnt; ++j) {
                int tempsum = 0;
                if (loc == n) {
                    ok = 0;
                    break;
                }
                while (loc < n) {
                    tempsum += nlist[loc];
                    ++loc;
                    if (tempsum == sum / partitioncnt) break;
                    if (tempsum > sum / partitioncnt) {
                        ok = 0;
                        break;
                    }
                }
            }
            if (loc != n) ok = 0;
            if (ok) {
                ans = n - partitioncnt;
                break;
            }
        }
        cout << ans << endl;
    }
}
