#include "bits/stdc++.h"

#define ll long long

using namespace std;
double distance(int x1, int y1, int x2, int y2)
{
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2) * 1.0);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    double r, x, y, dx, dy;
    cin >> r >> x >> y >> dx >> dy;

    int ans = 0;
    double dist = distance(x, y, dx, dy);
    cout << ceil(dist / (r * 2)) << endl;
}
