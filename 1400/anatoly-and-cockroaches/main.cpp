#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    string s;
    cin >> s;
    s += '$';
    int ans = INT_MAX;
    int r = 0, b = 0;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == 'r' && i % 2 == 0)
            r++;
        if (s[i] == 'b' && i % 2 == 1)
            b++;
    }
    ans = min(ans, max(r, b));
    r = 0, b = 0;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == 'b' && i % 2 == 0)
            r++;
        if (s[i] == 'r' && i % 2 == 1)
            b++;
    }
    ans = min(ans, max(r, b));
    cout << ans << endl;
}