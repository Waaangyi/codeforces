#include "bits/stdc++.h"

#define ll long long

using namespace std;
double dist(double x1, double y1, double x2, double y2)
{
    return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    double ax, ay, bx, by, cx, cy;
    cin >> ax >> ay >> bx >> by >> cx >> cy;

    double distab = dist(ax, ay, bx, by);
    double distbc = dist(bx, by, cx, cy);
    double slopeab = (by - ay) / (bx - ax);
    double slopebc = (cy - by) / (cx - bx);
    if (distab != distbc || slopeab == slopebc)
    {
        cout << "No" << endl;
    }
    else
        cout << "Yes";
}
