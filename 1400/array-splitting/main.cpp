#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    vector<int> difflist;
    for (int i = 1; i < n; ++i)
    {
        difflist.push_back(nlist[i - 1] - nlist[i]);
    }
    if (n == k)
    {
        cout << 0 << endl;
        return 0;
    }
    if (k == 1)
    {
        cout << nlist.back() - nlist.front() << endl;
        return 0;
    }
    sort(difflist.begin(), difflist.end());

    int ans = nlist.back() - nlist.front();
    for (int i = 0; i < k - 1; ++i)
    {
        ans += difflist[i];
    }
    cout << ans << endl;
}
