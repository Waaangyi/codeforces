#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        cin >> n >> k;

        set<int> nset;

        for (int i = 0; i < n; ++i)
        {
            int a;
            cin >> a;
            nset.insert(a);
        }
        if (k >= nset.size())
        {
            cout << 1 << endl;
            continue;
        }

        if (k == 1 && nset.size() != 1)
        {
            cout << -1 << endl;
            continue;
        }

        vector<vector<int>> brute;

        while (true)
        {
            vector<int> t;
            set<int> num;
            int c = 0;
            for (auto i : nset)
            {
                if (num.size() == k)
                {
                    t.push_back(t.back());
                    continue;
                }
                int sum = 0;
                for (int j = 0; j < brute.size(); ++j)
                {
                    sum += brute[j][c];
                }
                num.insert(i - sum);
                t.push_back(i - sum);

                ++c;
            }
            bool b = true;
            for (int i = 0; i < t.size(); ++i)
            {
                if (t[i] != 0)
                {
                    b = false;
                    break;
                }
            }
            if (b)
                break;
            brute.push_back(t);
        }
        cout << brute.size() << endl;
    }
}
