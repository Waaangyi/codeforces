#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;

        int n = s.length();
        vector<int> ans;
        for (int i = 0; i < n - 2; ++i)
        {
            if (s[i] == 'o' && s[i + 1] == 'n' && s[i + 2] == 'e')
            {
                ans.push_back(i + 2);
            }
            if (s[i] == 't' && s[i + 1] == 'w' && s[i + 2] == 'o')
            {
                if (i + 4 < n)
                {
                    if (s[i + 3] == 'n' && s[i + 4] == 'e')
                    {
                        ans.push_back(i + 3);
                        i += 2;
                    }
                    else
                        ans.push_back(i + 2);
                }
                else
                {
                    ans.push_back(i + 2);
                }
            }
        }
        cout << ans.size() << endl;
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
