#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    string number = "";
    string mult = "";
    int pos = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (s[i] == '.')
            continue;
        if (s[i] == 'e')
        {
            mult = s.substr(i + 1, s.length() - i - 1);
            pos = i;
            break;
        }
        else
        {
            number += s.substr(i, 1);
        }
    }
    if (mult == "0")
    {
        string ans = s.substr(0, pos);
        if (stod(ans) == int(stod(ans)))
        {
            cout << stod(ans) << endl;
        }
        else
        {
            cout << ans << endl;
        }

        return 0;
    }
    int power = stoi(mult);
    if (s[0] == '.')
        power--;
    if (number.length() - 1 <= power)
    {
        //cout << number;
        for (int i = number.length() - 1; i < power; i++)
        {
            number += '0';
        }
        number.erase(0, min(number.find_first_not_of('0'), number.size() - 1));
        cout << number << endl;
    }
    else
    {
        number.insert(power + 1, ".");
        number.erase(0, min(number.find_first_not_of('0'), number.size() - 1));
        cout << number << endl;
    }
}