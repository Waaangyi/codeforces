#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<string> nlist(n);
        bool odd, even;
        odd = even = 0;
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            odd = max(int(nlist[i].length() % 2), int(odd));
            even = max(int(1 - nlist[i].length() % 2), int(even));
        }
        if (odd)
        {
            cout << n << endl;
            continue;
        }
        int zero, one;
        zero = one = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < nlist[i].length(); ++j)
            {
                zero += nlist[i][j] == '0';
                one += nlist[i][j] == '1';
            }
        }
        if (one % 2)
            cout << n - 1 << endl;
        else
            cout << n << endl;
    }
}
