#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> shelf(200005);
    int left = 1, right = 0;
    while (n--)
    {
        char inst;
        int id;
        cin >> inst >> id;

        if (inst == 'L')
            shelf[id] = --left;
        if (inst == 'R')
            shelf[id] = ++right;
        if (inst == '?')
        {
            //auto it = find(shelf.begin(), shelf.end(), id);
            //int index = it - shelf.begin();
            //cout << min(int(shelf.size()) - index - 1, index) << endl;
            cout << min(shelf[id] - left, right - shelf[id]) << endl;
        }
    }
}
