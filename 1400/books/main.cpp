#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, t;
    cin >> n >> t;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }

    ll ans = 0;
    ll j = 0;
    ll time = 0;
    for (ll i = 0; i < n; i++)
    {
        while (time <= t && j != n)
        {
            time += nlist[j];
            j++;
        }
        if (time <= t)
        {
            ans = max(ans, j - i);
        }
        else
            ans = max(ans, j - i - 1);

        time -= nlist[i];
    }
    cout << ans << endl;
}
