#include <bits/stdc++.h>

#define ll long long

using namespace std;

bool sortbysec(const pair<int, int> &a, const pair<int, int> &b)
{
    return a.second < b.second;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        sort(nlist.begin(), nlist.end());
        vector<pair<int, int>> flist;
        flist.push_back(make_pair(nlist.front(), 1));
        for (int i = 1; i < n; ++i)
        {
            if (flist.back().first == nlist[i])
            {
                flist.back().second++;
            }
            else
            {
                flist.push_back(make_pair(nlist[i], 1));
            }
        }
        sort(flist.rbegin(), flist.rend(), sortbysec);
        int ans = flist.front().second;
        int val = flist.front().second;
        for (int i = 1; i < flist.size(); ++i)
        {
            if (val == 1)
                break;
            if (flist[i].second < val)
            {
                ans += flist[i].second;
                val = flist[i].second;
            }
            else
            {
                val--;
                ans += val;
            }
        }
        cout << ans << endl;
    }
}
