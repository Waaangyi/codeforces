#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    n++;
    vector<string> dir;
    while (n--)
    {
        string s;
        getline(cin, s);
        if (s[0] == 'p')
        {
            cout << "/";
            for (auto i : dir)
            {
                cout << i << "/";
            }
            cout << endl;
            continue;
        }
        if (s == "cd ..")
        {
            dir.pop_back();
            continue;
        }
        if (s[3] == '/')
            dir.clear();
        for (int i = 3; i < s.length(); i++)
        {
            string dirname = "";
            while (s[i] != '/' && i < s.length())
            {
                dirname += s.substr(i, 1);
                i++;
            }
            if (dirname.length() == 0)
                continue;
            if (dirname == "..")
            {
                dir.pop_back();
            }
            else
                dir.push_back(dirname);
        }
    }
}
