#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, k, t;
    cin >> n >> m >> k >> t;
    set<int> wasteland;

    for (int i = 0; i < k; ++i)
    {
        int a, b;
        cin >> a >> b;
        wasteland.insert((a - 1) * m + b);
    }
    const vector<string> food{"Grapes", "Carrots", "Kiwis"};
    while (t--)
    {
        int a, b;
        cin >> a >> b;
        int pos = (a - 1) * m + b;
        if (wasteland.count((a - 1) * m + b))
        {
            cout << "Waste" << endl;
            continue;
        }
        auto loc = lower_bound(wasteland.begin(), wasteland.end(), pos);
        cout << food[(pos - distance(wasteland.begin(), loc)) % 3] << endl;
    }
}
