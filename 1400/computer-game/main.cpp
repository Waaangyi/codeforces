#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--)
    {
        ll k, n, a, b;
        cin >> k >> n >> a >> b;

        if (n * b >= k)
        {
            cout << -1 << endl;
        }
        else
        {
            ll ans = 0;
            ll lowerbound = 0;
            ll upperbound = k / a;
            ll mid;
            while (true)
            {
                if (upperbound >= lowerbound)
                    mid = (lowerbound + upperbound) / 2;
                else
                    break;

                ll temp = k - mid * a - max(0ll, (n - mid)) * b;
                if (temp > 0)
                {
                    ans = mid;
                    lowerbound = mid + 1;
                }
                else
                    upperbound = mid - 1;
                // cout << lowerbound << " " << upperbound << endl;
            }
            cout << min(ans, n) << endl;
        }
    }
}
