#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int p;
    cin >> p;

    int cnt = 0;
    for (int i = 1; i < p; i++)
    {
        if (__gcd(i, p - 1) == 1)
        {
            cnt++;
        }
    }
    cout << cnt << endl;
}
