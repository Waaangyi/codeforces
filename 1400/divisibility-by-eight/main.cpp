#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    for (int i = 0; i < s.length(); ++i)
    {
        int num = s[i] - '0';
        if (num % 8 == 0)
        {
            cout << "YES" << endl << num;
            return 0;
        }
    }
    for (int i = 0; i < s.length(); ++i)
    {
        for (int j = i + 1; j < s.length(); ++j)
        {
            int num = (s[i] - '0') * 10 + (s[j] - '0');
            if (num % 8 == 0)
            {
                cout << "YES" << endl << num;
                return 0;
            }
        }
    }
    for (int i = 0; i < s.length(); ++i)
    {
        for (int j = i + 1; j < s.length(); ++j)
        {
            for (int k = j + 1; k < s.length(); ++k)
            {
                int num = (s[i] - '0') * 100 + (s[j] - '0') * 10 + (s[k] - '0');
                if (num % 8 == 0)
                {
                    cout << "YES" << endl << num;
                    return 0;
                }
            }
        }
    }
    cout << "NO" << endl;
}
