#include "bits/stdc++.h"

#define ll long long

using namespace std;
map<int, pair<int, int>> conn;
pair<int, int> findhead(map<int, int> &rcon, int a)
{
    int size = INT_MAX;
    int originial = a;
    while (rcon[a] != 0)
    {
        size = min(size, conn[rcon[a]].second);
        a = rcon[a];

        if (a == originial)
            return {-1, -1};
    }
    return {a, size};
}

pair<int, int> findtail(int a)
{
    int size = INT_MAX;
    int originial = a;
    while (conn[a].first != 0)
    {
        size = min(size, conn[a].second);
        a = conn[a].first;
        if (a == originial)
            return {-1, -1};
    }
    return {a, size};
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, p;
    cin >> n >> p;

    map<int, int> rcon;
    for (int i = 0; i < p; ++i)
    {
        int a, b, c;
        cin >> a >> b >> c;
        rcon[b] = a;

        conn[a].first = b;
        conn[a].second = c;
    }
    set<pair<int, pair<int, int>>> ans;
    set<int> head;
    // cout << findhead(rcon, 1) << endl;
    for (auto i : conn)
    {
        auto h = findhead(rcon, i.first);
        if (h.first == -1)
        {
            continue;
        }
        if (head.count(h.first))
            continue;
        head.insert(h.first);

        int size = h.second;
        auto t = findtail(i.first);
        size = min(size, t.second);

        if (t.first == -1)
            continue;
        ans.insert({h.first, {t.first, size}});
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i.first << " " << i.second.first << " " << i.second.second
             << endl;
    }
}
