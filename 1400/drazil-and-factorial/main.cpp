#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    vector<string> chars = {"",  "",   "2", "3",    "322",
                            "5", "35", "7", "7222", "7233"};

    string ans = "";
    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        char a;
        cin >> a;

        ans += chars[a - '0'];
    }
    sort(ans.rbegin(), ans.rend());
    cout << ans << endl;
}
