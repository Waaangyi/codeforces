#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;
    while (tc--)
    {
        ll n;
        cin >> n;

        vector<ll> nlist(n);
        for (ll i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        vector<ll> vlist(n, 0);
        ll sum = 0;
        ll high = 0;
        ll pos = 0;
        vector<ll> half;
        for (ll i = 0; i < n - 1; ++i)
        {
            vlist[nlist[i]]++;
            if (vlist[nlist[i]] == 2)
            {
                break;
            }
            high = max(high, nlist[i]);
            sum += nlist[i];
            if (high * (high + 1) / 2 == sum)
            {
                half.push_back(i);
            }
        }
        vector<pair<ll, ll>> ans;
        set<ll> temp;
        ll oldsize = 0;
        ll s = 0;
        ll h = 0;
        bool ok = 1;
        int j = nlist.size() - 1;
        sort(half.rbegin(), half.rend());
        for (ll i = 0; i < half.size(); ++i)
        {
            while (j > half[i] && j >= 0)
            {
                s += nlist[j];
                temp.insert(nlist[j]);
                h = max(h, nlist[j]);
                if (oldsize == temp.size())
                {
                    ok = 0;
                    break;
                }
                oldsize = temp.size();
                j--;
            }
            if (ok)
            {
                if (h * (h + 1) / 2 == s)
                {
                    ans.push_back({half[i] + 1, n - half[i] - 1});
                }
            }
            else
                break;
        }

        cout << ans.size() << endl;
        for (auto i : ans)
        {
            cout << i.first << " " << i.second << endl;
        }
    }
}
