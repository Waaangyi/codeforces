#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll x;
    cin >> x;

    vector<ll> factors;
    for (ll i = 1; i * i <= x; ++i)
    {
        if (x % i == 0)
        {
            factors.push_back(i);
            if (x / i != i)
            {
                factors.push_back(x / i);
            }
        }
    }
    ll a = 1, b = 1;
    ll high = 1e14;
    for (ll i = 0; i < factors.size(); ++i)
    {
        ll ta = factors[i];
        ll tb = x / ta;
        if (x != (ta * tb) / __gcd(ta, tb))
            continue;
        if (high > max(ta, tb))
        {
            high = max(ta, tb);
            a = ta;
            b = tb;
        }
    }
    cout << a << " " << b << endl;
}
