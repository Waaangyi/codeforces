#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int mod = 1e9 + 7;
    int i = 0;
    int target = 568124848;
    // int target = 992449444;
    int ans = 1;
    while (true)
    {
        if (ans == target)
        {
            cout << i << endl;
            return 0;
        }
        ans *= 2;
        ans %= mod;
        ++i;
    }
}
