#include "bits/stdc++.h"

#define ll long long

using namespace std;
ll mod = 1e9 + 7;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<int> nlist(n);
    vector<int> mlist(m);

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    for (int i = 0; i < m; ++i)
    {
        cin >> mlist[i];
    }

    vector<vector<bool>> board(n, vector<bool>(m, false));

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < nlist[i]; ++j)
        {
            board[i][j] = 1;
        }
    }
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < mlist[i]; ++j)
        {
            board[j][i] = 1;
        }
    }

    for (int i = 0; i < n; ++i)
    {
        int cont = 0;
        for (int j = 0; j < m; ++j)
        {
            if (board[i][j])
                ++cont;
            else
                break;
        }
        if (cont > nlist[i])
        {
            cout << 0 << endl;
            return 0;
        }
    }

    for (int i = 0; i < m; ++i)
    {
        int cont = 0;
        for (int j = 0; j < n; ++j)
        {
            if (board[j][i])
                ++cont;
            else
                break;
        }
        if (cont > mlist[i])
        {
            cout << 0 << endl;
            return 0;
        }
    }
    ll possible = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            if (!board[i][j])
            {
                if (nlist[i] < j && mlist[j] < i)
                {
                    ++possible;
                }
            }
        }
    }
    cerr << possible << endl;
    ll ans = 1;
    for (int i = 0; i < possible; ++i)
    {
        ans *= 2;
        ans %= mod;
    }
    cout << ans << endl;
}
