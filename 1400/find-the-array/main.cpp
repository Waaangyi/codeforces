#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        ll sum = 0;
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            sum += nlist[i];
        }

        vector<int> ans = nlist;
        ll delta = 0;
        for (int i = 0; i < n; i += 2)
        {
            delta += nlist[i] - 1;
        }
        if (delta * 2 <= sum)
        {
            for (int i = 0; i < n; i += 2)
            {
                nlist[i] = 1;
            }
        }
        else
        {
            for (int i = 1; i < n; i += 2)
            {
                nlist[i] = 1;
            }
        }
        for (auto i : nlist)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
