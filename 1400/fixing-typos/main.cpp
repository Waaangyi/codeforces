#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int n = s.length();
    vector<pair<char, int>> flist;
    flist.push_back(make_pair(s[0], 1));
    for (int i = 1; i < n; ++i)
    {
        if (s[i] == flist.back().first)
        {
            flist.back().second++;
        }
        else
        {
            flist.push_back(make_pair(s[i], 1));
        }
    }

    for (int i = 0; i < flist.size(); ++i)
    {
        if (flist[i].second >= 3)
            flist[i].second = 2;
    }
    int f = flist.front().second;
    for (int i = 1; i < flist.size(); ++i)
    {
        if (f == 2 && flist[i].second == 2)
        {
            flist[i].second--;
        }
        f = flist[i].second;
    }
    for (auto i : flist)
    {
        for (int j = 0; j < i.second; ++j)
        {
            cout << i.first;
        }
    }
}
