#include "bits/stdc++.h"

#define ll long long

using namespace std;
ll power(int num, int deg, int m) {
  if (!deg) return 1;

  if (deg % 2) {
    return (power(num, deg - 1, m) * num) % m;

  } else {
    ll sqrt_res = power(num, deg / 2, m);
    return (sqrt_res * sqrt_res) % m;
  }
}

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(nullptr);
  int n, m;
  cin >> n >> m;

  ll ans = power(3, n, m);
  ans--;
  if (ans < 0) ans += m;
  cout << ans;
}
