#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    if (2 * k + 1 > n)
    {
        cout << -1;
        return 0;
    }

    // set<pair<int, int>> used;
    vector<pair<int, int>> ans;
    for (int i = 0; i < n; ++i)
    {
        int cnt = 0;
        for (int j = 0; j < i - k; ++j)
        {
            ans.push_back(make_pair(i, j));
            cnt++;
            if (cnt == k)
            {
                break;
            }
        }
        if (cnt == k)
            continue;
        for (int j = i + 1; j < n; ++j)
        {
            ans.push_back(make_pair(i, j));
            cnt++;
            if (cnt == k)
            {
                break;
            }
        }
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i.first + 1 << " " << i.second + 1 << '\n';
    }
    cerr << ans.size() << endl;
}
