#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(105);
    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        nlist[a]++;
    }
    int ans = 0;
    while (true)
    {
        int j = -1;
        for (int i = 0; i <= 100; ++i)
            if (nlist[i])
            {
                j = i;
                break;
            }
        if (j == -1)
            break;
        ans++;
        for (int i = 0;; i++)
        {
            while (j <= 100 && (!nlist[j] || j < i))
                j++;
            if (j > 100)
                break;
            nlist[j]--;
        }
    }
    cout << ans << endl;
}
