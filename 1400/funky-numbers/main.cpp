#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist;
    {
        int temp = 1;
        while ((temp + 1) * temp < n * 2)
        {
            nlist.push_back(temp * (temp + 1) / 2);
            temp++;
        }
    }
    for (int i = 0; i < nlist.size(); i++)
    {
        if (binary_search(nlist.begin(), nlist.end(), n - nlist[i]))
        {
            cout << "YES" << endl;
            return 0;
        }
    }
    cout << "NO" << endl;
    return 0;
}
