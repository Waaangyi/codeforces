#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> dlist(10, 0), oldlist;
    vector<int> nlist(n);

    for (int &i : nlist)
    {
        char c;
        cin >> c;
        i = c - '0';
    }
    for (int i = 0; i < n; ++i)
    {
        char a;
        cin >> a;
        dlist[a - '0']++;
    }
    oldlist = dlist;
    int ans = 0;
    sort(nlist.begin(), nlist.end());
    for (int i = 0; i < n; ++i)
    {
        if (dlist[nlist[i]] != 0)
        {
            dlist[nlist[i]]--;
        }
        else
        {
            int big = 0;
            for (int j = 0; j <= 9; ++j)
            {
                if (dlist[j] != 0)
                {
                    big = j;
                }
                if (big > nlist[i])
                    break;
            }

            if (big < nlist[i])
            {
                ++ans;
            }
            --dlist[big];
        }
    }
    cout << ans << endl;
    ans = 0;
    dlist = oldlist;

    for (int i = 0; i < n; ++i)
    {
        int big = -1;
        for (int j = nlist[i] + 1; j < 10; ++j)
        {
            if (j > nlist[i] && dlist[j])
            {
                big = j;
                break;
            }
        }
        if (big != -1)
        {
            ++ans;
            --dlist[big];
        }
        else
            break;
    }
    cout << ans << endl;
}
