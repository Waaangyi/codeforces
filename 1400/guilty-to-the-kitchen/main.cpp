#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, v;
    cin >> n >> v;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    vector<int> mlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> mlist[i];
    }

    double r = 1000;
    for (int i = 0; i < n; ++i)
    {
        r = min(r, mlist[i] / (double)nlist[i]);
    }
    double ans = 0;
    for (int i = 0; i < n; ++i)
    {
        ans += nlist[i] * r;
    }
    cout << min(ans, (double)v);
}
