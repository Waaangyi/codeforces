#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int a, b, p;
        string s;
        cin >> a >> b >> p >> s;
        int i = s.length() - 2;
        while (i >= 0)
        {
            if (s[i] == 'A')
            {
                if (p - a >= 0)
                {
                    p -= a;
                }
                else
                {
                    break;
                }
                while (s[i] == 'A')
                {
                    --i;
                }
            }
            else
            {
                if (p - b >= 0)
                {
                    p -= b;
                }
                else
                {
                    break;
                }
                while (s[i] == 'B')
                {
                    --i;
                }
            }
        }
        cout << i + 2 << endl;
    }
}
