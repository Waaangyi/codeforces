#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<string> board;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        board.push_back(s);
    }
    int area = 0;
    int minx = INT_MAX, maxx = INT_MIN;
    int miny = INT_MAX, maxy = INT_MIN;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            if (board[i][j] == 'X')
            {
                minx = min(minx, i);
                maxx = max(maxx, i);
                miny = min(miny, j);
                maxy = max(maxy, j);
                area++;
            }
        }
    }
    if ((maxx - minx + 1) * (maxy - miny + 1) == area)
    {
        cout << "YES" << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
}