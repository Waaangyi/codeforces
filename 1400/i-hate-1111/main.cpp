#include "bits/stdc++.h"

#define ll long long

using namespace std;
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;

        if (n > 1099) {
            cout << "YES" << endl;
            continue;
        }
        bool ok = 0;
        for (int i = 0; i < 100; ++i) {
            for (int j = 0; j < 10; ++j) {
                if (i * 11 + j * 111 == n) {
                    ok = 1;
                    break;
                }
            }
        }
        if (ok) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
