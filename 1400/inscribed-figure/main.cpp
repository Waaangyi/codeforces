#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < n - 1; ++i)
    {
        if (nlist[i] == 2 && nlist[i + 1] == 3)
        {
            cout << "Infinite";
            return 0;
        }
        if (nlist[i] == 3 && nlist[i + 1] == 2)
        {
            cout << "Infinite";
            return 0;
        }
    }
    int ans = 0;
    int last = nlist.front();
    for (int i = 1; i < n; ++i)
    {
        if (last == 1)
        {
            if (nlist[i] == 2)
                ans += 3;
            if (nlist[i] == 3)
                ans += 4;
        }
        if (last == 2)
        {
            if (nlist[i] == 1)
                ans += 3;
        }
        if (last == 3)
        {
            if (nlist[i] == 1)
                ans += 4;
        }
        last = nlist[i];
    }
    cout << "Finite" << endl;
    cout << ans;
}
