#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        ll n;
        cin >> n;

        ll count = 1, res = 0;
        while (n > 0)
        {
            ll x = n / 2;
            ll y = n % 2;
            res = res + (count) * (x + y);
            ++count;
            n = n / 2;
        }
        cout << res << endl;
    }
}
