#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int n = s.size();
    vector<int> dist(26, 0);
    for (int j = 0; j < 26; ++j)
    {
        int temp = -1;
        for (int i = 0; i < n; ++i)
        {
            if (s[i] - 'a' == j)
            {
                dist[j] = max(dist[j], i - temp);
                temp = i;
            }
        }
        dist[j] = max(dist[j], n - temp);
    }
    cout << *min_element(dist.begin(), dist.end()) << endl;
}
