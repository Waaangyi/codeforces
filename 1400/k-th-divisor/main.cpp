#include "bits/stdc++.h"

#define ll long long

using namespace std;

vector<ll> factors(ll x)
{
    vector<ll> result;
    for (ll i = 1; i * i <= x; ++i)
        if (x % i == 0)
        {
            result.push_back(i);
            if (x / i != i)
            {
                result.push_back(x / i);
            }
        }
    return result;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    int k;
    cin >> n >> k;
    vector<ll> factor = factors(n);
    if (factor.size() < k)
    {
        cout << -1 << endl;
        return 0;
    }
    sort(factor.begin(), factor.end());
    cout << factor[k - 1] << endl;
}
