#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, k;
    cin >> n >> k;

    vector<pair<ll, pair<ll, ll>>> nlist(n);
    vector<ll> ans(n, 0);
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i].first;
    }
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i].second.first;
        nlist[i].second.second = i;
    }

    sort(nlist.begin(), nlist.end());
    vector<ll> aux(n);
    aux[0] = nlist[0].second.first;
    for (ll i = 1; i < n; i++)
    {
        aux[i] += aux[i - 1] + nlist[i].second.first;
    }
    multiset<ll> history;
    for (ll i = 0; i < n; i++)
    {
        ll cankill = min(k, i - 0);
        if (i - cankill > 0)
        {
            ans[nlist[i].second.second] = nlist[i].second.first;
            ll cnt = 0;
            for (auto it = history.rbegin(); it != history.rend(); it++)
            {
                if (cnt++ == cankill)
                    break;
                ans[nlist[i].second.second] += *it;
            }
        }
        else
        {
            ans[nlist[i].second.second] = aux[i];
        }
        history.insert(nlist[i].second.first);
    }
    for (auto i : ans)
    {
        cout << i << " ";
    }
}