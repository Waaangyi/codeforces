#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int u = 0;
    for (int i = 0; i < s.size(); ++i)
    {
        if (s[i] <= 'Z')
        {
            u++;
        }
    }
    int ans = u;
    int l = 0;
    for (int i = 0; i < s.size(); ++i)
    {
        if (s[i] > 'Z')
        {
            l++;
        }
        else
            u--;
        ans = min(ans, l + u);
    }
    cout << ans;
}
