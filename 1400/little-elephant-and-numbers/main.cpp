#include <bits/stdc++.h>

#define ll long long

using namespace std;
vector<int> factors(int x)
{
    vector<int> result;
    for (int i = 1; i * i <= x; i++)
    {
        if (x % i == 0)
        {
            result.push_back(i);
            if (x / i != i)
            {
                result.push_back(x / i);
            }
        }
    }
    return result;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> nlist = factors(n);
    string s = to_string(n);
    int ans = 0;
    for (int i = 0; i < nlist.size(); i++)
    {
        string a = to_string(nlist[i]);
        bool ok = false;
        for (int j = 0; j < s.length(); j++)
        {
            for (int k = 0; k < a.length(); k++)
            {
                if (s[j] == a[k])
                {
                    ok = 1;
                    break;
                }
            }
            if (ok)
                break;
        }
        if (ok)
            ans++;
    }
    cout << ans << endl;
}