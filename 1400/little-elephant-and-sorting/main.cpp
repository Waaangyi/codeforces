#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    ll num;
    cin >> num;
    ll add = 0;
    for (ll i = 1; i < n; i++)
    {
        ll a;
        cin >> a;
        a += add;
        if (a < num)
        {
            add += num - a;
            a = num;
        }
        num = a;
    }
    cout << add;
}
