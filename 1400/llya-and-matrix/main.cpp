#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (auto &i : nlist)
    {
        cin >> i;
    }

    sort(nlist.rbegin(), nlist.rend());

    ll ans = 0;
    for (int i = 1; i <= n; i *= 4)
    {
        ans += accumulate(nlist.begin(), nlist.begin() + i, 0ll);
    }
    cout << ans << endl;
}
