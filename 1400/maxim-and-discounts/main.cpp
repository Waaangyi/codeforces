#include <bits/stdc++.h>
#include <climits>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll m;
    cin >> m;
    ll smallest = INT_MAX;
    for (ll i = 0; i < m; i++)
    {
        ll a;
        cin >> a;
        smallest = min(a, smallest);
    }
    ll n;
    cin >> n;
    ll sum = 0;
    vector<ll> nlist(n);
    for (ll i = 0; i < n; i++)
    {
        cin >> nlist[i];
        sum += nlist[i];
    }

    sort(nlist.rbegin(), nlist.rend());

    int cartcnt = 0;
    for (ll i = 0; i < n; i++)
    {
        if (smallest == cartcnt)
        {
            // mlist.erase(mlist.begin());
            ll t = 0;
            ll cnt = i;
            while (i < n && i - cnt != 2)
            {
                sum -= nlist[i];
                i++;
            }
            i--;
            cartcnt = -1;
        }
        cartcnt++;
    }
    cout << sum << endl;
}
