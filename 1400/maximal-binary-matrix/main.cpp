#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<vector<bool>> nlist(n, vector<bool>(n, 0));
    if (k > n * n)
    {
        cout << -1;
        return 0;
    }
    int x = 0;
    int y = x;
    while (k)
    {
        if (k == 1)
        {
            if (nlist[x][x] == 1)
            {
                x++;
            }
            nlist[x][x] = 1;
            break;
        }
        if (y != x)
        {
            k -= 2;
        }
        else
            --k;
        // cout << x << " " << y << endl;
        nlist[x][y] = 1;
        nlist[y][x] = 1;
        ++y;
        if (y == n)
        {
            ++x;
            y = x;
        }
    }
    for (auto i : nlist)
    {
        for (auto j : i)
        {
            cout << j << " ";
        }
        cout << endl;
    }
}
