#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    vector<int> nlist(n);

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    sort(nlist.begin(), nlist.end());

    // make all the elements equal
    // increment by one until can't anymore
    const int middle = n / 2;
    int target = nlist.back();
    ll distance = 0;
    for (int i = middle; i < nlist.size(); ++i)
    {
        distance += target - nlist[i];
    }
    if (distance == k)
    {
        cout << target << endl;
        return 0;
    }
    if (distance > k)
    {
        distance = 0;
        int i = middle;
        int ans = nlist[middle];
        while (true)
        {
            ll temp = nlist[i + 1] - nlist[i];
            distance = temp * (i - middle + 1);
            if (distance < k)
            {
                ans = nlist[i + 1];
                k -= distance;
            }
            else
            {
                // make this part as big as possible
                int length = i - middle + 1;
                ans += k / length;
                break;
            }
            ++i;
        }
        cout << ans << endl;
        return 0;
    }
    else
    {
        k -= distance;
        int ans = nlist.back();

        ans += k / (n - middle);
        cout << ans << endl;
        return 0;
    }
}
