#include <bits/stdc++.h>

#define ll long long

using namespace std;
int toint(char c)
{
    return c - '0';
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    vector<pair<int, int>> position(10);
    for (int i = 1; i <= 9; i++)
    {
        position[i].first = int(ceil(i / (double)3));
        if (i % 3 == 0)
        {
            position[i].second = 3;
        }
        else
            position[i].second = i % 3;
        //cout << position[i].first << " " << position[i].second << endl;
    }
    position[0].first = 4;
    position[0].second = 2;
    int n;
    string s;
    cin >> n >> s;
    if (n == 1)
    {
        cout << "NO" << endl;
        return 0;
    }
    vector<pair<double, double>> vect;
    for (int i = 0; i < n - 1; i++)
    {
        int a = toint(s[i]), b = toint(s[i + 1]);
        //if (position[a].second - position[b].second == 0 && abs(position[a].first - position[b].first) != 3)
        //{
        //    cout << "NO" << endl;
        //    return 0;
        //}
        double slope = 0;
        if (position[a].second - position[b].second == 0)
        {
            if (position[a].first < position[b].first)
            {
                slope = INT_MIN;
            }
            else
            {
                slope = INT_MAX;
            }
        }
        else
        {
            slope = (position[a].first - position[b].first) / (double)(position[a].second - position[b].second);
        }

        double dist = sqrt(pow(position[a].second - position[b].second, 2) + pow(position[a].first - position[b].first, 2));
        vect.push_back(make_pair(slope, dist));

        //for (int j = 0; j < 10; j++)
        //{
        //    for (int k = 0; k < 10; k++)
        //    {
        //        if (j != k && j != a && j != b && k != a && k != b && position[j].second - position[k].second != 0)
        //        {
        //            if (dist == sqrt(pow(position[j].second - position[k].second, 2) + pow(position[j].first - position[k].first, 2)) && slope == (position[a].first - position[b].first) / (double)(position[a].second - position[b].second))
        //            {
        //                cout << "NO" << endl;
        //                return 0;
        //            }
        //        }
        //    }
        //}
    }
    for (int i = 0; i < 10; i++)
    {
        int last = i;
        if (last == toint(s[0]))
            continue;
        bool possible = true;
        for (int j = 1; j < n; j++)
        {
            bool flag = 0;
            for (int k = 0; k < 10; k++)
            {
                if (k == toint(s[j]))
                {
                    flag = 0;
                    continue;
                }
                if (position[last].second - position[k].second == 0)
                {
                    double slope = 0;
                    if (position[last].first < position[k].first)
                    {
                        slope = INT_MIN;
                    }
                    else
                    {
                        slope = INT_MAX;
                    }
                    if (slope == vect[j - 1].first)
                    {
                        flag = 1;
                        last = k;
                        break;
                    }
                    else
                    {
                        flag = 0;
                        continue;
                    }
                }
                if ((vect[j - 1].first == position[last].first - position[k].first) / (double)(position[last].second - position[k].second) && vect[j - 1].second == sqrt(pow(position[last].second - position[k].second, 2) + pow(position[last].first - position[k].first, 2)))
                {
                    flag = 1;
                    last = k;
                    break;
                }
            }
            if (flag == 0)
            {
                possible = 0;
                break;
            }
        }
        if (possible)
        {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;
}