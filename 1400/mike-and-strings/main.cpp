#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<string> nlist(n);

    for (string &i : nlist)
    {
        cin >> i;
    }
    if (n == 1)
    {
        cout << 0 << endl;
        return 0;
    }

    int ans = INT_MAX;
    for (int i = 0; i < n; ++i)
    {
        int cnt = 0;
        string laststring = "";
        for (int j = 0; j < n; ++j)
        {
            if (j == i)
                continue;
            int errcnt = 0;
            string s = nlist[j];
            while (s != nlist[i])
            {
                char c = s.front();
                s.erase(0, 1);
                s += c;
                ++errcnt;
                if (errcnt > s.length() + 10)
                {
                    cout << -1 << endl;
                    return 0;
                }
            }
            cnt += errcnt;
        }
        ans = min(ans, cnt);
    }
    cout << ans << endl;
}
