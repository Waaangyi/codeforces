#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int n = s.length();
    int ocnt = 0;
    bool two = false;
    for (int i = 0; i < n; i++)
    {
        if (s[i] == '1')
            ocnt++;
        if (s[i] == '2')
            two = 1;
    }
    if (!two)
    {
        for (int i = 0; i < n - ocnt; i++)
        {
            cout << 0;
        }
        for (int i = 0; i < ocnt; i++)
        {
            cout << 1;
        }
        return 0;
    }
    if (ocnt)
    {
        bool print = false;
        for (int i = 0; i < n; i++)
        {
            if (s[i] == '2' && !print)
            {
                print = true;
                for (int j = 0; j < ocnt; j++)
                    cout << 1;
            }
            if (s[i] != '1')
                cout << s[i];
        }
        return 0;
    }
    cout << s;
}