#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    int n = s.length();
    int high = 0;
    int low = 0;
    int pos = 0;
    for (int i = 0; i < n; ++i)
        if (s[i] == '-')
        {
            --pos;
            low = min(pos, low);
        }
        else
        {
            ++pos;
            high = max(pos, high);
        }
    cout << high - low << endl;
}
