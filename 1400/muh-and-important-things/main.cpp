#include <bits/stdc++.h>

#define ll long long

using namespace std;
bool sortbysec(const pair<int, int> &a,
               const pair<int, int> &b)
{
    return (a.second < b.second);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, int>> nlist(n), ans;
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i].first;
        nlist[i].second = i;
    }
    sort(nlist.begin(), nlist.end());
    int ele = nlist[0].first;
    int repeat = 1;
    vector<int> rem;
    for (int i = 1; i < n; i++)
    {
        if (nlist[i].first == ele)
        {
            repeat++;
            rem.push_back(i);
        }
        if (nlist[i].first != ele || i == n - 1)
        {
            repeat = 1;
            ele = nlist[i].first;
        }
    }
    if (rem.size() >= 2)
    {
        cout << "YES" << endl;
        ans = nlist;
        swap(ans[rem[0] - 1], ans[rem[0]]);
        for (int i = 0; i < n; i++)
        {
            cout << ans[i].second + 1 << " ";
        }
        cout << endl;
        swap(ans[rem[1] - 1], ans[rem[1]]);
        for (int i = 0; i < n; i++)
        {
            cout << ans[i].second + 1 << " ";
        }
        cout << endl;
        swap(ans[rem[0] - 1], ans[rem[0]]);
        swap(ans[rem[1] - 1], ans[rem[1]]);
        for (int i = 0; i < n; i++)
        {
            cout << ans[i].second + 1 << " ";
        }
        cout << endl;
    }
    else
    {
        cout << "NO" << endl;
    }
}