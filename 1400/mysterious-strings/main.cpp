#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string president[] = {
        "Washington", "Adams",     "Jefferson", "Madison",    "Monroe",
        "Adams",      "Jackson",   "Van Buren", "Harrison",   "Tyler",
        "Polk",       "Taylor",    "Fillmore",  "Pierce",     "Buchanan",
        "Lincoln",    "Johnson",   "Grant",     "Hayes",      "Garfield",
        "Arthur",     "Cleveland", "Harrison",  "Cleveland",  "McKinley",
        "Roosevelt",  "Taft",      "Wilson",    "Harding",    "Coolidge",
        "Hoover",     "Roosevelt", "Truman",    "Eisenhower", "Kennedy",
        "Johnson",    "Nixon",     "Ford",      "Carter",     "Reagan",
        "Bush",       "Clinton"};
    int n;
    cin >> n;

    cout << president[n - 1];
}
