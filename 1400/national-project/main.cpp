#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        ll n, g, b;
        cin >> n >> g >> b;

        ll half = (n + 1) / 2;

        ll ans = half / g * (b + g);
        if (half % g == 0)
            ans -= b;
        else
            ans += half % g;
        cout << max(ans, n) << endl;
    }
}
