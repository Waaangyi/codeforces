#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, q;
    cin >> n >> q;

    vector<vector<bool>> board(n, vector<bool>(2, false));

    set<pair<int, int>> blocked;

    while (q--)
    {
        int a, b;
        cin >> a >> b;

        --a;
        --b;
        board[b][a] = !board[b][a];
        bool block = false;
        if (board[b][a])
        {
            int sec = 0;
            if (a == 0)
            {
                sec = a + 1;
            }
            else
            {
                sec = a - 1;
            }
            if (board[b][sec])
            {
                block = true;
                blocked.insert({b, b});
            }
            if (b > 0)
            {
                if (board[b - 1][sec])
                {
                    block = true;
                    blocked.insert({b - 1, b});
                }
            }
            if (b < n - 1)
            {
                if (board[b + 1][sec])
                {
                    block = true;
                    blocked.insert({b, b + 1});
                }
            }
        }
        else
        {
            int sec = 0;
            if (a == 0)
            {
                sec = 1;
            }
            if (board[b][sec])
            {
                blocked.erase({b, b});
            }

            if (b > 0)
            {
                if (board[b - 1][1] && board[b][0] ||
                    board[b - 1][0] && board[b][1])
                {
                    blocked.insert({b - 1, b});
                }
                else
                {
                    blocked.erase({b - 1, b});
                }
            }
            if (b < n - 1)
            {
                if (board[b + 1][1] && board[b][0] ||
                    board[b + 1][0] && board[b][1])
                {
                    blocked.insert({b, b + 1});
                }
                else
                    blocked.erase({b, b + 1});
            }
        }
        if (blocked.size() == 0)
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }
}
