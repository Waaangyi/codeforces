#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cout << 100000 << " " << 100000 << endl;
    for (int i = 0; i < 100000; ++i)
    {
        cout << 1 << " " << i + 1 << endl;
    }
}
