#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<vector<int>> nlist(n);

    for (int i = 0; i < n; ++i)
    {
        int o;
        cin >> o;
        for (int j = 0; j < o; ++j)
        {
            int a;
            cin >> a;

            nlist[i].push_back(a);
        }
    }
    vector<set<int>> nset;
    vector<int> lookup_table(5e5 + 5, -1);
    set<int> left;
    nset.push_back({});
    for (int i = 0; i < nlist[0].size(); ++i)
    {
        nset[0].insert(nlist[0][i]);
        lookup_table[nlist[0][i] - 1] = 0;
    }
    for (int i = 1; i < n; ++i)
    {
        left.insert(i);
    }
    int z = 0;
    while (true)
    {
        cout << left.size() << endl;
        vector<int> to_remove;
        for (auto i : left)
        {
            bool merge = 0;
            for (int j = 0; j < nlist[i].size(); ++j)
            {
                if (nset[z].count(nlist[i][j]))
                {
                    to_remove.push_back(i);
                    merge = 1;
                    break;
                }
            }
            if (merge)
            {
                for (int j = 0; j < nlist[i].size(); ++j)
                {
                    lookup_table[nlist[i][j] - 1] = z;
                    nset[z].insert(nlist[i][j]);
                }
            }
        }
        if (to_remove.size() == 0)
        {
            if (nlist[*left.begin()].size() == 0)
            {
                left.erase(left.begin());
                continue;
            }
            // create new
            nset.push_back({});
            ++z;
            for (int i = 0; i < nlist[*left.begin()].size(); ++i)
            {
                lookup_table[nlist[*left.begin()][i] - 1] = z;
                nset[z].insert(nlist[*left.begin()][i]);
            }
            left.erase(left.begin());
        }
        else
        {
            for (int i = 0; i < to_remove.size(); ++i)
            {
                left.erase(to_remove[i]);
            }
        }
        if (left.size() == 0)
            break;
    }
    for (int i = 0; i < n; ++i)
    {
        if (lookup_table[i] == -1)
        {
            cout << 1 << " ";
        }
        else
            cout << nset[lookup_table[i]].size() << " ";
    }
}
