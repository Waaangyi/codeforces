#include "bits/stdc++.h"

#define ll long long

using namespace std;
inline ll dsum(ll a)
{
    ll sum = 0;
    while (a != 0)
    {
        sum += a % 10;
        a /= 10;
    }
    return sum;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    ll s = sqrt(n);
    ll ans = -1;
    for (ll i = s; s - i <= 10000; --i)
    {
        if (i)
        {
            if (i * i + i * dsum(i) == n)
            {
                ans = i;
            }
        }
        else
            break;
    }
    cout << ans;
}
