#include <bits/stdc++.h>

#define ll long long

using namespace std;
bool func(int n)
{
    for (int i = 2; i * i <= n; i++)
    {
        if (n % i == 0)
        {
            return false;
        }
    }
    return true;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        bool flag = 0;
        if (n == 1)
        {
            cout << "FastestFinger" << endl;
            continue;
        }
        if (n == 2)
        {
            cout << "Ashishgup" << endl;
            continue;
        }
        for (int i = 2; i < 31; i++)
        {
            if (pow(2, i) == n)
            {
                flag = 1;
                break;
            }
        }
        if (n % 2 == 0 && func(n / 2) && n > 3)
        {
            flag = 1;
        }
        if (flag)
        {
            cout << "FastestFinger" << endl;
        }
        else
        {
            cout << "Ashishgup" << endl;
        }
    }
}
