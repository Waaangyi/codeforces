#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, k;
    cin >> n >> m >> k;

    vector<string> board(n);
    vector<int> ans(m, 0);
    for (int i = 0; i < n; i++)
    {
        cin >> board[i];
    }
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            int t = 0;
            if (board[i][j] == 'L')
            {
                t = j - i;
                if (t >= 0 && t < m)
                    ans[t]++;
            }
            if (board[i][j] == 'R')
            {
                t = j + i;
                if (t >= 0 && t < m)
                    ans[t]++;
            }
            if (board[i][j] == 'U')
            {
                if (!(i & 1))
                    ans[j]++;
            }
        }
    }
    for (auto i : ans)
    {
        cout << i << " ";
    }
}
