#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }
        vector<int> alist(n, 1);
        for (int i = 0; i < n; ++i)
        {
            for (int j = (i + 1) * 2; j <= n; j += (i + 1))
            {
                if (nlist[i] < nlist[j - 1])
                {
                    alist[j - 1] = max(alist[j - 1], alist[i] + 1);
                }
            }
        }
        cout << *max_element(alist.begin(), alist.end()) << endl;
    }
}
