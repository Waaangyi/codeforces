#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        cin >> n >> k;

        vector<int> nlist(n);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        if (n == k)
        {
            cout << n << endl;
            for (auto i : nlist)
            {
                cout << i << ' ';
            }
            continue;
        }
        sort(nlist.begin(), nlist.end());

        int num = nlist.front();
        int uni = 1;
        vector<int> unilist;
        unilist.push_back(num);
        for (int i = 0; i < n; i++)
        {
            if (num != nlist[i])
            {
                uni++;
                num = nlist[i];
                unilist.push_back(num);
            }
        }
        if (uni > k)
        {
            cout << -1 << endl;
            continue;
        }
        for (int i = unilist.size(); i < k; i++)
        {
            unilist.push_back(1);
        }
        cout << n * k << endl;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < k; j++)
            {
                cout << unilist[j] << " ";
            }
        }
        cout << endl;
    }
}
