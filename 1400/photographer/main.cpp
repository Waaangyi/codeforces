#include "bits/stdc++.h"

#define ll long long

using namespace std;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, d, a, b;
    cin >> n >> d >> a >> b;

    vector<pair<int, int>> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        int c, d;
        cin >> c >> d;
        nlist[i].first = c * a + b * d;
        nlist[i].second = i;
    }
    sort(nlist.begin(), nlist.end());
    vector<int> anslist;
    for (int i = 0; i < n; ++i)
    {
        if (d - nlist[i].first >= 0)
        {
            anslist.push_back(nlist[i].second + 1);
            d -= nlist[i].first;
        }
        else
            break;
    }
    cout << anslist.size() << endl;
    for (auto i : anslist)
        cout << i << " ";
}
