#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, d;
    cin >> n >> m >> d;
    n *= m;
    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    int ans = INT_MAX;
    bool possible = 0;
    for (int i = 0; i < 1e4 + 4; ++i)
    {
        bool ok = true;
        int val = 0;
        for (int j = 0; j < n; ++j)
        {
            if (abs(i - nlist[j]) % d != 0)
            {
                ok = 0;
                break;
            }
            val += abs(i - nlist[j]) / d;
        }
        if (ok)
        {
            possible = 1;
            ans = min(val, ans);
        }
    }
    if (possible)
    {
        cout << ans << endl;
    }
    else
    {
        cout << -1 << endl;
    }
}
