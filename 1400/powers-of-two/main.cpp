#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    if (k > n)
    {
        cout << "NO" << endl;
        return 0;
    }
    int bitcnt = 0;
    int bn = n;
    while (bn != 0)
    {
        bn &= bn - 1;
        ++bitcnt;
    }
    if (bitcnt > k)
    {
        cout << "NO" << endl;
        return 0;
    }
    vector<pair<bool, int>> ans(32, {0, 0});
    vector<int> powers;
    for (int i = 0; i < 31; ++i)
    {
        powers.push_back(pow(2, i));
    }
    int loc = 1;
    for (int i = 30; i >= 0; --i)
    {
        if (n / powers[i])
        {
            loc = max(loc, i);
            n -= powers[i];
            ans[i] = {1, 1};
        }
        if (n == 0)
            break;
    }

    while (bitcnt != k)
    {
        ++bitcnt;
        if (ans[loc].second == 0)
        {
            ans[loc].first = 0;
            --loc;
        }
        // break higher powers into 2 lower powers
        if (ans[loc].second-- != 0)
        {
            ans[loc - 1].second += 2;
            ans[loc - 1].first = 1;
        }
    }
    cout << "YES" << endl;
    for (int i = 0; i < 32; ++i)
    {
        if (ans[i].first)
        {
            int out = pow(2, i);
            for (int j = 0; j < ans[i].second; ++j)
            {
                cout << out << " ";
            }
        }
    }
}
