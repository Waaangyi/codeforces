#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        string a, b;
        cin >> a >> b;
        vector<int> ans;
        for (int i = 0; i < a.length(); ++i)
        {
            if (a[i] != b[i])
            {
                ans.push_back(i + 1);
                ans.push_back(1);
                ans.push_back(i + 1);
            }
        }
        cout << ans.size() << " ";
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
