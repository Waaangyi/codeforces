#include "bits/stdc++.h"

#define ll long long

using namespace std;
int n, low, high, x, ans = 0;
void combinationUtil(int arr[], int data[], int start, int end, int index,
                     int r)
{
    if (index == r)
    {
        // vector<int> li(r);
        int sum = 0;
        int small, big;
        small = big = 0;
        small = 1e9;
        for (int i = 0; i < r; ++i)
        {
            // li[i] = data[i];
            sum += data[i];
            small = min(data[i], small);
            big = max(data[i], big);
        }
        if (sum >= low && sum <= high && big - small >= x)
        {
            ++ans;
        }
    }

    for (int i = start; i <= end && end - i + 1 >= r - index; i++)
    {
        data[index] = arr[i];
        combinationUtil(arr, data, i + 1, end, index + 1, r);
    }
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n >> low >> high >> x;

    int nlist[n];
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    for (int i = 2; i <= n; ++i)
    {
        int data[i + 10];
        combinationUtil(nlist, data, 0, n - 1, 0, i);
    }
    cout << ans << endl;
}
