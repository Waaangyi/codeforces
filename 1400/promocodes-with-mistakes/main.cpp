#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<string> nlist(n);

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    int ans = 6;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (i == j)
                continue;
            int cnt = 1;
            for (int k = 0; k < 6; ++k)
            {
                if (nlist[i][k] != nlist[j][k])
                    cnt++;
            }
            ans = min(ans, cnt / 2 - 1);
        }
    }
    cout << ans;
}
