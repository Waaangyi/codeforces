#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n;
    cin >> n;
    vector<string> ans;
    while (n != 0)
    {
        string sub = "";
        string s = to_string(n);
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] != '0')
                sub += '1';
            else
                sub += '0';
        }
        ans.push_back(sub);
        n -= stoi(sub);
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i << " ";
    }
}
