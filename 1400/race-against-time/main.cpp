#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    double h, m, s, t1, t2;
    cin >> h >> m >> s >> t1 >> t2;

    s /= 5;
    m += s / 12;
    m /= 5;
    h += m / 12;
    if (s >= 12)
        s -= 12;
    if (m >= 12)
        m -= 12;
    if (h >= 12)
        h -= 12;
    if (t2 == 12)
        t2 = 0;
    if (t1 == 12)
        t1 = 0;
    cerr << h << " " << m << " " << s << endl;
    double time = t1 + 1;
    if (time == 12)
        time = 0;
    bool flag = true;
    cerr << t1 << " " << h << " " << time << endl;
    if (t1 <= h && time >= h || t1 <= s && time >= s || t1 <= m && time >= m)
    {
        flag = 0;
    }
    while (time != t2 && flag)
    {
        if (time == 12)
            time = -1;
        time++;
        double last = time - 1;
        if (time == 0)
            last = 11;
        if (last <= h && time >= h || last <= s && time >= s || last <= m && time >= m)
        {
            flag = false;
            break;
        }
    }

    if (flag)
    {
        cout << "YES" << endl;
        return 0;
    }
    time = t1 - 1;
    if (time == -1)
        time = 11;
    flag = 1;
    time = t1 - 1;
    double temp = t1;
    if (t1 == 0)
        temp = 12;
    if (time == -1)
        time = 11;
    if (temp >= h && time <= h || temp >= s && time <= s || temp >= m && time <= m)
    {
        flag = false;
    }
    while (time != t2 && flag)
    {
        if (time == -1)
            time = 12;
        time--;
        double last = time + 1;
        if (last >= h && time <= h || last >= s && time <= s || last >= m && time <= m)
        {
            flag = false;
            break;
        }
    }
    if (flag)
    {
        cout << "YES" << endl;
        return 0;
    }
    cout << "NO" << endl;
}