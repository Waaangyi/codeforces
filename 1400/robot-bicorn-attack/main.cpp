#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    int n = s.size();
    // for (int i = 0; i < n - 2; ++i)
    int i = 0;
    int ans = -1;
    // vector<string> snapshots(3, " ");
    int instructions = 0;
    while (i < n - 2)
    {
        ++instructions;
        if (instructions > 100)
        {
            break;
        }
        if (i > 6)
        {
            i = 6;
        }
        if (i == 6)
        {
            if (stoi(s.substr(0, i + 1)) > 1000000)
            {
                i--;
            }
        }
        string one = s.substr(0, i + 1);
        if (one[0] == '0' && one.size() > 1)
        {
            break;
        }
        int j = i + 1;
        int subinstructions = 0;
        while (j < n - 1)
        {
            ++subinstructions;
            if (subinstructions > 100)
                break;
            string two = s.substr(i + 1, j - i);
            while (stoi(two) > 1000000)
            {
                --j;
                two = s.substr(i + 1, j - i);
            }
            if (two[0] == '0' && two.size() > 1)
            {
                break;
            }
            string three = s.substr(j + 1, n - j - 1);
            // cout << one << " " << two << " " << three << endl;
            ++j;
            if (three.size() > 7)
            {
                continue;
            }
            if (stoi(three) > 1000000)
            {
                continue;
            }
            if (three[0] == '0' && three.size() > 1)
            {
                continue;
            }
            ans = max(ans, stoi(one) + stoi(two) + stoi(three));
        }
        ++i;
    }
    cout << ans;
}
