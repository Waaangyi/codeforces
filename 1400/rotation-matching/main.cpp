#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n), blist(2e5 + 10);
    for (int i = 0; i < n; i++)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < n; i++)
    {
        int a;
        cin >> a;
        blist[a] = i;
    }
    vector<int> ans(2e5 + 10);
    for (int i = 0; i < n; i++)
    {
        int dist;
        if (blist[nlist[i]] >= i)
        {
            dist = blist[nlist[i]] - i;
        }
        else
        {
            dist = n - 1 - i;
            dist += blist[nlist[i]] + 1;
        }
        ans[dist]++;
    }
    sort(ans.rbegin(), ans.rend());
    cout << ans.front() << endl;
}
