#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll a, b, c, d;
    cin >> a >> b >> c >> d;
    if (b * c == a * d)
    {
        cout << "0/1" << endl;
        return 0;
    }
    if (a * d < c * b)
    {
        ll p = c * a * b - d * a * a, q = c * a * b;
        ll d = __gcd(p, q);
        cout << p / d << '/' << q / d << endl;
    }
    else
    {
        ll p = d * a * b - c * b * b, q = d * a * b;
        ll d = __gcd(p, q);
        cout << p / d << '/' << q / d << endl;
    }
}