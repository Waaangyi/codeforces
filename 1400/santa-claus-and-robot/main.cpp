#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    string s;
    cin >> s;

    int checkpointx = 0;
    int checkpointy = 0;

    int x = 0, y = 0;
    int lastx = 0, lasty = 0;
    int ans = 0;
    int checkpoint = 0;
    for (int i = 0; i < n; i++)
    {
        lastx = x;
        lasty = y;
        if (s[i] == 'R')
        {
            x++;
        }
        if (s[i] == 'D')
        {
            y++;
        }
        if (s[i] == 'U')
        {
            y--;
        }
        if (s[i] == 'L')
        {
            x--;
        }
        if (abs(x - checkpointx) + abs(y - checkpointy) < i - checkpoint + 1)
        {
            checkpointx = lastx;
            checkpointy = lasty;
            checkpoint = i;
            ans++;
        }
    }
    cout << ans + 1 << endl;
}
