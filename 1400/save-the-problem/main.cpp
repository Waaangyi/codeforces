#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    if (n == 1)
    {
        cout << 1 << " " << 1 << endl << 1;
    }
    else
    {
        cout << (n - 1) * 2 << " " << 2 << endl << 1 << " " << 2;
    }
}
