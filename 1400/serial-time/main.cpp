#include "bits/stdc++.h"

#define ll long long

using namespace std;

vector<vector<string>> nlist;
int k, n, m;
int ans = 0;

int dfs(int a, int b, int c)
{
    int ans = 0;
    if (a < 0 || a >= k || b < 0 || b >= n || c < 0 || c >= m)
        return 0;
    if (nlist[a][b][c] == '#')
        return 0;
    nlist[a][b][c] = '#';
    ans++;
    ans += dfs(a + 1, b, c);
    ans += dfs(a - 1, b, c);
    ans += dfs(a, b + 1, c);
    ans += dfs(a, b - 1, c);
    ans += dfs(a, b, c + 1);
    ans += dfs(a, b, c - 1);
    return ans;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> k >> n >> m;

    vector<string> block(n);
    for (int i = 0; i < k; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            cin >> block[j];
        }
        nlist.push_back(block);
    }
    int x, y;
    cin >> x >> y;
    x--;
    y--;

    cout << dfs(0, x, y);
}
