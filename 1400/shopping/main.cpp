#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, k;
    cin >> n >> m >> k;

    vector<int> klist(k);
    for (int i = 0; i < k; ++i)
    {
        cin >> klist[i];
    }
    reverse(klist.begin(), klist.end());
    int ans = 0;
    while (n--)
    {
        for (int o = 0; o < m; ++o)
        {
            int order;
            cin >> order;
            for (int i = 0; i < k; ++i)
            {
                if (order == klist[k - i - 1])
                {
                    ans += i + 1;
                    klist.push_back(klist[k - i - 1]);
                    klist.erase(klist.begin() + k - i - 1);
                    break;
                }
            }
        }
    }
    cout << ans << endl;
}
