#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    vector<bool> stat(1e6 + 5, 0);
    vector<int> freq(1e6 + 5, 0);
    int filledpos[1000005] = {0};
    int numppl = 0;
    vector<int> ans;
    int last = 0;
    int pos = 0;
    for (int i = 0; i < n; ++i)
    {
        int id = nlist[i];
        if (id < 0)
        {
            filledpos[pos++] = -id;
            id = -id;
            // cout << freq[id] << " ";
            if (stat[id] == 0)
            {
                cout << -1 << endl;
                return 0;
            }
            if (freq[id] > 2)
            {
                cout << -1 << endl;
                return 0;
            }
            stat[id] = 0;
            freq[id]++;
            --numppl;
            if (numppl < 0)
            {
                cout << -1 << endl;
                return 0;
            }
        }
        else
        {
            if (stat[id])
            {
                cout << -1 << endl;
                return 0;
            }
            if (freq[id] > 2)
            {
                cout << -1 << endl;
                return 0;
            }
            stat[id] = 1;
            freq[id]++;
            ++numppl;
        }
        // cout << numppl << endl;
        if (numppl == 0)
        {
            ans.push_back(i + 1 - last);
            last = i + 1;
            // stat.clear();
            // freq.clear();
            // fill(stat.begin(), stat.end(), 0);
            for (int j = 0; j < pos; ++j)
            {
                freq[filledpos[j]] = 0;
            }
        }
    }
    if (numppl != 0)
    {
        cout << -1 << endl;
        return 0;
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i << " ";
    }
}
