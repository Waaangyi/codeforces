#include "bits/stdc++.h"

#define ll long long

using namespace std;
#define PI 3.14159265

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        n *= 2;

        double anglesum = (n - 2) * 180;
        double bigangle = anglesum / n;
        double angle = 180 - bigangle;
        angle /= 2;
        printf("%.9f\n", 2 * .5 / tan(angle * PI / 180));
    }
}
