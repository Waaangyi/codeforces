#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    int s = 0;
    cin >> s;
    vector<int> alist(s);
    for (int i = 0; i < s; ++i)
    {
        cin >> alist[i];
    }
    cin >> s;
    vector<int> blist(s);
    for (int i = 0; i < s; ++i)
    {
        cin >> blist[i];
    }

    reverse(alist.begin(), alist.end());
    reverse(blist.begin(), blist.end());
    int it = 0;
    while (true)
    {
        if (it > 5e6)
        {
            cout << -1 << endl;
            return 0;
        }
        if (alist.empty())
        {
            cout << it << " " << 2 << endl;
            return 0;
        }
        if (blist.empty())
        {
            cout << it << " " << 1 << endl;
            return 0;
        }
        ++it;
        int a = alist.back(), b = blist.back();
        alist.pop_back();
        blist.pop_back();
        if (a > b)
        {
            alist.insert(alist.begin(), b);
            alist.insert(alist.begin(), a);
        }
        else
        {
            blist.insert(blist.begin(), a);
            blist.insert(blist.begin(), b);
        }
    }
}
