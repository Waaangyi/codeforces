#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    vector<int> sortedlist = nlist;
    sort(sortedlist.begin(), sortedlist.end());
    vector<set<int>> ans;
    for (int i = 0; i < n; ++i)
    {
        if (nlist[i] == sortedlist[i])
        {
            ans.push_back(set<int>());
            ans.back().insert(i);
        }
    }

    for (int i = 0; i < n; ++i)
    {
        if (nlist[i] != sortedlist[i])
        {
            ans.push_back(set<int>());
            while (nlist[i] != sortedlist[i])
            {
                auto pos =
                    lower_bound(sortedlist.begin(), sortedlist.end(), nlist[i]);
                ans.back().insert(i);
                ans.back().insert(pos - sortedlist.begin());
                swap(nlist[i], nlist[pos - sortedlist.begin()]);
            }
        }
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i.size() << " ";
        for (auto j : i)
        {
            cout << j + 1 << " ";
        }
        cout << endl;
    }
}
