#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll p, d;
    cin >> p >> d;

    ll temp = p + 1;
    ll mult = 10;
    ll ans = temp;
    while (temp % mult <= d)
    {
        ans = temp - temp % mult;
        mult *= 10;
    }
    cout << ans - 1 << endl;
}