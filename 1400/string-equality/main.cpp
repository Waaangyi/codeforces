#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n, k;
        cin >> n >> k;

        string a, b;
        cin >> a >> b;
        vector<int> alist(27, 0);
        vector<int> blist(27, 0);
        for (int i = 0; i < a.size(); ++i)
        {
            ++alist[a[i] - 'a'];
            ++blist[b[i] - 'a'];
        }
        bool ok = 1;
        for (int i = 0; i < 26; ++i)
        {
            if (alist[i] < blist[i] || (alist[i] -= blist[i]) % k)
            {
                ok = 0;
                break;
            }
            alist[i + 1] += alist[i];
        }
        if (ok)
        {
            cout << "YES" << endl;
        }
        else
            cout << "NO" << endl;
    }
}
