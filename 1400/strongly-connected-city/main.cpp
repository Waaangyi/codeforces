#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    string a, b;
    cin >> a >> b;
    bool flag = 1;
    if (a[0] == '<' && b[0] == '^')
    {
        flag = 0;
    }
    if (a[a.length() - 1] == '<' && b[0] == 'v')
    {
        flag = 0;
    }
    if (a[0] == '>' && b[b.length() - 1] == '^')
    {
        flag = 0;
    }
    if (a[a.length() - 1] == '>' && b[b.length() - 1] == 'v')
    {
        flag = 0;
    }
    if (flag)
        cout << "YES";
    else
        cout << "NO";
}
