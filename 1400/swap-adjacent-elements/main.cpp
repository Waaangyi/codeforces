#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, correct = 0, now = 0;
    char ch;
    vector<char> result;

    cin >> n;

    for (int i = 0; i < n; i++)
    {
        int tmp;

        cin >> tmp;
        correct += i + 1;
        now += tmp;

        if (now != correct)
            result.push_back('1');
        else
            result.push_back('0');
    }

    for (int i = 0; i < n - 1; i++)
    {
        cin >> ch;
        if (result[i] == '1' && ch == '0')
        {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;
}
