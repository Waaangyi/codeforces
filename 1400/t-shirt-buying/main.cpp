#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> p(n), a(n), b(n);

    for (int i = 0; i < n; i++)
    {
        cin >> p[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> a[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> b[i];
    }
    vector<bool> used(n, false);

    vector<vector<pair<int, int>>> plist(4);

    for (int i = 0; i < n; i++)
    {
        plist[a[i]].push_back({p[i], i});
        plist[b[i]].push_back({p[i], i});
    }
    for (int i = 1; i <= 3; i++)
    {
        sort(plist[i].begin(), plist[i].end());
        reverse(plist[i].begin(), plist[i].end());
    }
    int m;
    cin >> m;
    vector<int> c(m), ans(m, -1);
    for (int i = 0; i < m; i++)
    {
        cin >> c[i];

        while (!plist[c[i]].empty())
        {
            int x = plist[c[i]].back().second;
            if (used[x])
                plist[c[i]].pop_back();
            else
            {
                ans[i] = p[x];
                used[x] = true;
                plist[c[i]].pop_back();
                break;
            }
        }
    }

    for (auto p : ans)
        cout << p << " ";
    cout << "\n";

    return 0;
}
