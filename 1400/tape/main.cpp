#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, k;
    cin >> n >> m >> k;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    vector<int> dlist;
    for (int i = 0; i < n - 1; ++i)
    {
        dlist.push_back(nlist[i + 1] - nlist[i] - 1);
    }
    int ans = n;

    sort(dlist.begin(), dlist.end());
    for (int i = 0; i < n - k; ++i)
    {
        ans += dlist[i];
    }
    cout << ans << endl;
}
