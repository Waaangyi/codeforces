#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int s, b;
    cin >> s >> b;

    vector<pair<int, int>> slist(s);
    for (int i = 0; i < s; ++i)
    {
        cin >> slist[i].first;
        slist[i].second = i;
    }

    vector<pair<int, int>> blist(b);
    for (int i = 0; i < b; ++i)
    {
        cin >> blist[i].first >> blist[i].second;
    }

    sort(blist.begin(), blist.end());
    sort(slist.begin(), slist.end());
    vector<int> ans(s);
    int lastpos = 0;
    int lastans = 0;
    for (int i = 0; i < s; ++i)
    {
        while (blist[lastpos].first <= slist[i].first && lastpos < b)
        {
            lastans += blist[lastpos].second;
            lastpos++;
        }

        ans[slist[i].second] = lastans;
    }
    for (auto i : ans)
    {
        cout << i << " ";
    }
}
