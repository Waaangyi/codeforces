#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;
    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    vector<string> bestans;
    int len = INT_MAX;
    for (int i = 0; i < n; ++i)
    {
        vector<string> ans;
        vector<int> blist = nlist;
        bool abort = 0;
        for (int j = i - 1; j >= 0; --j)
        {
            int normal = blist[j + 1] - k;
            if (blist[j] == normal)
                continue;
            if (blist[j] < normal)
            {
                ans.push_back("+ " + to_string(j + 1) + " " +
                              to_string(normal - blist[j]));
            }
            if (blist[j] > normal)
            {
                ans.push_back("- " + to_string(j + 1) + " " +
                              to_string(abs(normal - blist[j])));
            }
            if (normal <= 0)
            {
                abort = 1;
                break;
            }
            blist[j] = normal;
        }
        for (int j = i + 1; j < n; ++j)
        {
            int normal = blist[j - 1] + k;

            if (blist[j] == normal)
                continue;
            if (blist[j] < normal)
            {
                ans.push_back("+ " + to_string(j + 1) + " " +
                              to_string(normal - blist[j]));
            }
            if (blist[j] > normal)
            {
                ans.push_back("- " + to_string(j + 1) + " " +
                              to_string(abs(normal - blist[j])));
            }
            if (normal <= 0)
            {
                abort = 1;
                break;
            }
            blist[j] = normal;
        }
        if (!abort)
            if (len > ans.size())
            {
                len = ans.size();
                bestans = ans;
            }
    }
    cout << bestans.size() << endl;
    for (auto i : bestans)
    {
        cout << i << endl;
    }
}
