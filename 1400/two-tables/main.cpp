#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int na, ma;
    cin >> na >> ma;

    vector<vector<bool>> nlist(na);
    for (int i = 0; i < na; ++i)
    {
        for (int j = 0; j < ma; ++j)
        {
            char a;
            cin >> a;
            nlist[i].push_back(a == '1');
        }
    }

    int nb, mb;
    cin >> nb >> mb;

    vector<vector<bool>> mlist(nb);

    for (int i = 0; i < nb; ++i)
    {
        for (int j = 0; j < mb; ++j)
        {
            char a;
            cin >> a;
            mlist[i].push_back(a == '1');
        }
    }
    int bestx = 0, besty = 0;
    int maxo = 0;
    for (int i = -50; i <= 50; ++i)
    {
        for (int j = -50; j <= 50; ++j)
        {
            int tempo = 0;
            for (int k = 0; k < na; ++k)
            {
                for (int l = 0; l < ma; ++l)
                {
                    if (nlist[k][l])
                    {
                        if (k + i >= 0 && k + i < nb)
                            ;
                        else
                            continue;
                        if (l + j >= 0 && l + j < mb)
                            ;
                        else
                            continue;
                        if (mlist[k + i][l + j])
                            tempo++;
                    }
                }
            }
            if (tempo > maxo)
            {
                bestx = i;
                besty = j;
                maxo = tempo;
            }
        }
    }
    cout << bestx << " " << besty;
}
