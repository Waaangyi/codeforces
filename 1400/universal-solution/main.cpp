#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        int n = s.length();

        int a, b, c;
        a = b = c = 0;
        for (int i = 0; i < s.length(); ++i)
        {
            if (s[i] == 'R')
                ++a;
            if (s[i] == 'P')
                ++b;
            if (s[i] == 'S')
                ++c;
        }

        int high = max({a, b, c});
        if (a == high)
        {
            string ans(n, 'P');
            cout << ans << endl;
            continue;
        }
        if (b == high)
        {
            string ans(n, 'S');
            cout << ans << endl;
            continue;
        }
        if (c == high)
        {
            string ans(n, 'R');
            cout << ans << endl;
            continue;
        }
    }
}
