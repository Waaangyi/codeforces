#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k, l, r, sall, sk;
    cin >> n >> k >> l >> r >> sall >> sk;

    int left = sall - sk;
    vector<int> ans;
    for (int i = 0; i < k; i++)
    {
        ans.push_back(sk / k);
        if (sk % k != 0)
        {
            ans.back()++;
            sk--;
        }
    }
    for (int i = k; i < n; i++)
    {
        ans.push_back(left / (n - k));
        if (left % (n - k) != 0)
        {
            ans.back()++;
            left--;
        }
    }
    for (auto i : ans)
    {
        cout << i << " ";
    }
}