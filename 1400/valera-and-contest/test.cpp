#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    long i = 0;
    clock_t test = clock();
    //cout << clock() << endl;
    while ((clock() - test) / (float)CLOCKS_PER_SEC <= 2000)
    {
        i++;
    }
    cout << i << endl;
}