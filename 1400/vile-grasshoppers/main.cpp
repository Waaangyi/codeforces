#include <bits/stdc++.h>

#define ll long long

using namespace std;

int p, y;
bool check(int x)
{
    int i = 1;
    for (int i = 1; i * i <= x; i++)
    {
        if (x % i == 0)
        {
            if (i != 1 && i <= p)
            {
                return false;
            }
            if (x / i != i)
            {
                if (x / i != 1 && x / i <= p)
                {
                    //cerr << x / i << endl;
                    return false;
                }
            }
        }
    }
    return true;
}
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> p >> y;

    for (int i = y; i > y - 1000; i--)
    {
        if (i == 2)
            break;
        if (i % 2)
        {
            if (check(i))
            {
                cout << i << endl;
                return 0;
            }
        }
    }
    cout << -1 << endl;
}