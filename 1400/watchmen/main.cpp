#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, int>> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }

    map<int, ll> xmap;
    map<int, ll> ymap;
    map<pair<int, int>, ll> repeat;
    for (int i = 0; i < n; ++i)
    {
        ++xmap[nlist[i].first];
        ++ymap[nlist[i].second];
        ++repeat[nlist[i]];
    }
    ll ans = 0;
    for (auto i : xmap)
    {
        ans += i.second * (i.second - 1) / 2;
    }
    for (auto i : ymap)
    {
        ans += i.second * (i.second - 1) / 2;
    }

    for (auto i : repeat)
    {
        ans -= i.second * (i.second - 1) / 2;
    }
    cout << ans << endl;
}
