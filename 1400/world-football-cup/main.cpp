#include "bits/stdc++.h"

using namespace std;
vector<string> parsename(string s)
{
    string one = "";
    int i = 0;
    while (s[i] != '-')
    {
        one += s[i];
        ++i;
    }
    ++i;
    string two = "";
    while (s[i] != ' ')
    {
        two += s[i];
        ++i;
    }
    vector<string> ans(2);
    ans[0] = one;
    ans[1] = two;
    return ans;
}
vector<int> parsescore(string s)
{
    int i = 0;
    while (s[i] != ' ')
    {
        ++i;
    }
    ++i;
    string one = "";
    while (s[i] != ':')
    {
        one += s[i];
        ++i;
    }
    ++i;
    string two = "";
    // cout << one << endl << two << endl;
    while (i < s.size())
    {
        two += s[i];
        ++i;
    }
    vector<int> ans(2);
    ans[0] = stoi(one);
    ans[1] = stoi(two); return ans; 
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    map<string, int> score;
    for (int i = 0; i < n; ++i)
    {
        string s;
        cin >> s;
        score[s] = 0;
    }
    for (int i = 0; i < n * (n - 1) / 2; ++i)
    {
        string s;
        cin >> s;
        string s1;
        getline(cin, s1);
        s += s1;

        vector<string> team = parsename(s);
        vector<int> sc = parsescore(s);
        if (sc[0] == sc[1])
        {
            score[team[0]] += 1;
            score[team[1]] += 1;
        }
        if (sc[0] > sc[1])
            score[team[0]] += 3;
        if (sc[0] < sc[1])
            score[team[1]] += 3;
    }
    vector<pair<int, string> > final;
    for (auto i : score)
    {
        cout << i.first << " " << i.second << endl;
        final.push_back(make_pair(i.second, i.first));
    }
    sort(final.rbegin(), final.rend());
    vector<string> ans;
    for (int i = 0; i < n / 2; ++i)
    {
        ans.push_back(final[i].second);
    }
    sort(ans.begin(), ans.end());
    for(int i = 0; i < n / 2; ++i)
    {
        cout << ans[i] << endl;
    }
}
