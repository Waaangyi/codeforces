#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        ll n, k;
        cin >> n >> k;
        //vector<int> nlist(n);
        map<int, int> nmap;
        int ma = 0;
        for (int i = 0; i < n; i++)
        {
            int a = 0;
            cin >> a;
            if (a % k == 0)
                continue;
            nmap[k - a % k]++;
            ma = max(ma, nmap[k - a % k]);
        }
        ll ans = 0;
        for (auto i : nmap)
        {
            if (i.second == ma)
            {
                ans = k * (ll)(i.second - 1) + i.first + 1;
            }
        }
        cout << ans << endl;
    }
}