#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    vector<int> cnt(m + 1, 0);
    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        cnt[a]++;
    }
    vector<int> mlist(m);
    for (int i = 0; i < m; ++i)
    {
        cin >> mlist[i];
    }
    sort(mlist.rbegin(), mlist.rend());
}
