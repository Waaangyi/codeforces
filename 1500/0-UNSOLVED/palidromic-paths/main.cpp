#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, m;
        cin >> n >> m;

        vector<vector<int>> board(n);
        for (auto &i : board)
        {
            for (int j = 0; j < m; ++j)
            {
                int a;
                cin >> a;
                i.push_back(a);
            }
        }
    }
}
