#include "bits/stdc++.h"

#define ll long long

using namespace std;

string s;
int dfs(int start, int end, char c)
{
    // base
    if (end - start == 1)
    {
        if (s[start] != c)
            return 1;
        return 0;
    }
    int mid = (end + start) / 2;
    int c1 = 0, c2 = 0;
    c1 = dfs(start, mid, c + 1);
    for (int i = mid; i < end; ++i)
    {
        if (s[i] != c)
        {
            ++c1;
        }
    }
    c2 = dfs(mid, end, c + 1);
    for (int i = start; i < mid; ++i)
    {
        if (s[i] != c)
        {
            ++c2;
        }
    }
    return min(c1, c2);
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        cin >> s;
        int ans = dfs(0, s.size(), 'a');
        cout << ans << endl;
    }
}
