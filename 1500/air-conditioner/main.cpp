#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, m;
        cin >> n >> m;

        vector<pair<int, pair<int, int>>> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i].first >> nlist[i].second.first >>
                nlist[i].second.second;
        }
        int t = 0;
        bool flag = 1;
        int upper = m, lower = m;
        for (int i = 0; i < n; ++i)
        {
            upper += abs(t - nlist[i].first);
            lower -= abs(t - nlist[i].first);
            if (upper < nlist[i].second.first || lower > nlist[i].second.second)
            {
                flag = 0;
                break;
            }
            t = nlist[i].first;
            upper = min(upper, nlist[i].second.second);
            lower = max(lower, nlist[i].second.first);
        }
        if (!flag)
        {
            cout << "NO" << endl;
        }
        else
            cout << "YES" << endl;
    }
}
