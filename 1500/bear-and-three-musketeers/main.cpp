#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;
    map<int, set<int>> nmap;
    bool matrix[n + 5][n + 5];
    for (int i = 0; i < n + 5; ++i) {
        for (int j = 0; j < n + 5; ++j) {
            matrix[i][j] = 0;
        }
    }
    for (int i = 0; i < m; ++i) {
        int a, b;
        cin >> a >> b;
        matrix[a][b] = 1;
        matrix[b][a] = 1;
        nmap[a].insert(b);
        nmap[b].insert(a);
    }
    int ans = INT_MAX;
    for (int i = 1; i < n + 1; ++i) {
        for (int j = 1; j < n + 1; ++j) {
            if (matrix[i][j]) {
                for (int k = j + 1; k < n + 1; ++k) {
                    if (matrix[i][k] && matrix[j][k]) {
                        ans = min(ans, int(nmap[i].size() + nmap[j].size() +
                                           nmap[k].size() - 6));
                    }
                }
            }
        }
    }
    if (ans == INT_MAX) {
        cout << -1 << endl;
    } else {
        cout << ans << endl;
    }
}
