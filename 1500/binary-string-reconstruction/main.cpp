#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        int x;
        cin >> x;

        int n = s.size();

        vector<bool> slist(n, 0);
        for (int i = 0; i < n; ++i)
        {
            slist[i] = '1' == s[i];
        }

        vector<pair<bool, bool>> ans(n, {0, 0});
        bool ok = 1;
        for (int i = 0; i < n; ++i)
        {
            if (slist[i] == 0)
            {
                if (i - x >= 0)
                {
                    if (ans[i - x].first == 1 && ans[i - x].second == 1)
                    {
                        ok = 0;
                        break;
                    }
                    ans[i - x].first = 0;
                    ans[i - x].second = 1;
                }
                if (i + x < n)
                {
                    if (ans[i + x].first == 1 && ans[i + x].second == 1)
                    {
                        ok = 0;
                        break;
                    }
                    ans[i + x].first = 0;
                    ans[i + x].second = 1;
                }
            }
            else
            {
                bool lexist = 0;
                bool rexist = 0;
                if (i - x < 0 && i + x >= n)
                {
                    ok = false;
                    break;
                }
                if (i - x >= 0)
                {
                    if (ans[i - x].first == 0 && ans[i - x].second == 1)
                    {
                    }
                    else
                    {
                        lexist = 1;
                        ans[i - x].first = 1;
                    }
                }
                if (i + x < n)
                {
                    if (ans[i + x].first == 0 && ans[i + x].second == 1)
                    {
                    }
                    else
                    {
                        rexist = 1;
                        ans[i + x].first = 1;
                    }
                }
                if (!lexist && !rexist)
                {
                    ok = 0;
                    break;
                }
                if (lexist != rexist)
                {
                    if (lexist)
                    {
                        ans[i - x].second = 1;
                    }
                    else
                    {
                        ans[i + x].second = 1;
                    }
                }
            }
        }
        if (!ok)
        {
            cout << -1 << endl;
        }
        else
        {
            for (int i = 0; i < n; ++i)
            {
                cout << ans[i].first;
            }
            cout << endl;
        }
    }
}
