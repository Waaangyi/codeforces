#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cout << 1 << endl << 200000 << endl;
    srand(time(0));

    for (int i = 0; i < 200000; ++i)
    {
        if (rand() % 1000 > 122)
            cout << 1 << " ";
        else
            cout << 0 << " ";
    }
}
