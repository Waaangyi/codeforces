#include "bits/stdc++.h"

#define ll long long

using namespace std;
int n, x;
ll dp[100500], a[100500], ans;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
    {
        scanf("%d", &x);
        a[x]++;
    }
    for (int i = 1; i <= 100000; i++)
    {
        a[i] = i * a[i];
    }
    dp[1] = a[1];
    for (int i = 2; i <= 100000; i++)
    {
        dp[i] = dp[i - 1];
        if (dp[i - 2] + a[i] > dp[i])
        {
            dp[i] = dp[i - 2] + a[i];
        }
    }
    for (int i = 1; i <= 100000; i++)
    {
        if (dp[i] > ans)
        {
            ans = dp[i];
        }
    }
    printf("%lld\n", ans);
    return 0;
}
