#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<string> nlist(n);
    multiset<pair<int, int>> fblist;
    map<int, int> fbmap;
    int good = 0;
    for (int i = 0; i < n; ++i) {
        string s;
        cin >> s;
        // https://codeforces.com/contest/1097/problem/C
        stack<char> eval;
        for (int j = 0; j < s.size(); ++j) {
            if (eval.size() > 0 && eval.top() == '(' && s[j] == ')') {
                eval.pop();
            } else {
                eval.push(s[j]);
            }
        }
        string evalstr(eval.size(), 0);
        for (int i = 0; i < evalstr.size(); ++i) {
            evalstr[evalstr.size() - 1 - i] = eval.top();
            eval.pop();
        }
        s = evalstr;
        if (s.size() == 0) {
            ++good;
        } else {
            if (!(s.front() == ')' && s.back() == '(')) {
                int f = 0, b = 0;
                for (int j = 0; j < s.size(); ++j) {
                    if (s[j] == '(') ++f;
                    if (s[j] == ')') ++b;
                }
                assert(f == 0 || b == 0);
                fblist.insert({f, b});
                ++fbmap[b];
            }
        }
    }
    ll ans = pow(good, 2);
    for (auto i : fblist) {
        // only allowed to add if f != 0
        // cout << i.first << " " << i.second << endl;
        if (i.first != 0) {
            ll matchcnt = fbmap[i.first];
            ans += matchcnt;
        }
    }
    cout << ans << endl;
}
