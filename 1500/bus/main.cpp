#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<pair<int, int>> nlist(n);

    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
    }
    int ltime = 0;
    int ans = 0;
    int ppl = 0;
    int d = 0;
    int laststudent = 0;
    // map<int, int> stops;
    set<int> checklist;
    vector<int> stops(1e5 + 10, 0);
    vector<int> temp(1e5 + 10, 0);
    for (int i = 0; i < n; ++i)
    {
        stops[nlist[i].second]++;
        checklist.insert(nlist[i].second);
        ppl++;
        if (nlist[i].first > ans)
            ans += nlist[i].first - ans;
        ltime = nlist[i].first;
        d = max(d, nlist[i].second);
        if (ppl == m || i == n - 1)
        {
            int last = 0;
            for (auto j : checklist)
            {
                // ans += j.first - last;
                ans += j - last;
                last = j;
                // last = j.first;
                temp[j] = ans;
                // temp[j.first] = ans;
                ans += 1 + stops[j] / 2;
                stops[j] = 0;
                // ans += 1 + j.second / 2;
            }
            for (int k = laststudent; k <= i; ++k)
            {
                cout << temp[nlist[k].second] << " ";
            }
            checklist.clear();
            ans += d;
            d = 0;
            ppl = 0;
            laststudent = i + 1;
        }
    }
}
