#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll k;
    cin >> k;

    vector<int> freq(10, 1);
    int p = 0;
    while (true)
    {
        ll test = 1;
        bool flag = 0;
        for (int i = 0; i < 10; ++i)
        {
            test *= freq[i];
            if (test >= k)
            {
                flag = 1;
                break;
            }
        }
        if (flag)
            break;

        ++freq[p];
        ++p;

        if (p == 10)
            p = 0;
    }
    string ans = "codeforces";
    for (int i = 0; i < 10; ++i)
    {
        for (int j = 0; j < freq[i]; ++j)
        {
            cout << ans[i];
        }
    }
}
