#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, a, b, c;
    cin >> n >> a >> b >> c;
    int ans = 0;
    for (int i = 0; i <= a; i += 2)
    {
        for (int j = 0; j <= b; ++j)
        {
            int left = (n - i / 2 - j);
            if (left < 0)
                break;
            if (left % 2)
                continue;
            else if (left / 2 <= c)
                ++ans;
        }
    }
    cout << ans << endl;
}
