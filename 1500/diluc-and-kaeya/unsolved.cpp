#include "bits/stdc++.h"

#define ll long long

using namespace std;
set<int> factor(int n) {
    set<int> ans;
    for (int i = 1; i * i <= n; ++i) {
        if (n % i == 0) {
            ans.insert(i);
            if (n / i != i) ans.insert(n / i);
        }
    }
    return ans;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        string s;
        cin >> n >> s;
        s = " " + s;
        vector<pair<int, int>> rlist(n + 5, {0, 0});
        // dcnt, kcnt
        vector<bool> oklist(n + 5, 1);
        vector<bool> toberevived(n + 5, 0);
        vector<int> Dprefixsum;
        vector<int> Kprefixsum;
        Dprefixsum.push_back(0);
        Kprefixsum.push_back(0);
        for (int i = 1; i <= n; ++i) {
            Dprefixsum.push_back(Dprefixsum[i - 1] + (s[i] == 'D'));
            Kprefixsum.push_back(Kprefixsum[i - 1] + (s[i] == 'K'));
        }
        /*
        for (int i = 0; i < Dprefixsum.size(); ++i) {
            cout << Dprefixsum[i] << " " << Kprefixsum[i] << endl;
        }
        */
        for (int i = 1; i <= n; ++i) {
            int ans = i;
            set<int> updatelist = factor(i);
            for (int j : updatelist) {
                if (j == *updatelist.rbegin()) {
                    int dcnt = Dprefixsum[i] - Dprefixsum[i - j];
                    int kcnt = Kprefixsum[i] - Kprefixsum[i - j];
                    assert(dcnt == Dprefixsum[i]);
                    assert(kcnt == Kprefixsum[i]);
                    int gcd = __gcd(dcnt, kcnt);
                    dcnt /= gcd;
                    kcnt /= gcd;
                    rlist[dcnt + kcnt] = {dcnt, kcnt};
                    // second chance
                    if (!oklist[dcnt + kcnt] && dcnt * gcd + kcnt * gcd == i) {
                        cout << "revived" << i << endl;
                        rlist[dcnt + kcnt] = {dcnt, kcnt};
                        toberevived[dcnt + kcnt] = 1;
                    }
                    // revive
                    if (!oklist[dcnt + kcnt] &&
                        rlist[dcnt + kcnt] == make_pair(dcnt, kcnt) &&
                        toberevived[dcnt + kcnt]) {
                        oklist[dcnt + kcnt] = 1;
                        toberevived[dcnt + kcnt] = 0;
                    }
                    if (oklist[dcnt + kcnt]) {
                        ans = min(ans, dcnt + kcnt);
                    }
                    break;
                }
                int dcnt = Dprefixsum[i] - Dprefixsum[i - j];
                int kcnt = Kprefixsum[i] - Kprefixsum[i - j];
                int gcd = __gcd(dcnt, kcnt);
                dcnt /= gcd;
                kcnt /= gcd;
                // cout << i << " " << dcnt << " " << kcnt << endl;
                // cout << rlist[j].first << " " << rlist[j].second << endl;
                if (rlist[dcnt + kcnt].first != dcnt ||
                    rlist[dcnt + kcnt].second != kcnt) {
                    oklist[dcnt + kcnt] = 0;
                    toberevived[dcnt + kcnt] = 0;
                }
                // second chance
                if (!oklist[dcnt + kcnt] && dcnt * gcd + kcnt * gcd == i) {
                    cout << "revived" << i << endl;
                    rlist[dcnt + kcnt] = {dcnt, kcnt};
                    toberevived[dcnt + kcnt] = 1;
                }
                // revive
                if (!oklist[dcnt + kcnt] &&
                    rlist[dcnt + kcnt] == make_pair(dcnt, kcnt) &&
                    toberevived[dcnt + kcnt]) {
                    oklist[dcnt + kcnt] = 1;
                    toberevived[dcnt + kcnt] = 0;
                }

                if (oklist[dcnt + kcnt]) {
                    ans = min(ans, dcnt + kcnt);
                }
            }
            if (i == 8)
                cout << rlist[4].first << " " << rlist[4].second << endl;
        }
        cout << endl;
    }
}
