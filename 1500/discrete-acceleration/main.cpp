#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        double l;
        cin >> n >> l;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
        }

        int speed1 = 1, speed2 = 1;
        double pos1 = 0, pos2 = l;
        double collisionpos = l / 2;
        double ans = 0;
        int li = 0, ri = n - 1;
        while (true) {
            if (li > ri) {
                // cout << pos1 << " " << pos2 << endl;
                ans += (pos2 - pos1) / (speed1 + speed2);
                break;
            }
            int a = nlist[li];
            int b = nlist[ri];
            assert(pos1 <= pos2);
            // calculate which car will get to a point faster
            double dista = abs(a - pos1);
            double distb = abs(b - pos2);
            double timea = dista / speed1;
            double timeb = distb / speed2;
            if (min(dista, distb) > pos2 - pos1) {
                // cout << pos1 << " " << pos2 << endl;
                ans += (pos2 - pos1) / (speed1 + speed2);
                break;
            }
            assert(li <= ri);
            if (timea < timeb) {
                ans += timea;
                pos1 = a;
                pos2 -= timea * speed2;
                ++speed1;
                ++li;
            } else if (timea > timeb) {
                ans += timeb;
                pos1 += timeb * speed1;
                pos2 = b;
                ++speed2;
                --ri;
            } else {
                ans += timea;
                pos1 += timea * speed1;
                pos2 -= timea * speed2;
                ++speed1;
                ++speed2;
                ++li;
                --ri;
            }
        }
        printf("%.15f\n", ans);
    }
}
