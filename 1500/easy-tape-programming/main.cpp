#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, q;
    cin >> n >> q;

    string line;
    cin >> line;

    while (q--)
    {
        int l, r;
        cin >> l >> r;
        string s = line.substr(l - 1, r - l + 1);

        vector<int> ans(10, 0);

        int dir = 1;
        int cur = 0;
        while (true)
        {
            if (cur >= s.size() || cur < 0)
            {
                break;
            }
            if (s[cur] == '>')
            {
                dir = 1;
                cur += dir;
                if (cur >= s.size())
                    break;
                if (cur >= s.size() || cur < 0)
                {
                    break;
                }
                else
                {
                    if (s[cur] == '<' || s[cur] == '>')
                    {
                        s.erase(s.begin() + cur - 1);
                    }
                    if (s.size() == 1)
                        break;
                }
                continue;
            }
            if (s[cur] == '<')
            {
                dir = -1;
                cur += dir;
                if (cur >= s.size() || cur < 0)
                {
                    break;
                }
                if (cur < 0)
                    break;
                else
                {
                    if (s[cur] == '<' || s[cur] == '>')
                    {
                        s.erase(s.begin() + cur + 1);
                    }
                    if (s.size() == 1)
                        break;
                }
                continue;
            }
            ans[s[cur] - '0']++;
            s[cur]--;
            if (s[cur] < '0')
            {
                s.erase(s.begin() + cur);
                if (dir == 1)
                    cur--;
            }
            cur += dir;
            if (cur >= s.size() || cur < 0)
            {
                break;
            }
        }
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
