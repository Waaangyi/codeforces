#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    set<pair<int, int>> nlist;
    set<int> a, b;
    for (int i = 0; i < 8; ++i)
    {
        int a1, b1;
        cin >> a1 >> b1;
        nlist.insert(make_pair(a1, b1));
        a.insert(a1);
        b.insert(b1);
    }
    auto at = a.begin(), bt = b.begin();
    at++, bt++;
    if (a.size() != 3 || b.size() != 3 || nlist.size() != 8 ||
        nlist.find(make_pair(*at, *bt)) != nlist.end())
        cout << "ugly";
    else
        cout << "respectable";
}
