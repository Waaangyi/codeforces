#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int c, d, n, m, k;
    cin >> c >> d >> n >> m >> k;

    int a = n * m - k;
    if (a <= 0)
    {
        cout << 0;
        return 0;
    }

    if (n * d <= c)
    {
        cout << d * a;
    }
    else
        cout << a / n * c + min(c, (a % n) * d);
}
