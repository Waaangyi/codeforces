#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m, c;
    cin >> n >> m >> c;

    vector<int> nlist(n), mlist(m);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    for (int i = 0; i < m; ++i)
    {
        cin >> mlist[i];
    }
    int current = 0;
    for (int i = 0; i < n; ++i)
    {
        if (i < m)
            current += mlist[i];
        if (i > n - m)
            current -= mlist[i - n + m - 1];
        nlist[i] += current;
        nlist[i] %= c;
    }
    for (auto i : nlist)
    {
        cout << i << " ";
    }
}
