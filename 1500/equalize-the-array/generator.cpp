#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    vector<int> out;
    vector<int> cnt = {1, 3, 4, 7, 10};
    vector<int> freq = {1, 2, 3, 4, 5};

    int a = 1;
    for (int i = 0; i < cnt.size(); ++i)
    {
        for (int j = 0; j < freq[i]; ++j)
        {
            for (int k = 0; k < cnt[i]; ++k)
            {
                out.push_back(a);
            }
            ++a;
        }
    }
    cout << 1 << endl << out.size() << endl;
    for (auto i : out)
    {
        cout << i << " ";
    }
}
