#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    vector<vector<string>> races;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<string> temp(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> temp[i];
        }
        races.push_back(temp);
    }
    map<string, int> points;
    vector<map<string, int>> place(60);
    const vector<int> stable{25, 18, 15, 12, 10, 8, 6, 4, 2, 1};
    int hp = 0;
    for (int i = 0; i < races.size(); ++i)
    {
        for (int j = 0; j < races[i].size(); ++j)
        {
            if (j <= 9)
            {
                points[races[i][j]] += stable[j];
                hp = max(hp, points[races[i][j]]);
            }
            place[j][races[i][j]]++;
        }
    }
    set<string> winners;
    for (auto i : points)
    {
        if (i.second == hp)
        {
            winners.insert(i.first);
        }
    }
    if (winners.size() == 1)
    {
        cout << *winners.begin() << endl;
    }
    else
    {
        for (int i = 0; i < place.size(); ++i)
        {
            vector<pair<string, int>> temp;
            int score = 0;
            for (auto j : place[i])
            {
                if (winners.count(j.first))
                {
                    temp.push_back(make_pair(j.first, j.second));
                    score = max(score, j.second);
                }
            }
            for (int i = 0; i < temp.size(); ++i)
            {
                if (temp[i].second != score)
                {
                    winners.erase(temp[i].first);
                }
            }
            if (winners.size() == 1)
            {
                cout << *winners.begin() << endl;
                break;
            }
        }
    }
    winners.clear();
    {
        int score = 0;
        int i = 0;
        for (auto j : place[i])
        {
            score = max(score, j.second);
        }
        for (auto j : place[i])
        {
            if (j.second == score)
                winners.insert(j.first);
        }
        if (winners.size() == 1)
        {
            cout << *winners.begin() << endl;
            return 0;
        }
    }
    for (int i = 1; i < place.size(); ++i)
    {
        int score = 0;
        for (auto j : winners)
        {
            score = max(score, place[i][j]);
            cout << j << " ";
        }
        cout << endl;
        for (auto j : winners)
        {
            if (place[i][j] != score)
                winners.erase(j);
        }
        if (winners.size() == 1)
        {
            cout << *winners.begin() << endl;
            return 0;
        }
    }
}
