#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, x;
        cin >> n >> x;
        int degree = 0;
        for (int i = 1; i < n; ++i) {
            int a, b;
            cin >> a >> b;
            if (a == x || b == x) {
                ++degree;
            }
        }
        if (degree <= 1) {
            cout << "Ayush" << endl;
            continue;
        }
        if (n % 2) {
            cout << "Ashish" << endl;
        } else {
            cout << "Ayush" << endl;
        }
    }
}
