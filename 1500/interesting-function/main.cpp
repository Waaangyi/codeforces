#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int l, r;
        cin >> l >> r;
        string sr = to_string(r);
        string sl = to_string(l);
        while (sr.size() != sl.size()) {
            sl = char(0) + sl;
        }
        int ans = 0;
        int prevint = 0;
        for (int i = 0; i < sr.size(); ++i) {
            if (sl[i] == 0) {
                ans += (sr[i] - '0') + prevint * 10;
            } else {
                ans += (sr[i] - sl[i]) + prevint * 10;
            }
            prevint *= 10;
            prevint += (sr[i] - ((sl[i] != 0) ? sl[i] : '0'));
        }
        cout << ans << endl;
    }
}
