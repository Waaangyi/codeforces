#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        string s;
        cin >> n >> k >> s;
        vector<vector<int>> cost(k, vector<int>(26, 0));
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < 26; ++j)
            {
                if (s[i] - 'a' != j)
                {
                    ++cost[i % k][j];
                }
            }
        }
        int ans = 0;
        for (int i = 0; i < (cost.size()) / 2; ++i)
        {
            int temp = 1e9;
            for (int j = 0; j < 26; ++j)
            {
                temp = min(temp, cost[i][j] + cost[cost.size() - 1 - i][j]);
            }
            ans += temp;
        }
        if (cost.size() % 2)
        {
            int temp = 1e9;
            for (int j = 0; j < 26; ++j)
            {
                temp = min(temp, cost[cost.size() / 2][j]);
            }
            ans += temp;
        }
        cout << ans << endl;
    }
}
