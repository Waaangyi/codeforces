#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, l, r;
    cin >> n >> l >> r;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    vector<int> blist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> blist[i];
    }
    for (int i = 0; i < l - 1; ++i)
    {
        if (nlist[i] != blist[i])
        {
            cout << "LIE";
            return 0;
        }
    }
    for (int i = n - 1; i >= r; --i)
    {
        if (nlist[i] != blist[i])
        {
            cout << "LIE";
            return 0;
        }
    }
    multiset<int> a, b;
    for (int i = l - 1; i < r; ++i)
    {
        a.insert(nlist[i]);
        b.insert(blist[i]);
    }
    if (a == b)
    {
        cout << "TRUTH";
    }
    else
        cout << "LIE";
}
