#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, q;
    cin >> n >> q;

    vector<ll> nlist(n);
    for (auto &i : nlist)
    {
        cin >> i;
    }

    sort(nlist.rbegin(), nlist.rend());
    vector<ll> qlist(n);
    for (ll i = 0; i < q; ++i)
    {
        ll l, r;
        cin >> l >> r;

        --l;
        --r;
        ++qlist[l];
        if (r - 1 < n)
            --qlist[r + 1];
    }
    ll val = 0;
    vector<ll> ans(n);
    for (ll i = 0; i < n; ++i)
    {
        val += qlist[i];
        ans[i] = val;
    }

    sort(ans.rbegin(), ans.rend());
    ll total = 0;
    for (ll i = 0; i < n; ++i)
    {
        total += (ans[i] * nlist[i]);
    }
    cout << total;
}
