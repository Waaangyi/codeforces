#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist;
        for (int i = 0; i < n; ++i)
        {
            int a;
            cin >> a;
            nlist.push_back(a);
            if (i)
            {
                if (nlist.back() == -1 && nlist[nlist.size() - 2] == -1)
                {
                    nlist.pop_back();
                }
            }
        }
        n = nlist.size();
        if (n == 1)
        {
            cout << 0 << " " << 0 << endl;
            continue;
        }
        int h, l;
        h = 0;
        l = INT_MAX;
        if (nlist.front() == -1)
        {
            h = l = nlist[1];
        }
        if (nlist.back() == -1)
        {
            h = max(nlist[nlist.size() - 2], h);
            l = min(nlist[nlist.size() - 2], l);
        }
        for (int i = 1; i < nlist.size() - 1; ++i)
        {
            if (nlist[i] == -1)
            {
                h = max(nlist[i - 1], h);
                h = max(nlist[i + 1], h);

                l = min(nlist[i - 1], l);
                l = min(nlist[i + 1], l);
            }
        }
        int opt = (h + l) / 2;
        int high = 0;
        for (int i = 0; i < nlist.size(); ++i)
        {
            if (nlist[i] == -1)
            {
                nlist[i] = opt;
            }
            if (i != 0)
            {
                high = max(high, abs(nlist[i] - nlist[i - 1]));
            }
        }
        cout << high << " " << opt << endl;
    }
}
