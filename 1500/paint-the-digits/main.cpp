#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);

        vector<set<int>> plist(15);
        vector<int> ans(n, 2);
        for (int i = 0; i < n; ++i) {
            char c;
            cin >> c;
            nlist[i] = c - '0';
            plist[nlist[i]].insert(i);
        }
        int pos = -1;
        bool ok = 1;
        for (int i = 0; i < 10; ++i) {
            while (plist[i].size()) {
                auto it = plist[i].upper_bound(pos);
                if (it != plist[i].end()) {
                    pos = *it;
                    ans[pos] = 1;
                    plist[i].erase(it);
                } else {
                    ok = 0;
                    break;
                }
            }
            if (!ok) {
                break;
            }
        }
        string s1 = "", s2 = "";
        for (int i = 0; i < n; ++i) {
            if (ans[i] == 1) {
                s1 += to_string(nlist[i]);
            } else {
                s2 += to_string(nlist[i]);
            }
        }
        string s3 = s1 + s2, s4 = s1 + s2;
        sort(s4.begin(), s4.end());
        if (s3 != s4) {
            cout << "-" << endl;
            continue;
        }
        for (int i = 0; i < ans.size(); ++i) {
            cout << ans[i];
        }
        cout << endl;
    }
}
