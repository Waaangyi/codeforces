#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, m;
        cin >> n >> m;

        vector<vector<int>> board(n, vector<int>(m, 0));
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cin >> board[i][j];
            }
        }

        int steps = n + m - 1;
        int ans = 0;
        for (int i = 0; i < steps / 2; ++i) {
            int x = i, y = 0;

            vector<int> cnt(2, 0);
            while (x >= 0) {
                if (x < n && y < m) {
                    ++cnt[board[x][y]];
                }
                --x;
                ++y;
            }
            y = m - 1;
            x = n - 1 - i;
            while (x < n) {
                if (x >= 0 && y >= 0) {
                    ++cnt[board[x][y]];
                }
                ++x;
                --y;
            }
            ans += min(cnt[0], cnt[1]);
        }
        cout << ans << endl;
    }
}
