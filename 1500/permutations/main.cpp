#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    const int limit = 1e5 + 10;
    int n;
    cin >> n;
    vector<int> nlist(n);
    vector<int> flist(limit, 0);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
        ++flist[nlist[i] - 1];
    }

    int t = flist.front();

    for (int i = 1; i < limit; ++i)
    {
        if (flist[i] > t)
        {
            cout << -1;
            return 0;
        }
        t = flist[i];
    }
    cout << flist.front();
    vector<int> num(limit, 0);
    vector<int> ans;
    cout << endl;
    for (int i = 0; i < n; ++i)
    {
        num[nlist[i] - 1]++;
        ans.push_back(num[nlist[i] - 1]);
    }
    for (auto i : ans)
    {
        cout << i << " ";
    }
}
