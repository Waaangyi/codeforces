#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> alist(n);
    vector<int> blist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> alist[i];
    }
    for (int i = 0; i < n; ++i)
    {
        cin >> blist[i];
    }

    vector<pair<int, int>> ans;
    for (int i = 0; i < n; ++i)
    {
        if (alist[i] != blist[i])
        {
            int position = -1;
            for (int j = i; j < n; ++j)
            {
                if (blist[j] == alist[i])
                {
                    position = j;
                    break;
                }
            }

            while (alist[i] != blist[i])
            {
                swap(blist[position - 1], blist[position]);
                ans.push_back(make_pair(position, position + 1));
                position--;
            }
        }
    }
    cout << ans.size() << endl;
    for (auto i : ans)
    {
        cout << i.first << " " << i.second << endl;
    }
}
