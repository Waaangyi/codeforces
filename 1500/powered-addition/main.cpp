#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        int high = INT_MIN;
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }
        int d = 0;
        for (int i = 0; i < n; ++i)
        {
            high = max(high, nlist[i]);
            d = max(d, high - nlist[i]);
        }
        int ans = 0;
        while (d)
        {
            d /= 2;
            ++ans;
        }
        cout << ans << endl;
    }
}
