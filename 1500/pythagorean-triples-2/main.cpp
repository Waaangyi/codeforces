#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--) {
        ll n;
        cin >> n;
        ll temp = sqrt(n * 2) - 1;
        cout << temp / 2 << endl;
    }
}
