#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;
    if (n < 3) {
        cout << -1 << endl;
        return 0;
    }
    n = n * n;
    if (n % 2 == 1) {
        cout << n / 2 << " " << n / 2 + 1 << endl;
    } else {
        cout << n / 4 - 1 << " " << n / 4 + 1 << endl;
    }
}
