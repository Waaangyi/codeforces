#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<string> nlist(n);
    vector<int> colonlist(n, 0);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
        for (int j = 0; j < nlist[i].size(); ++j)
        {
            if (nlist[i][j] == ':')
                ++colonlist[i];
        }
    }
    vector<vector<string>> ans;
    for (int i = 0; i < n; ++i)
    {
        string sp = "";
        int cnt = 0;
        vector<string> temp(8, "0000");
        for (int j = 0; j < nlist[i].size(); ++j)
        {
            if (nlist[i][j] != ':')
            {
                sp += nlist[i][j];
            }
            else
            {
                temp[cnt++] = sp;
                sp = "";
                if (nlist[i][j + 1] == ':')
                    break;
            }
        }
        cnt = 7;
        sp = "";
        for (int j = nlist[i].size() - 1; j >= 0; --j)
        {
            if (nlist[i][j] != ':')
            {
                sp += nlist[i][j];
            }
            else
            {
                reverse(sp.begin(), sp.end());
                temp[cnt--] = sp;
                sp = "";
                if (nlist[i][j - 1] == ':')
                    break;
            }
        }
        ans.push_back(temp);
    }
    for (int i = 0; i < ans.size(); ++i)
    {
        for (int j = 0; j < ans[i].size(); ++j)
        {
            for (int k = 0; k < 4 - ans[i][j].size(); ++k)
            {
                cout << 0;
            }
            cout << ans[i][j];
            if (j + 1 == ans[i].size())
                break;
            cout << ":";
        }
        cout << endl;
    }
}
