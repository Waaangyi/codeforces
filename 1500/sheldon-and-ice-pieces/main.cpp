#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string str;
    cin >> str;
    vector<int> num;
    for (int i = 0; i < str.size(); ++i)
    {
        num.push_back(str[i] - '0');
    }
    int n = num.size();

    vector<int> nlist(10, 0);
    {
        string s;
        cin >> s;
        for (int i = 0; i < s.size(); ++i)
        {
            ++nlist[s[i] - '0'];
        }
    }

    int ans = 0;
    while (true)
    {
        bool ok = 1;
        vector<int> templist = nlist;
        for (int i = 0; i < n; ++i)
        {
            if (templist[num[i]])
            {
                templist[num[i]]--;
            }
            else
            {
                ok = false;
                break;
            }
        }
        if (!ok)
            break;
        nlist = templist;
        ++ans;
    }
    vector<int> replacement{0, 1, 5, 3, 4, 2, 9, 7, 8, 6};
    while (true)
    {
        bool ok = 1;
        for (int i = 0; i < n; ++i)
        {
            if (nlist[num[i]])
            {
                nlist[num[i]]--;
            }
            else
            {
                if (nlist[replacement[num[i]]])
                {
                    nlist[replacement[num[i]]]--;
                }
                else
                {
                    ok = false;
                    break;
                }
            }
        }
        if (!ok)
            break;
        ++ans;
    }
    cout << ans;
}
