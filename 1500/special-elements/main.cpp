#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int &i : nlist)
        {
            cin >> i;
        }

        int ans = 0;
        for (int i = 0; i < n; ++i)
        {
            int sum = 0;
            int head = 0, tail = 0;
            sum += nlist[head];
            bool found = 0;
            while (tail < i)
            {
                if (sum < nlist[i])
                {
                    ++tail;
                    if (tail >= n)
                        break;
                    sum += nlist[tail];
                }
                else
                {
                    sum -= nlist[head];
                    ++head;
                }
                if (sum == nlist[i] && tail - head >= 1)
                {
                    ++ans;
                    // cout << nlist[i] << " " << head << " " << tail << endl;
                    found = true;
                    break;
                }
                if (tail >= n)
                    break;
            }
            if (found)
                continue;
            if (i + 1 == n)
                break;
            head = i + 1, tail = i + 1;
            sum = nlist[head];
            while (true)
            {
                if (sum < nlist[i])
                {
                    ++tail;
                    if (tail >= n)
                        break;
                    sum += nlist[tail];
                }
                else
                {
                    sum -= nlist[head];
                    ++head;
                }
                if (sum == nlist[i] && tail - head >= 1)
                {
                    ++ans;
                    // cout << nlist[i] << " " << head << " " << tail << endl;
                    break;
                }
                if (tail >= n)
                    break;
            }
        }
        cout << ans << endl;
    }
}
