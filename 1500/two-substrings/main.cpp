#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;
    if (s.length() < 4)
    {
        cout << "NO" << endl;
        return 0;
    }
    vector<int> ab;
    vector<int> ba;
    for (int i = 0; i < s.length() - 1; ++i)
    {
        if (s[i] == 'A' && s[i + 1] == 'B')
        {
            ab.push_back(i);
        }
        if (s[i] == 'B' && s[i + 1] == 'A')
        {
            ba.push_back(i);
        }
    }
    if (ab.size() == 0 || ba.size() == 0)
    {
        cout << "NO" << endl;
        return 0;
    }
    for (int i = 0; i < ab.size(); ++i)
    {
        for (int j = 0; j < ba.size(); ++j)
        {
            if (abs(ab[i] - ba[j]) >= 2)
            {
                cout << "YES" << endl;
                return 0;
            }
        }
    }
    cout << "NO" << endl;
}
