#include "bits/stdc++.h"

#define ll long long

using namespace std;

int n, a[1001], maxn = -2e9, sum;
string s[1001];
map<string, int> t1, t2;
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        cin >> s[i] >> a[i];
        t1[s[i]] += a[i];
    }
    for (int i = 1; i <= n; i++)
        maxn = max(maxn, t1[s[i]]);
    for (int i = 1; i <= n; i++)
    {
        t2[s[i]] += a[i];
        if (maxn == t1[s[i]] && maxn <= t2[s[i]])
        {
            cout << s[i];
            return 0;
        }
    }
}
