#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<pair<int, int>> nlist(n);
    for (auto &i : nlist)
    {
        cin >> i.first >> i.second;
    }

    if (n <= 2)
    {
        cout << n;
        return 0;
    }
    vector<pair<int, int>> flist;
    flist.push_back(make_pair(nlist.front().first, -1));
    int ans = 2;
    for (int i = 1; i < n - 1; ++i)
    {
        int l = nlist[i].first - nlist[i].second;
        int r = nlist[i].first + nlist[i].second;
        int leftbound = 0;
        if (flist.back().first == nlist[i - 1].first)
            if (flist.back().second == -1)
            {
                leftbound = flist.back().first;
            }
            else
            {
                leftbound = nlist[i - 1].first + nlist[i - 1].second;
            }
        else
        {
            leftbound = nlist[i - 1].first;
        }

        if (l > leftbound)
        {
            flist.push_back(make_pair(nlist[i].first, -1));
            ++ans;
            continue;
        }
        if (r < nlist[i + 1].first)
        {
            flist.push_back(make_pair(nlist[i].first, 1));
            ++ans;
        }
    }
    cout << ans;
}
