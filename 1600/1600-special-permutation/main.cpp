#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    for(int t = 0; t < tc; t++)
    {
        int value;
        cin >> value;
        if(value >= 4)
        {
            vector<int> numlist;
            for(int i = 0; i < value; i++)
            {
                numlist.push_back(i + 1);
            }
            for(int i = numlist.size() - 1; i >= 0; i--)
            {
                if(numlist[i] % 2 == 1)
                    cout << numlist[i] << " ";
            }
            cout << 4 << " " << 2 << " ";
            for(int i = 5; i < numlist.size(); i++)
            {
                if(numlist[i] % 2 == 0)
                    cout << numlist[i] << " ";
            }
            cout << endl;
        }
        else
        {
            cout << "-1\n";
        }
    }
}
