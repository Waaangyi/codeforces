#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    vector<int> ans(n);
    for (int i = 0; i < n / 2; ++i) {
        cout << "? " << i + 1 << " " << n - i << endl;
        cout.flush();
        int a, b;
        cin >> a;
        cout << "? " << n - i << " " << i + 1 << endl;
        cout.flush();
        cin >> b;
        if (a > b) {
            ans[i] = a;
        } else {
            ans[n - i - 1] = b;
        }
    }
    vector<int> unknownpos;
    bool bignumber = 0;
    int bignumpos = 0;
    int bignum = 0;
    for (int i = 0; i < n; ++i) {
        if (ans[i] > n / 2) {
            bignumber = 1;
            bignum = ans[i];
            bignumpos = i;
        }
        if (ans[i] == 0) {
            unknownpos.push_back(i);
        }
    }
    int maxmod = (n + 1) / 2 - 1;
    if (bignumber) {
        for (int i = 0; i < unknownpos.size(); ++i) {
            cout << "? " << bignumpos + 1 << " " << unknownpos[i] + 1 << endl;
            cout.flush();
            int mod;
            cin >> mod;
            if (mod == bignum) {
                cout << "? " << unknownpos[i] + 1 << " " << bignumpos + 1
                     << endl;
                cout.flush();
                cin >> mod;
                ans[unknownpos[i]] = bignum + mod;
            } else {
                cout << "? " << unknownpos[i] + 1 << " " << bignumpos + 1
                     << endl;
                cout.flush();
                cin >> mod;
                ans[unknownpos[i]] = mod;
            }
        }
        goto printans;
    }
    ans[unknownpos.front()] = n;
    for (int i = 1; i < unknownpos.size(); ++i) {
        cout << "? " << unknownpos.front() + 1 << " " << unknownpos[i] + 1
             << endl;
        cout.flush();
        int mod;
        cin >> mod;
        if (mod > maxmod) {
            ans[unknownpos.front()] = mod;
            break;
        }
    }

    for (int i = 1; i < unknownpos.size(); ++i) {
        cout << "? " << unknownpos[i] + 1 << " " << unknownpos.front() + 1
             << endl;
        cout.flush();
        int mod;
        cin >> mod;
        if (mod > maxmod) {
            ans[unknownpos[i]] = mod;
        } else {
            ans[unknownpos[i]] = ans[unknownpos.front()] + mod;
        }
    }
printans:
    cout << "! ";
    for (int i = 0; i < n; ++i) {
        cout << ans[i] << " ";
    }
    cout.flush();
}
