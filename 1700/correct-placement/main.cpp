#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<pair<int, int>> nlist(n);
        set<pair<int, int>> nset;
        map<pair<int, int>, int> table;
        vector<int> ans(n, -1);
        vector<pair<int, int>> ulist;
        vector<pair<int, int>> rnlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i].first >> nlist[i].second;
            table[nlist[i]] = i;
            nset.insert(nlist[i]);
            rnlist[i].first = nlist[i].second;
            rnlist[i].second = nlist[i].first;
        }
        for (auto i : nset)
        {
            ulist.push_back(i);
        }
        vector<pair<int, int>> smallest;
        smallest.push_back(ulist.front());
        for (int i = 0; i < ulist.size(); ++i)
        {
            if (smallest.back().first != ulist[i].first)
            {
                smallest.push_back(ulist[i]);
            }
        }
        vector<int> smallest_f(smallest.size());
        vector<pair<int, int>> smallest_s(smallest.size());
        smallest_s[0].first = smallest[0].second;
        smallest_s[0].second = 0;
        for (int i = 1; i < smallest.size(); ++i)
        {
            smallest_f[i] = smallest[i].first;
            if (smallest[i].second < smallest_s[i - 1].first)
            {
                smallest_s[i].second = i;
                smallest_s[i].first = smallest[i].second;
            }
            else
            {
                smallest_s[i].second = smallest_s[i - 1].second;
                smallest_s[i].first = smallest_s[i - 1].first;
            }
        }

        sort(rnlist.begin(), rnlist.end());
        vector<pair<int, int>> rsmallest;
        rsmallest.push_back(rnlist.front());
        for (int i = 0; i < rnlist.size(); ++i)
        {
            if (rsmallest.back().first != rnlist[i].first)
            {
                rsmallest.push_back(rnlist[i]);
            }
        }
        vector<pair<int, int>> rsmallest_s(rsmallest.size());
        vector<int> rsmallest_f(rsmallest.size());
        rsmallest_s[0].first = rsmallest[0].second;
        rsmallest_s[0].second = 0;
        for (int i = 1; i < rsmallest.size(); ++i)
        {
            rsmallest_f[i] = rsmallest[i].first;
            if (rsmallest[i].second < rsmallest_s[i - 1].first)
            {
                rsmallest_s[i].second = i;
                rsmallest_s[i].first = rsmallest[i].second;
            }
            else
            {
                rsmallest_s[i].second = rsmallest_s[i - 1].second;
                rsmallest_s[i].first = rsmallest_s[i - 1].first;
            }
        }

        for (int i = 0; i < n; ++i)
        {
            {
                auto it = lower_bound(smallest_f.begin(), smallest_f.end(),
                                      nlist[i].first);
                if (it != smallest_f.begin())
                {
                    it = prev(it);
                    int index = it - smallest_f.begin();
                    if (smallest[index].first < nlist[i].first)
                    {
                        if (smallest_s[index].first < nlist[i].second)
                        {
                            ans[i] =
                                table[smallest[smallest_s[index].second]] + 1;
                        }
                    }
                }
            }
            {
                auto it = lower_bound(rsmallest_f.begin(), rsmallest_f.end(),
                                      nlist[i].first);
                if (it != rsmallest_f.begin())
                {
                    it = prev(it);

                    int index = it - rsmallest_f.begin();
                    if (rsmallest[index].first < nlist[i].first)
                    {
                        if (rsmallest_s[index].first < nlist[i].second)
                        {
                            ans[i] =
                                table[{
                                    rsmallest[rsmallest_s[index].second].second,
                                    rsmallest[rsmallest_s[index].second]
                                        .first}] +
                                1;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < n; ++i)
        {
            cout << ans[i] << " ";
        }
        cout << endl;
    }
}

