#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    
    int tc; 
    cin >> tc;
    while(tc--)
    {
        int len, elen;
        cin >> len >> elen;

        int evenct = 0, oddct = 0;
        for(int i = 0; i < len; i++)
        {
            int a;
            cin >> a;
            if(a % 2 == 0)
                evenct++;
            else
                oddct++;
        }
        if(oddct == 0)
        {
            cout << "No" << endl;
            continue;
        }
        if(elen % 2 == 0 && evenct == 0)
        {
            cout << "No" << endl;
            continue;
        }
        if(evenct >= elen && oddct != 0)
        {
            cout << "Yes" << endl;
            continue;
        }
        int left = elen - evenct;

        if(left % 2 == 1)
        {
            cout << "Yes" << endl;
            continue;
        }
        if(left + 1 <= oddct)
        {
            cout << "Yes" << endl;
            continue;
        }
        cout << "No" << endl;

    }
}
