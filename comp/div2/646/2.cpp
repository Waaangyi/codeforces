#include <bits/stdc++.h>

using namespace std;

int main()
{
    //ios_base::sync_with_stdio(false);
    //cin.tie(nullptr);
    
    int tc;
    cin >> tc;

    while(tc--)
    {
        string line;
        cin >> line;

        int zs = 0, os = 0;

        for(int i = 0; i < line.length(); i++)
        {
            if(line[i] == '1')
                os++;
            else
                zs++;
        }
        bool o = false;
        bool changed = false;
        if(line[0] == '1')
            o = true;
        bool ok = true;
        for(int i = 0; i < line.length(); i++)
        {
            if(o)
            {
                if(line[i] == '1' && changed)
                {
                    ok = false;
                    break;
                }
                if(line[i] == '0' && !changed)
                    changed = true;
            }
            else
            {
                if(line[i] == '0' && changed)
                {
                    ok = false;
                    break;
                }
                if(line[i] == '1' && !changed)
                    changed = true;
            }
        }
        if(ok)
            cout << 0 << endl;
        else
            cout << min(os,zs) << endl;
    }
}
