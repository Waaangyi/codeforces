#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        string s;
        cin >> s;
        int cnt = 0;
        int zcnt = 0;
        string fp = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == '0' && cnt)
            {
                while (s[i] == '0')
                {
                    zcnt++;
                    i++;
                    if (i == s.length())
                        break;
                }
                if (zcnt > 1)
                {
                    s.erase(s.begin() + i - zcnt, s.begin() + i - 1);
                    i -= zcnt;
                }
                zcnt = 0;
            }
            if (s[i] == '1')
            {
                cnt++;
            }
            else
            {
                cnt = 0;
            }
        }
        int lastz = s.rfind('0');
        for (int i = 0; i < s.length(); i++)
        {
            if (i != 0 && s[i] == '0' && s[i - 1] == '1' && lastz != i)
            {
                s[i] = '1';
            }
        }
        for (int i = 0; i < s.length(); i++)
        {

            if (s[i] == '1')
            {
                int pos = i;
                while (s[i] == '1')
                {
                    i++;
                    if (i == s.length())
                        break;
                }
                if (i == s.length() && s[i] != '0')
                    break;

                s.erase(s.begin() + pos, s.begin() + i);
                i = pos;
            }
        }
        cout << s << endl;
    }
}