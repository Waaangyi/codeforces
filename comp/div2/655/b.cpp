#include <bits/stdc++.h>

#define ll long long

using namespace std;
vector<ll> factor(ll x)
{
    vector<ll> result;
    for (ll i = 1; i * i <= x; i++)
    {
        if (x % i == 0)
        {
            result.push_back(i);
            if (x / i != i)
            {
                result.push_back(x / i);
            }
        }
    }
    return result;
}
ll gcd(ll a, ll b)
{
    if (a == 0)
        return b;
    return gcd(b % a, a);
}
ll lcm(ll a, ll b) { return (a * b) / gcd(a, b); }

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--)
    {
        ll n;
        cin >> n;
        if (n % 2 == 0)
        {
            cout << n / 2 << " " << n / 2 << endl;
        }
        else
        {
            vector<ll> f = factor(n);
            ll mi = 9223372036854775807;
            ll a = 0;
            ll b = 0;
            for (auto i : f)
            {
                if (i != n)
                {
                    ll t = lcm(i, n - i);
                    if (t < mi)
                    {
                        mi = t;
                        a = i;
                        b = n - i;
                    }
                }
            }
            cout << a << " " << b << endl;
        }
    }
}
