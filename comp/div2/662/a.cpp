#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        cerr << endl;
        if (n == 3)
        {
            cout << 2 << endl;
            continue;
        }
        if (n <= 2)
        {
            cout << n << endl;
            continue;
        }
        if (n % 2 == 0)
        {
            cout << n / 2 + 1 << endl;
        }
        else
        {
            if ((n + 1) % 4 == 0)
            {
                cout << (n + 1) / 2 + 1 << endl;
            }
            else
            {
                cout << (n - 1) / 2 + 1 << endl;
            }
        }
    }
}
