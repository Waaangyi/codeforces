#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    map<int, int> nmap;

    for (int i = 0; i < n; ++i)
    {
        int a;
        cin >> a;
        nmap[a]++;
    }

    int q;
    cin >> q;

    while (q--)
    {
        char c;
        int a;
        cin >> c >> a;

        if (c == '+')
        {
            nmap[a]++;
        }
        else
            nmap[a]--;
    }
}
