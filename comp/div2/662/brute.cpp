#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<vector<int>> nlist(n, vector<int>(n, 9));
    int steps = 1;
    for (int i = 0; i < n; ++i)
    {
        nlist[0][i] = i % 2;
    }
    for (int i = 0; i < n; ++i)
    {
        if (n % 2)
        {
            nlist[n - 1][i] = i % 2;
        }
        else
            nlist[n - 1][i] = !(i % 2);
    }
    for (int i = 1; i < n; ++i)
    {
        nlist[i][0] = i % 2;
    }
    for (int i = 1; i < n; ++i)
    {
        if (n % 2)
        {
            nlist[i][n - 1] = i % 2;
        }
        else
            nlist[i][n - 1] = !(i % 2);
    }
    bool state = 1;
    while (true)
    {
        steps++;
        for (int i = 1; i < n - 1; ++i)
        {
            for (int j = 1; j < n - 1; ++j)
            {
                if (nlist[i][j] == 9)
                {
                    if (nlist[i - 1][j] == !state ||
                        nlist[i][j - 1] == !state ||
                        nlist[i + 1][j] == !state || nlist[i][j + 1] == !state)
                    {
                        nlist[i][j] = state;
                    }
                }
            }
        }
        state = !state;
        // for (auto i : nlist)
        //{
        //    for (auto j : i)
        //    {
        //        cout << j << " ";
        //    }
        //    cout << endl;
        //}
        // cout << endl;

        bool complete = true;
        for (auto i : nlist)
        {
            for (auto j : i)
            {
                if (j == 9)
                {
                    complete = 0;
                    break;
                }
            }
        }
        if (complete)
            break;
    }
    // for (auto i : nlist)
    //{
    //    for (auto j : i)
    //    {
    //        cout << j << " ";
    //    }
    //    cout << endl;
    //}
    cout << endl << steps << endl;
}
