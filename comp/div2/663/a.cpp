#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n, 0);
        int i = 1;
        int loc = 1;
        while (loc < n)
        {
            nlist[loc] = i;
            i++;
            loc += 2;
        }
        loc = 0;
        while (loc < n)
        {
            nlist[loc] = i;
            i++;
            loc += 2;
        }
        for (auto i : nlist)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
