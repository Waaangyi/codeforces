#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, m;
        cin >> n >> m;
        vector<string> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        if (n == 1 && m == 1)
        {
            cout << 0 << endl;
            continue;
        }
        int ans = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < m; ++j)
            {
                int x = i, y = j;
                while (nlist[x][y] != 'C')
                {
                    // cout << nlist[x][y] << endl;
                    if (nlist[x][y] == 'R')
                    {
                        y++;
                        if (y >= m)
                        {
                            y--;
                            ans++;
                            nlist[x][y] = 'D';
                        }
                    }
                    if (nlist[x][y] == 'L')
                    {
                        y--;
                        if (y < 0)
                        {
                            y++;
                            ans++;
                            nlist[x][y] = 'R';
                        }
                    }
                    if (nlist[x][y] == 'U')
                    {
                        x--;
                        if (x < 0)
                        {
                            x++;
                            ans++;
                            nlist[x][y] = 'R';
                        }
                    }
                    if (nlist[x][y] == 'D')
                    {
                        x++;
                        if (x >= n)
                        {
                            x--;
                            ans++;
                            nlist[x][y] = 'R';
                        }
                    }
                    // cout << x << " " << y << " " << nlist[x][y] << endl;
                }
            }
        }
        cout << ans << endl;
    }
}
