#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        nlist[i] = i + 1;
    }
    int cnt = 0;
    do
    {
        for (auto i : nlist)
        {
            cout << i << " ";
        }
        cnt++;
        cout << endl;
    } while (next_permutation(nlist.begin(), nlist.end()));
    cout << cnt << endl;
}
