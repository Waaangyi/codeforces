#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<ll> nlist(n);
    for (ll i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }
    sort(nlist.begin(), nlist.end());
    ll ans = 0;
    ans += nlist.front() - 1;
    ll minans = 1e18;
    ll powerval = 0;
    while (true)
    {
        bool ok = 1;
        ans = nlist.front() - 1;
        ll target = 1;
        for (ll i = 1; i < n; ++i)
        {
            if (target > 1e15)
            {
                cout << minans << endl;
                return 0;
            }
            target *= powerval;
            if (target > nlist.back() || target < 0)
            {
                ok = 0;
            }

            ans += min(abs(target - nlist[i]), abs(target - nlist[i]));
        }
        minans = min(ans, minans);
        if (!ok)
            break;
        ++powerval;
    }
    cout << minans << endl;
}
