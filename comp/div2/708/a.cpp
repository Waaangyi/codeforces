#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
        }
        sort(nlist.begin(), nlist.end());

        vector<pair<int, int>> compress;
        compress.push_back({nlist.front(), 1});
        for (int i = 1; i < n; ++i) {
            if (compress.back().first == nlist[i])
                ++compress.back().second;
            else
                compress.push_back({nlist[i], 1});
        }
        while (true) {
            bool ok = 0;
            for (int i = 0; i < compress.size(); ++i) {
                if (compress[i].second != 0) {
                    ok = 1;
                    cout << compress[i].first << " ";
                    --compress[i].second;
                }
            }
            if (!ok) break;
        }
        cout << endl;
    }
}
