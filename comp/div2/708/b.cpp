#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, m;
        cin >> n >> m;
        vector<int> nlist(n);
        vector<int> rlist(m + 100, 0);
        for (int i = 0; i < n; ++i) {
            int a;
            cin >> a;
            nlist[i] = a % m;
            ++rlist[nlist[i]];
        }

        int ans = 0;
        if (rlist[0] != 0) ++ans;
        int i = 1;
        n -= rlist[0];
        while (n > 0) {
            if (i == m - i) {
                ++ans;
                n -= rlist[i];
                ++i;
                rlist[i] = 0;
                continue;
            }
            int maxCommon = min(rlist[i], rlist[m - i]);
            if (maxCommon > 0) {
                ++ans;
                n -= maxCommon * 2;
                rlist[i] -= maxCommon;
                rlist[m - i] -= maxCommon;
                if (rlist[m - i] > 0) {
                    --rlist[m - i];
                    --n;
                } else {
                    if (rlist[i] > 0) {
                        --rlist[i];
                        --n;
                    }
                }
            }
            ans += rlist[i];
            ans += rlist[m - i];
            n -= rlist[i];
            n -= rlist[m - i];
            rlist[i] = 0;
            rlist[m - i] = 0;
            ++i;
        }
        cout << ans << endl;
    }
}
