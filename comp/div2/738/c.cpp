#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<bool> nlist(n);
        for (int i = 0; i < n; ++i) {
            int a;
            cin >> a;
            nlist[i] = a;
        }
        if (nlist[0] == 1) {
            cout << n + 1 << " ";
            for (int i = 0; i < n; ++i) {
                cout << i + 1 << " ";
            }
            cout << endl;
            continue;
        }
        if (nlist.back() == 0) {
            for (int i = 0; i < n + 1; ++i) {
                cout << i + 1 << " ";
            }
            cout << endl;
            continue;
        }
        int ok = -1;
        for (int i = 0; i < n - 1; ++i) {
            if (nlist[i] == 0 && nlist[i + 1] == 1) {
                ok = i;
                break;
            }
        }
        if (ok != -1) {
            for (int i = 0; i <= ok; ++i) {
                cout << i + 1 << " ";
            }
            cout << n + 1 << " ";
            for (int i = ok + 1; i < n; ++i) {
                cout << i + 1 << " ";
            }
            cout << endl;
            continue;
        }
        cout << -1 << endl;
    }
}
