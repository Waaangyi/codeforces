#include "bits/stdc++.h"

#define ll long long

using namespace std;
const int mod = 1e9 + 7;
ll powermod(ll n, int p) {
    ll ans = 1;
    for (int i = 0; i < p; ++i) {
        ans *= n;
        ans %= mod;
    }
    return ans;
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int tc;
    cin >> tc;

    while (tc--) {
        ll n, k;
        cin >> n >> k;
        vector<bool> binary(32, 0);
        for (int i = 31; i >= 0; --i) {
            if (k >= pow(2, i)) {
                k -= pow(2, i);
                binary[i] = 1;
            }
        }
        ll ans = 0;
        for (int i = 0; i < 32; ++i) {
            if (binary[i]) {
                ans += powermod(n, i);
                ans %= mod;
            }
        }
        cout << ans << endl;
    }
}
