#include "bits/stdc++.h"

#define ll long long

using namespace std;

const int mod = 1e9 + 7;
ll bin_power(ll base, ll pow) {
    if (pow == 0) {
        return 1;
    }
    ll res = bin_power(base, pow / 2) % mod;

    ll ans = (res * res) % mod;
    if (pow % 2) {
        return (ans * base) % mod;
    } else {
        return ans % mod;
    }
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int n;
    cin >> n;

    ll nodes = pow(2, n) - 1;
    ll ans = bin_power(4, nodes - 1);
    ans *= 6;
    ans %= mod;
    cout << ans << endl;
}
