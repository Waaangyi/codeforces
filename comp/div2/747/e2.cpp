#include "bits/stdc++.h"

#define ll long long

using namespace std;
const int mod = 1e9 + 7;
ll bin_power(ll base, ll pow) {
    if (pow == 0) {
        return 1;
    }
    ll res = bin_power(base, pow / 2) % mod;

    ll ans = (res * res) % mod;
    if (pow % 2) {
        return (ans * base) % mod;
    } else {
        return ans % mod;
    }
}

ll get_parent(ll n) { return n / 2; }
ll get_partner(ll n) {
    if (n % 2)
        return n - 1;
    else
        return n + 1;
}
pair<ll, ll> get_children(ll n) { return {n * 2, n * 2 + 1}; }
int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int k, n;
    cin >> k >> n;
    ll refund = 0;
    ll nodes = pow(2, k) - 1;
    ll ans = 0;
    map<ll, string> nmap;
    map<ll, int> modifications;
    modifications[1] = 6;
    map<string, pair<string, string>> conflict;
    conflict["white"] = {"white", "yellow"};
    conflict["yellow"] = {"white", "yellow"};
    conflict["red"] = {"red", "orange"};
    conflict["orange"] = {"red", "orange"};
    conflict["blue"] = {"blue", "green"};
    conflict["green"] = {"blue", "green"};
    for (int i = 0; i < n; ++i) {
        ll a;
        string b;
        cin >> a >> b;
        nmap[a] = b;
        modifications[a] = 1;
    }
    // if modified check for conflicts
    // check children
    // check partner

    for (auto i : nmap) {
        // check if parent is modified
        if (nmap.find(get_parent(i.first)) != nmap.end()) {
            // if modified check for conflicts
            set<pair<string, string>> temp;
            temp.insert(conflict[i.second]);
            if (nmap.find(get_partner(i.first)) != nmap.end()) {
                temp.insert(conflict[nmap[get_partner(i.first)]]);
            }
            if (nmap.find(get_parent(get_parent(i.first))) != nmap.end()) {
                temp.insert(conflict[nmap[get_parent(get_parent(i.first))]]);
            }
            if (i.second == nmap[get_parent(i.first)]) {
                goto bad;
            }
            if (temp.count(conflict[nmap[i.first / 2]])) goto bad;
        } else {
            // if parent is not defined restrict the parent
            set<pair<string, string>> temp;
            temp.insert(conflict[i.second]);
            if (nmap.find(get_partner(i.first)) != nmap.end()) {
                temp.insert(conflict[nmap[get_partner(i.first)]]);
            }
            if (nmap.find(get_parent(get_parent(i.first))) != nmap.end()) {
                temp.insert(conflict[nmap[get_parent(get_parent(i.first))]]);
            }
            if (temp.size() == 3) goto bad;
            modifications[i.first / 2] = (3 - temp.size()) * 2;
        }
    }
    for (auto i : modifications) {
        if (i.first != 0 && i.first != 1 && i.first <= nodes) {
            if (i.second == 2) {
                refund += 1;
            }
            if (i.second == 1) {
                refund += 2;
            }
            if (i.second == 0) {
                goto bad;
            }
            // cout << i.first << " " << refund << endl;
        }
    }
    // cout << nodes << endl;
    // cout << refund << endl;
    // cout << modifications[1] << endl;
    ans = bin_power(2, (nodes - 1) * 2 - refund);
    ans *= max(1, modifications[1]);
    ans %= mod;
    cout << ans << endl;

    return 0;
bad:
    cout << 0 << endl;
}
