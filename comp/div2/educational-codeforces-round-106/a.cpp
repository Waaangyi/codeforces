#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, k1, k2, b, w;
        cin >> n >> k1 >> k2 >> b >> w;

        int commonBlack = min(k1, k2);
        bool ok = 1;
        if (commonBlack < b) {
            if (commonBlack + (k1 - commonBlack) / 2 + (k2 - commonBlack) / 2 <
                b) {
                ok = 0;
            }
        }
        if (!ok) {
            cout << "NO" << endl;
            continue;
        }

        int commonWhite = min(n - k1, n - k2);
        if (commonWhite < w) {
            if (commonWhite + (n - k1 - commonWhite) / 2 +
                    (n - k2 - commonWhite) / 2 <
                w) {
                ok = 0;
            }
        }
        if (!ok) {
            cout << "NO" << endl;
            continue;
        } else {
            cout << "YES" << endl;
        }
    }
}
