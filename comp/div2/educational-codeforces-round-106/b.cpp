#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        string s;
        cin >> s;

        if (is_sorted(s.begin(), s.end())) {
            cout << "YES" << endl;
            continue;
        }
        vector<pair<bool, int>> compress;
        compress.push_back({s.front() - '0', 1});
        for (int i = 1; i < s.size(); ++i) {
            if (compress.back().first == s[i] - '0')
                ++compress.back().second;
            else
                compress.push_back({s[i] - '0', 1});
        }
        bool ok = 1;
        for (int i = 0; i < compress.size() - 1; ++i) {
            if (compress[i].first == 1) {
                if (compress[i].second > 1) {
                    // remove the zero
                    if (compress[i + 1].second > 1) {
                        ok = 0;
                        break;
                    } else {
                        cout << compress[i + 1].first << " "
                             << compress[i + 1].second << endl;
                        ++i;
                    }
                } else {
                    cout << compress[i].first << " " << compress[i].second
                         << endl;
                }
            }
        }
        if (ok) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
