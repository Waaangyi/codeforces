
#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        string s;
        cin >> s;

        if (is_sorted(s.begin(), s.end())) {
            cout << "YES" << endl;
            continue;
        }
        vector<pair<bool, int>> compress;
        compress.push_back({s.front() - '0', 1});
        for (int i = 1; i < s.size(); ++i) {
            if (compress.back().first == s[i] - '0')
                ++compress.back().second;
            else
                compress.push_back({s[i] - '0', 1});
        }
        int lockedZero = -1;
        int lockedOne = -1;
        for (int i = 0; i < compress.size(); ++i) {
            if (lockedZero == -1) {
                if (compress[i].first == 0 && compress[i].second > 1) {
                    lockedZero = i;
                }
            }
            if (lockedOne == -1) {
                if (compress[i].first == 1 && compress[i].second > 1) {
                    lockedOne = i;
                }
            }
        }
        bool ok = 1;
        if (lockedZero == -1 && lockedOne == -1) {
            cout << "YES" << endl;
            continue;
        }
        if (lockedOne == -1) {
            cout << "YES" << endl;
            continue;
        }
        if (lockedZero > lockedOne) {
            cout << "NO" << endl;
            continue;
        }
        if (lockedZero == -1) {
            int lastindex = -2;
            for (int i = 0; i < lockedOne; ++i) {
                if (compress[i].first == 1) {
                    if (lastindex + 1 == i) {
                        ok = 0;
                        break;
                    } else {
                        lastindex = i;
                    }
                }
            }
            lastindex = -2;
            for (int i = lockedOne + 1; i < compress.size(); ++i) {
                if (compress[i].first == 0 && compress[i].second > 1) {
                    ok = 0;
                    break;
                }
                if (compress[i].first == 0) {
                    if (lastindex + 1 == i) {
                        ok = 0;
                        break;
                    } else {
                        lastindex = i;
                    }
                }
            }
        } else {
            int lastindex = -2;
            for (int i = 0; i < lockedOne; ++i) {
                if (compress[i].first == 1 && compress[i].second > 1) {
                    ok = 0;
                    break;
                }
                if (compress[i].first == 1) {
                    if (lastindex + 1 == i) {
                        ok = 0;
                        break;
                    } else {
                        lastindex = i;
                    }
                }
            }
            lastindex = -2;
            for (int i = lockedOne + 1; i < compress.size(); ++i) {
                if (compress[i].first == 0 && compress[i].second > 1) {
                    ok = 0;
                    break;
                }
                if (compress[i].first == 0) {
                    if (lastindex + 1 == i) {
                        ok = 0;
                        break;
                    } else {
                        lastindex = i;
                    }
                }
            }
        }
        if (ok) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
