#include <bits/stdc++.h>

using namespace std;

int main()
{
    int ts;
    cin >> ts;

    for(int t = 0; t < ts; t++)
    {
        int a, b;
        cin >> a >> b;

        int mi = min(a,b);
        int ma = max(a,b);

        if(mi * 2 >= ma)
        {
            cout << (mi * 2) * mi * 2 << endl;
        }
        else
            cout << ma * ma << endl;
    }
}
