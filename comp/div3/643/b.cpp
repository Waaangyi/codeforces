#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;
    for(int t = 0; t < tc; t++)
    {
        int num, s;
        cin >> num;
        vector<int> strengthar;
        for(int i = 0; i < num; i++)
        {
            cin >> s;
            strengthar.push_back(s);
        }
        int min = INT_MAX;
        for(int i = 0; i < num; i++)
        {
            for(int b = 0; b < num; b++)
            {
                if(abs(strengthar[b] - strengthar[i]) < min && b != i)
                    min = abs(strengthar[b] - strengthar[i]);
                if(min == 0)
                    break;
            }
        }
        cout << min << endl;
    }
}
