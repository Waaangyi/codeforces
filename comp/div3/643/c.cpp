#include <bits/stdc++.h>

using namespace std;

int main()
{
    int ts;
    cin >> ts;
    for(int t = 0; t < ts; t++)
    {
        int s, num;

        cin >> s;
        vector<int> numberar;
        for(int i = 0; i < s; i++)
        {
            cin >> num;
            numberar.push_back(num);
        }
        
        sort(numberar.begin(), numberar.end());
        
        int evenct = 0;
        for(int i = 0; i < s; i++)
            if(numberar[i] % 2 == 0)
                evenct++;
        bool yes = false;
        if(evenct % 2 == 0)
            cout << "YES" << endl;
        else
        {
            for(int i = 0; i < s - 1; i++)
            {
                if(numberar[i + 1] - numberar[i] == 1)
                {
                    yes = true;
                    break;
                }

            }
            if(!yes)
            cout << "NO" << endl;
            if(yes)
            cout << "YES" << endl;
        }
    
    }
}
