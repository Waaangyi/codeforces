#include <bits/stdc++.h>

using namespace std;

vector<int> factor(int n)
{
    int i = 1;
    vector<int> factors;
    while(i*i <= n)
    {
        if(n % i == 0)
        {
            factors.push_back(i);
            if(n / i != i)
                factors.push_back(n / i);
        }
        i++;
    }
    return factors;
}
int main()
{
    int tc;
    cin >> tc;
    for(int t = 0; t < tc; t++)
    {
        int n, a;
        cin >> n >> a;

        if(a == 1)
            cout << n << endl;
        else if(n == a)
            cout << 1 << endl;
        else
        {
            vector<int> factors = factor(n);
            int previous = 0;
            sort(factors.begin(), factors.end());

            for(int i = 0; i < factors.size(); i++)
                if(factors[i] <= a)
                    previous = factors[i];
                else
                    break;
            cout << n / previous << endl;
        }
    }
}
