#include <bits/stdc++.h>

using namespace std;

int main()
{
    int tc;
    cin >> tc;

    for(int t = 0; t < tc; t++)
    {
        int s;
        cin >> s;
        vector<string> board;
        string border = "";
        for(int i = 0; i < s + 2; i++)
        {
            border = border + "1";
        }
        board.push_back(border);
        for(int i = 0; i < s; i++)
        {
            string row;
            cin >> row;
            board.push_back("1" + row + "1");
        }
        board.push_back(border);
        bool possible = true;
        for(int x = s; x > 0; x--)
        {
            for(int y = s; y > 0; y--)
            {
                string row = board[x];
                string bott = board[x + 1];
                if(row[y] == '1' && row[y + 1] == '0' && row[y] == '1' && bott[y] == '0')
                {
                    possible = false;
                    break;
                }
            }
        }
        if(possible)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
}
