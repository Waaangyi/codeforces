#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        string s;
        cin >> n >> k >> s;

        vector<int> sub;
        int c = 0;
        for (int i = 0; i < n; i++)
        {
            if (s[i] == '0')
            {
                c++;
            }
            if (s[i] != '0' || i == n - 1)
            {
                if (c != 0)
                    sub.push_back(c);
                c = 0;
            }
        }
        bool f = false, b = false;
        if (s[0] == '0')
            f = true;
        if (s[n - 1] == '0')
            b = true;
        int begin = 0;
        int e = sub.size();
        if (f)
            begin++;
        if (b)
            e--;
        int ans = 0;
        if (s.find('1') == string::npos)
        {
            ans++;
            n -= k + 1;
            ans += int(ceil(n / double(k + 1)));
            cout << ans << endl;
            continue;
        }
        for (int i = begin; i < e; i++)
        {
            if (sub[i] >= k * 2 + 1)
            {
                ans += 1;
                sub[i] -= k * 2 + 1;
            }
            else
            {
                continue;
            }
            if (sub[i] >= k + 1)
                ans += sub[i] / (k + 1);
        }
        int a = 0;
        while (s[a] != '1')
        {
            if (a + k + 1 <= sub[0])
            {
                ans++;
                a += k + 1;
            }
            else
            {
                break;
            }
        }
        a = 0;
        while (s[s.size() - a - 1] != '1')
        {
            if (a + k + 1 <= sub.back())
            {
                ans++;
                a += k + 1;
            }
            else
            {
                break;
            }
        }
        cout << ans << endl;
    }
}