#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k;
        string s;
        cin >> n >> k >> s;
        int ans = 0;
        int accumz = 0;
        for (int i = 0; i < n; i++)
        {
            bool ok = false;

            if (s[i] == '0')
            {
                ok = true;

                for (int j = 1; j <= k; j++)
                {
                    if (i - j >= 0)
                    {
                        if (s[i - j] == '1')
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (i + j < n)
                    {
                        if (s[j + i] == '1')
                        {
                            ok = false;
                            break;
                        }
                    }
                }
            }
            if (ok)
            {
                ans++;
                s[i] = '1';
                i += k;
            }
        }
        cout << ans << endl;
    }
}