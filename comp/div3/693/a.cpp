#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int w, h, n;
        cin >> w >> h >> n;
        int a = 1;

        while (w % 2 == 0)
        {
            w /= 2;
            a *= 2;
        }
        while (h % 2 == 0)
        {
            h /= 2;
            a *= 2;
        }
        if (a >= n)
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
    }
}
