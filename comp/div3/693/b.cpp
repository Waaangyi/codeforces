#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(2, 0);
        for (int i = 0; i < n; ++i)
        {
            int a;
            cin >> a;
            ++nlist[a - 1];
        }
        int sum = nlist[0] + nlist[1] * 2;
        if (sum % 2 == 1)
        {
            cout << "NO" << endl;
            continue;
        }

        int half = sum / 2;
        if (half % 2 == 0)
        {
            cout << "YES" << endl;
        }
        else
        {
            if (nlist[0] != 0)
            {
                cout << "YES" << endl;
            }
            else
                cout << "NO" << endl;
        }
    }
}
