#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<ll> olist;
        vector<ll> elist;
        for (ll i = 0; i < n; ++i)
        {
            ll a;
            cin >> a;
            if (a % 2 == 0)
            {
                elist.push_back(a);
            }
            else
                olist.push_back(a);
        }

        sort(elist.begin(), elist.end());
        sort(olist.begin(), olist.end());
        ll alice = 0, bob = 0;
        bool aturn = 0;

        while (elist.size() != 0 || olist.size() != 0)
        {
            aturn = !aturn;
            if (aturn)
            {
                if (olist.size() && elist.size())
                {
                    if (olist.back() > elist.back())
                    {
                        olist.pop_back();
                    }
                    else
                    {
                        alice += elist.back();
                        elist.pop_back();
                    }
                }
                else
                {
                    if (elist.size() == 0)
                    {
                        olist.pop_back();
                        continue;
                    }
                    if (olist.size() == 0)
                    {
                        alice += elist.back();
                        elist.pop_back();
                        continue;
                    }
                }
            }
            else
            {
                if (olist.size() && elist.size())
                {
                    if (olist.back() < elist.back())
                        elist.pop_back();
                    else
                    {
                        bob += olist.back();
                        olist.pop_back();
                    }
                }
                else
                {
                    if (olist.size() == 0)
                    {
                        elist.pop_back();
                        continue;
                    }
                    if (elist.size() == 0)
                    {
                        bob += olist.back();
                        olist.pop_back();
                        continue;
                    }
                }
            }
        }
        /*
        while (elist.size() != 0 || olist.size() != 0)
        {
            aturn = !aturn;
            if (elist.size() != 0 && olist.size() != 0)
            {
                if (aturn)
                {
                    alice += elist.back();
                    elist.pop_back();
                }
                else
                {
                    bob += olist.back();
                    olist.pop_back();
                }
            }
            else
            {
                if (elist.size() == 0)
                {
                    if (aturn)
                    {
                        olist.pop_back();
                    }
                    else
                    {
                        bob += olist.back();
                        olist.pop_back();
                    }
                }
                else
                {
                    if (olist.size() == 0)
                    {
                        if (aturn)
                        {
                            alice += elist.back();
                            elist.pop_back();
                        }
                        else
                            elist.pop_back();
                    }
                }
            }
        }
        */
        if (alice == bob)
        {
            cout << "Tie" << endl;
        }
        if (alice > bob)
        {
            cout << "Alice" << endl;
        }
        if (bob > alice)
        {
            cout << "Bob" << endl;
        }
    }
}
