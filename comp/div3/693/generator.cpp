#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    cout << 1 << endl << 200000 << endl;
    for (int i = 0; i < 200000; ++i)
    {
        cout << 1 << " " << 1 + i << endl;
    }
}
