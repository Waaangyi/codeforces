#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(n);
        vector<int> remainder(3, 0);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
            ++remainder[nlist[i] % 3];
        }
        int target = n / 3;
        int cnt = 0;
        while (remainder[0] != remainder[1] || remainder[0] != remainder[2])
        {

            if (remainder[0] > target)
            {
                int move = remainder[0] - target;
                remainder[0] = target;
                cnt += move;
                remainder[1] += move;
            }
            if (remainder[1] > target)
            {
                int move = remainder[1] - target;
                remainder[1] = target;
                cnt += move;
                remainder[2] += move;
            }
            if (remainder[2] > target)
            {
                int move = remainder[2] - target;
                remainder[2] = target;
                cnt += move;
                remainder[0] += move;
            }
        }
        cout << cnt << endl;
    }
}
