#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        ll n;
        cin >> n;
        ll i = 1;
        bool ok = 0;
        while (true)
        {
            ll a = pow(i, 3);
            if (a >= n)
                break;
            ll b = cbrt(n - a);
            if (n - a == pow(b, 3))
            {
                ok = 1;
                break;
            }
            ++i;
        }
        if (ok)
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}
