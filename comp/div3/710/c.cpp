#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        string a, b;
        cin >> a >> b;

        int longestcommon = 0;
        for (int i = 0; i < a.size(); ++i) {
            for (int j = 1; j <= a.size() - i; ++j) {
                string match = a.substr(i, j);
                if (b.find(match) != string::npos) {
                    longestcommon = max(longestcommon, (int)match.size());
                }
            }
        }
        cout << a.size() + b.size() - longestcommon * 2 << endl;
    }
}
