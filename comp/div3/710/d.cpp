#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
        }
        sort(nlist.begin(), nlist.end());
        vector<pair<int, int>> compress;

        compress.push_back({nlist.front(), 1});
        for (int i = 1; i < n; ++i) {
            if (compress.back().first == nlist[i])
                ++compress.back().second;
            else
                compress.push_back({nlist[i], 1});
        }
        set<pair<int, int>> compressRanked;
        for (int i = 0; i < compress.size(); ++i) {
            compressRanked.insert({compress[i].second, compress[i].first});
        }
        set<int> nonzeroindex;
        for (int i = 0; i < compress.size(); ++i) {
            nonzeroindex.insert(i);
        }
        /*
        while (nonzeroindex.size() > 1) {
            int index1 = *nonzeroindex.begin();
            int index2 = *(++nonzeroindex.begin());
            int maxtake = min(compress[index1].second, compress[index2].second);
            compress[index1].second -= maxtake;
            compress[index2].second -= maxtake;
            if (compress[index2].second == 0) {
                nonzeroindex.erase(index2);
            }
            if (compress[index1].second == 0) {
                nonzeroindex.erase(index1);
            }
        }
        */
        /*
        while (nonzeroindex.size() > 1) {
            int index1 = *nonzeroindex.begin();
            set<int>::iterator it = nonzeroindex.begin();
            ++it;
            vector<int> removeindex;
            while (compress[index1].second != 0) {
                if (it == nonzeroindex.end()) {
                    break;
                }
                int index2 = *it;
                if (compress[index2].second != 0) {
                    --compress[index2].second;
                    --compress[index1].second;
                }
                if (compress[index2].second == 0) {
                    removeindex.push_back(index2);
                }
                ++it;
            }
            for (int i = 0; i < removeindex.size(); ++i) {
                nonzeroindex.erase(removeindex[i]);
            }
            if (compress[index1].second == 0) nonzeroindex.erase(index1);
        }
        */

        while (true) {
            if (compressRanked.size() == 0) {
                cout << 0 << endl;
                break;
            }
            pair<int, int> e = *compressRanked.begin();
            pair<int, int> f = *compressRanked.rbegin();
            if (compressRanked.size() <= 1) {
                if (compressRanked.size() == 0)
                    cout << 0 << endl;
                else
                    cout << f.first << endl;
                break;
            }
            if (f.first == e.first && f.first != 1) {
                cout << 0 << endl;
                break;
            }
            compressRanked.erase(f);
            compressRanked.erase(e);
            --f.first;
            --e.first;
            if (e.first != 0) {
                compressRanked.insert(e);
            }
            if (f.first != 0) {
                compressRanked.insert(f);
            }
        }
        /*
        int ans = 0;
        for (int i = 0; i < compress.size(); ++i) {
            ans += compress[i].second;
        }
        cout << ans << endl;
        */
    }
}
