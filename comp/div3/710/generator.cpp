#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout << 1 << " " << (int)2e5 << endl;
    for (int i = 0; i < 2e5; ++i) {
        cout << i + 1 << " ";
    }
}
