#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;

        vector<int> nlist(n);
        vector<int> numcnt(105, 0);
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
            ++numcnt[nlist[i]];
        }
        for (int i = 0; i < n; ++i) {
            if (numcnt[nlist[i]] == 1) {
                cout << i + 1 << endl;
                break;
            }
        }
    }
}
