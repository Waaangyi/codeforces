#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<vector<bool>> board(n, vector<bool>(n, 0));
        vector<pair<int, int>> loc(2);
        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                char c;
                cin >> c;
                board[i][j] = (c == '*');
                if (c == '*') {
                    loc[cnt].first = i;
                    loc[cnt].second = j;
                    ++cnt;
                }
            }
        }
        // same row
        if (loc.front().first == loc.back().first) {
            if (loc.front().first == 0) {
                // down
                board[loc.front().first + 1][loc.front().second] = 1;
                board[loc.back().first + 1][loc.back().second] = 1;
            } else {
                // up
                board[loc.front().first - 1][loc.front().second] = 1;
                board[loc.back().first - 1][loc.back().second] = 1;
            }
        } else {
            // same col
            if (loc.front().second == loc.back().second) {
                if (loc.front().second == 0) {
                    // right
                    board[loc.front().first][loc.front().second + 1] = 1;
                    board[loc.back().first][loc.back().second + 1] = 1;
                } else {
                    // left
                    board[loc.front().first][loc.front().second - 1] = 1;
                    board[loc.back().first][loc.back().second - 1] = 1;
                }
            } else {
                board[loc.front().first][loc.back().second] = 1;
                board[loc.back().first][loc.front().second] = 1;
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (board[i][j]) {
                    cout << '*';
                } else {
                    cout << '.';
                }
            }
            cout << endl;
        }
    }
}
