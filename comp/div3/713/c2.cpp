
#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int a, b;
        cin >> a >> b;
        string s;
        cin >> s;
        int n = s.size();
        bool ok = 1;
        for (int i = 0; i < n / 2; ++i) {
            int o = n - i - 1;
            int cnt = (s[i] == '?') + (s[o] == '?');
            if (cnt == 0 && s[i] != s[o]) {
                ok = 0;
                break;
            }
            if (cnt == 1) {
                if (s[i] == '?') {
                    s[i] = s[o];
                } else
                    s[o] = s[i];
            }
        }
        if (!ok) {
            cout << -1 << endl;
            continue;
        }
        for (int i = 0; i < n; ++i) {
            if (s[i] == '0') --a;
            if (s[i] == '1') --b;
        }
        if (a < 0 || b < 0) {
            cout << -1 << endl;
            continue;
        }

        for (int i = 0; i < n / 2; ++i) {
            int o = n - i - 1;
            int cnt = (s[i] == '?') + (s[o] == '?');
            if (cnt == 2) {
                if (a >= 2) {
                    a -= 2;
                    s[i] = '0';
                    s[o] = '0';
                    continue;
                }
                if (b >= 2) {
                    b -= 2;
                    s[i] = '1';
                    s[o] = '1';
                    continue;
                }
                ok = 0;
                break;
            }
        }

        if (n % 2) {
            if (s[n / 2] == '?') {
                if (a == 0 && b == 0)
                    ok = 0;
                else {
                    if (a >= 1) {
                        --a;
                        s[n / 2] = '0';
                    } else {
                        if (b >= 1) {
                            --b;
                            s[n / 2] = '1';
                        }
                    }
                }
            }
        }
        if (a != 0 || b != 0) {
            ok = 0;
        }
        if (ok) {
            cout << s << endl;
        } else {
            cout << -1 << endl;
        }
    }
}
