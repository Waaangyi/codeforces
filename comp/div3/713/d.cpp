#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        vector<int> nlist(n + 2);
        map<int, int> lookup;
        for (int i = 0; i < n + 2; ++i) {
            cin >> nlist[i];
            ++lookup[nlist[i]];
        }
        sort(nlist.begin(), nlist.end());

        int sum1 = 0, sum2 = 0;
        for (int i = 0; i < n; ++i) {
            sum1 += nlist[i];
        }
        for (int i = 0; i < n + 1; ++i) {
            sum2 += nlist[i];
        }
        if (sum1 == nlist[n]) {
            for (int i = 0; i < n; ++i) {
                cout << nlist[i] << " ";
            }
            cout << endl;
            continue;
        }
        int leftover = sum2 - nlist.back();
        if (leftover <= 0) {
            cout << -1 << endl;
            continue;
        }
        if (lookup[leftover] - (leftover == nlist.back()) <= 0) {
            cout << -1 << endl;
            continue;
        }

        int remove = leftover;
        for (int i = 0; i < n + 1; ++i) {
            if (nlist[i] == remove) {
                remove = 0;
            } else {
                cout << nlist[i] << " ";
            }
        }
        cout << endl;
    }
}
