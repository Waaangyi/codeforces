#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        string s;
        cin >> s;

        bool seen[26];
        for (int i = 0; i < 26; ++i) {
            seen[i] = 0;
        }

        string unique = s.substr(0, 1);
        for (int i = 0; i < s.size(); ++i) {
            if (unique.back() != s[i]) unique += s[i];
        }
        bool ok = 1;
        for (int i = 0; i < unique.size(); ++i) {
            if (seen[unique[i] - 'A'] == 1) {
                ok = 0;
                break;
            }
            seen[unique[i] - 'A'] = 1;
        }
        if (ok) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
}
