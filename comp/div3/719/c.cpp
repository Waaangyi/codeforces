#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;

        if (n == 1) {
            cout << 1 << endl;
            continue;
        }
        if (n == 2) {
            cout << -1 << endl;
            continue;
        }
        vector<vector<int>> board(n, vector<int>(n, 0));

        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            board[i][i] = ++cnt;
        }
        for (int i = 1; i < n - 1; ++i) {
            for (int j = 0; j + i < n; ++j) {
                board[j + i][j] = ++cnt;
            }
        }
        for (int i = 1; i < n - 1; ++i) {
            for (int j = 0; j + i < n; ++j) {
                board[j][j + i] = ++cnt;
            }
        }
        board[n - 1][0] = ++cnt;
        board[0][n - 1] = ++cnt;
        for (auto i : board) {
            for (auto j : i) {
                cout << j << " ";
            }
            cout << endl;
        }
    }
}
