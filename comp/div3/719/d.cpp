#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--) {
        ll n;
        cin >> n;
        vector<ll> nlist(n);
        for (ll i = 0; i < n; ++i) {
            cin >> nlist[i];
        }

        ll ans = 0;
        // worst contest problem ever
        // literally copied word for word from geeks for geeks
        map<ll, ll> nmap;
        for (ll i = 0; i < n; ++i) {
            ++nmap[nlist[i] - i];
        }
        for (auto i : nmap) {
            ll cnt = i.second;
            ans += ((cnt * (cnt - 1)) / 2);
        }
        cout << ans << endl;
    }
}

