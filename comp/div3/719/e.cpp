#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--) {
        ll n;
        cin >> n;
        string s;
        cin >> s;

        vector<pair<bool, ll>> compress;
        compress.push_back({s[0] == '*', 0});
        for (ll i = 0; i < n; ++i) {
            if (compress.back().first == (s[i] == '*'))
                ++compress.back().second;
            else
                compress.push_back({s[i] == '*', 1});
        }
        if (compress.front().first == 0) {
            compress.erase(compress.begin());
        }
        vector<ll> slist;
        for (ll i = 0; i < compress.size(); ++i) {
            if (compress[i].first == 0) {
                slist.push_back(compress[i].second);
            }
        }

        ll before = 0;
        ll after = 0;
        ll space = 0;
        for (ll i = 1; i < compress.size(); ++i) {
            if (compress[i].first) {
                space += compress[i - 1].second;
                after += compress[i].second * space;
            }
        }
        ll ans = after;
        ll beforesheep = 0;
        ll aftersheep = 0;
        ll beforespace = 0;
        ll afterspace = space;
        for (ll i = 0; i < compress.size(); ++i) {
            aftersheep += compress[i].second * compress[i].first;
        }
        for (ll i = 1; i < compress.size(); ++i) {
            if (compress[i].first) {
                before = beforesheep * compress[i - 1].second + before;
                after -= compress[i].second * compress[i - 1].second;
                after = after - (aftersheep - compress[i].second) *
                                    compress[i - 1].second;
                ans = min(ans, after + before);
            } else {
                beforespace += compress[i].second;
                afterspace -= compress[i].second;
                beforesheep += compress[i - 1].second;
                aftersheep -= compress[i - 1].second;
            }
        }
        cout << ans << endl;
    }
}
