#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    while (true)
    {
        string s;
        getline(cin, s);
        if (s.size() == 0)
            break;

        cout << "NO\n" << flush;
    }
}
