#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    string s;
    cin >> s;

    if (s.size() < 3)
    {
        cout << "YES" << endl;
    }
    else
    {
        for (int i = 2; i < s.size(); ++i)
        {
            int sum = s[i - 2] - 'A' + s[i - 1] - 'A';
            sum %= 26;
            if (sum != s[i] - 'A')
            {
                cout << "NO" << endl;
                return 0;
            }
        }
        cout << "YES" << endl;
    }
}
