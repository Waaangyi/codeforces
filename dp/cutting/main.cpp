#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, b;
    cin >> n >> b;

    vector<int> nlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i];
    }

    vector<int> price;
    int odd = 0;
    int even = 0;
    for (int i = 0; i < n; ++i)
    {
        if (nlist[i] % 2)
            ++odd;
        else
            ++even;

        if (odd == even && i != n - 1)
        {
            price.push_back(abs(nlist[i] - nlist[i + 1]));
            odd = even = 0;
        }
    }
    sort(price.begin(), price.end());
    int ans = 0;
    for (int i = 0; i < price.size(); ++i)
    {
        if (b - price[i] >= 0)
        {
            b -= price[i];
            ans++;
        }
        else
            break;
    }
    cout << ans << endl;
}
