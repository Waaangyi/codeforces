#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, k;
    cin >> n >> k;

    vector<int> tlist(n), mlist(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> tlist[i];
    }
    int wakesum = 0;
    for (int i = 0; i < n; ++i)
    {
        cin >> mlist[i];
        if (mlist[i])
            wakesum += tlist[i];
    }
    vector<int> dp;
    int sum = 0;
    for (int i = 0; i < k; ++i)
    {
        if (mlist[i] == 0)
            sum += tlist[i];
    }
    dp.push_back(sum);
    for (int i = 1; i <= n - k; ++i)
    {
        if (mlist[i - 1] == 0)
            sum -= tlist[i - 1];
        if (mlist[i + k - 1] == 0)
            sum += tlist[i + k - 1];
        dp.push_back(sum);
    }
    sort(dp.rbegin(), dp.rend());
    cout << wakesum + dp.front();
}
