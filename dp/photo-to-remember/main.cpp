#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int sum = 0;
    vector<pair<int, int>> nlist(n);
    vector<pair<int, int>> height(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> nlist[i].first >> nlist[i].second;
        height[i].first = nlist[i].second;
        height[i].second = i;
        sum += nlist[i].first;
    }
    sort(height.rbegin(), height.rend());
    for (int i = 0; i < n; ++i)
    {
        int h = 0;
        if (height.front().second == i)
        {
            h = height[1].first;
        }
        else
            h = height.front().first;

        cout << (sum - nlist[i].first) * h << " ";
    }
}
