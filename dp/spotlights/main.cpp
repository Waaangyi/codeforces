#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n, m;
    cin >> n >> m;

    vector<vector<int>> board;
    for (int i = 0; i < n; ++i)
    {
        vector<int> line(m, 0);
        for (int j = 0; j < m; ++j)
        {
            cin >> line[j];
        }
        board.push_back(line);
    }
    vector<bool> hsnapshot;
    vector<pair<int, int>> hdetail;
    for (int i = 0; i < n; ++i)
    {
        int ma = -69;
        int mi = INT_MAX;
        for (int j = 0; j < m; ++j)
        {
            if (board[i][j])
            {
                mi = min(j, mi);
                ma = max(j, ma);
            }
        }
        if (ma == -69)
        {
            hsnapshot.push_back(0);
            hdetail.push_back(make_pair(-69, -420));
        }
        else
        {
            hsnapshot.push_back(1);
            hdetail.push_back(make_pair(mi, ma));
        }
    }
    vector<bool> vsnapshot;
    vector<pair<int, int>> vdetail;
    for (int j = 0; j < m; ++j)
    {
        int ma = -69;
        int mi = INT_MAX;
        for (int i = 0; i < n; ++i)
        {
            if (board[i][j])
            {
                mi = min(i, mi);
                ma = max(i, ma);
            }
        }
        if (ma == -69)
        {
            vsnapshot.push_back(0);
            vdetail.push_back(make_pair(-69, -420));
        }
        else
        {
            vsnapshot.push_back(1);
            vdetail.push_back(make_pair(mi, ma));
        }
    }
    int ans = 0;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            if (board[i][j] == 1)
                continue;
            if (hsnapshot[i])
            {
                int mi = hdetail[i].first;
                int ma = hdetail[i].second;
                if (j < ma)
                    ++ans;
                if (j > mi)
                    ++ans;
            }
        }
    }
    for (int j = 0; j < m; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            if (board[i][j] == 1)
                continue;
            if (vsnapshot[j])
            {
                // cout << j << endl;
                int mi = vdetail[j].first;
                int ma = vdetail[j].second;
                if (i < ma)
                    ++ans;
                if (i > mi)
                    ++ans;
            }
        }
    }

    cout << ans;
}
