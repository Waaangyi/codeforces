#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int n;
    cin >> n;
    int ans = 0;
    while (n != 0)
    {
        if (n < 10)
        {
            ++ans;
            break;
        }

        string s = to_string(n);
        int sub = 0;
        for (int i = 0; i < s.length(); ++i)
        {
            sub = max(sub, s[i] - '0');
        }
        n -= sub;
        ans++;
    }
    cout << ans << endl;
}
