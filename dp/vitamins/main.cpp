#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    ll tri = INT_MAX;
    ll ab = INT_MAX;
    ll bc = INT_MAX;
    ll ac = INT_MAX;
    ll A = INT_MAX;
    ll B = INT_MAX;
    ll c = INT_MAX;
    for (ll i = 0; i < n; ++i)
    {
        ll a;
        string b;
        cin >> a >> b;
        if (b.size() == 3)
            tri = min(tri, a);
        else
        {
            if (b.size() == 2)
            {
                sort(b.begin(), b.end());
                if (b == "AB")
                {
                    ab = min(a, ab);
                }
                if (b == "AC")
                {
                    ac = min(a, ac);
                }
                if (b == "BC")
                {
                    bc = min(a, bc);
                }
            }
            else
            {
                if (b == "A")
                    A = min(a, A);
                if (b == "B")
                    B = min(a, B);
                if (b == "C")
                    c = min(a, c);
            }
        }
    }
    ll duo = min({ab + bc, ac + bc, ab + ac, ab + c, ac + B, bc + A});
    if (min({duo, A + B + c, tri}) >= INT_MAX)
    {
        cout << -1 << endl;
        return 0;
    }
    cout << min({duo, A + B + c, tri});
}
