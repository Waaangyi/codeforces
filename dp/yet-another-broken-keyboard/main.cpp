#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n, k;
    cin >> n >> k;

    string s;
    cin >> s;

    vector<bool> good(26, 0);
    while (k--)
    {
        char c;
        cin >> c;
        good[c - 'a'] = 1;
    }
    vector<ll> segments;
    for (ll i = 0; i < n; ++i)
    {
        if (good[s[i] - 'a'])
        {
            segments.push_back(1);
            ++i;
            if (i == n)
                break;
            while (good[s[i] - 'a'])
            {
                segments.back()++;
                ++i;
                if (i == n)
                    break;
            }
            --i;
        }
    }
    ll ans = 0;
    for (auto i : segments)
    {
        ans += i * (i + 1) / 2;
    }
    cout << ans;
}
