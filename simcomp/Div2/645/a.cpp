#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, m;
        cin >> n >> m;

        int ans = n * (m / 2);
        if (m % 2 == 1)
        {
            ans += (n + 1) / 2;
        }
        cout << ans << endl;
    }
}
