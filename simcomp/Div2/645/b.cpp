#include <bits/stdc++.h>
#include <iostream>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; i++)
        {
            cin >> nlist[i];
        }
        sort(nlist.begin(), nlist.end());
        int ans = 1;
        int hold = 0;
        for (int i = 0; i < n; i++)
        {
            if (nlist[i] <= ans + hold)
            {
                ans += hold;
                hold = 0;
                ans++;
            }
            else
                hold++;
        }
        cout << ans << endl;
    }
}
