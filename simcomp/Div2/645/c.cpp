#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;
    while (tc--)
    {
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;

        if (x1 == x2 || y1 == y2)
        {
            cout << 1 << endl;
            continue;
        }

        ll ans = 0;

        vector<int> test = {1, 2, 2, 2};
        do
        {
            for (auto i : test)
            {
                cout << i << " ";
            }
            cout << endl;
        } while (next_permutation(test.begin(), test.end()));
    }
}
