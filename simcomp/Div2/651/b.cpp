#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        vector<int> nlist(2 * n);
        int even = 0;
        int odd = 0;
        for (int i = 0; i < 2 * n; i++)
        {
            cin >> nlist[i];
            if (nlist[i] % 2 == 0)
                even++;
            else
            {
                odd++;
            }
        }
        if (even >= (n - 1) * 2)
        {
            int c = 0;
            int a = 0;
            int l = 0;
            while (c < n - 1)
            {
                if (nlist[a] % 2 == 0)
                {
                    if (l != 0)
                    {
                        cout << l << " " << a + 1 << endl;
                        l = 0;
                        c++;
                    }
                    else
                    {
                        l = a + 1;
                    }
                }
                a++;
            }
        }
        else
        {
            int c = 0, a = 0, l = 0;
            while (c < even / 2)
            {
                if (nlist[a] % 2 == 0)
                {
                    if (l != 0)
                    {
                        cout << l << " " << a + 1 << endl;
                        l = 0;
                        c++;
                    }
                    else
                    {
                        l = a + 1;
                    }
                }
                a++;
            }
            c = 0;
            a = 0;
            l = 0;
            while (c < n - even / 2 - 1)
            {
                if (nlist[a] % 2 != 0)
                {
                    if (l != 0)
                    {
                        cout << l << " " << a + 1 << endl;
                        l = 0;
                        c++;
                    }
                    else
                    {
                        l = a + 1;
                    }
                }
                a++;
            }
        }
    }
}