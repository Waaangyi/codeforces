#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int n;
        cin >> n;
        if (n <= 2)
        {
            cout << 1 << endl;
            continue;
        }
        if (n % 2 == 0)
        {
            cout << n / 2 << endl;
        }
        else
            cout << (n + 1) / 2 << endl;
    }
}
