#include <bits/stdc++.h>

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int tc;
    cin >> tc;
    while (tc--)
    {
        vector<vector<int>> ans;
        int n, k;
        cin >> n >> k;
        vector<int> line(n, 0);
        for (int i = 0; i < n; i++)
        {
            ans.push_back(line);
        }
        bool flag = 1;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (flag)
                {
                    ans[i][j] = 1;
                }
                else
                    ans[i][j] = 0;
                flag = !flag;
            }
        }
    }
}
