#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;

        vector<int> nlist;

        for (int i = 0; i < n; ++i) {
            char c;
            cin >> c;

            nlist.push_back(c - '0');
        }
        if (nlist.size() % 2 == 0) {
            int ans = 1;
            int even, odd;
            even = odd = 0;
            int maxMove = n / 2 - 1;
            for (int i = 0; i < n; ++i) {
                if ((i + 1) % 2 == 0) {
                    if (nlist[i] % 2)
                        ++odd;
                    else
                        ++even;
                }
            }
            if (maxMove >= odd) {
                ans = 2;
            }
            cout << ans << endl;
        } else {
            int maxMove = n / 2;
            int even, odd;
            even = odd = 0;
            for (int i = 0; i < n; ++i) {
                if ((i + 1) % 2) {
                    if (nlist[i] % 2)
                        ++odd;
                    else
                        ++even;
                }
            }
            int ans = 2;
            if (even <= maxMove) {
                ans = 1;
            }
            cout << ans << endl;
        }
    }
}
