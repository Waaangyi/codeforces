#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        ll x;
        cin >> x;

        int ans = 0;
        for (int i = 1; i < 60; ++i) {
            ll levelcnt = pow(2, i) - 1;
            ll blockcnt = levelcnt * (levelcnt + 1) / 2;
            if (x >= blockcnt) {
                ++ans;
                x -= blockcnt;
            } else
                break;
        }
        cout << ans << endl;
    }
}
