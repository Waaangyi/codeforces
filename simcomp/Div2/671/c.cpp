#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n, x;
        cin >> n >> x;

        vector<int> nlist(n);
        int infected = 0;
        double average = 0;
        for (int i = 0; i < n; ++i) {
            cin >> nlist[i];
            if (nlist[i] == x) {
                ++infected;
            }
            average += nlist[i];
        }
        average /= n;
        if (infected == n) {
            cout << 0 << endl;
        } else {
            if (infected > 0) {
                cout << 1 << endl;
            } else if (average == x) {
                cout << 1 << endl;
            } else
                cout << 2 << endl;
        }
    }
}
