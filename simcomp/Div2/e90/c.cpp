#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        ll temp = 0;
        ll ans = s.length();
        ll req = 0;
        for (ll i = 0; i < s.length(); ++i)
        {
            if (s[i] == '+')
            {
                temp++;
            }
            else
            {
                temp--;
            }
            if (temp < req)
            {
                req = temp;
                ans += i + 1;
            }
        }
        cout << ans << endl;
    }
}
