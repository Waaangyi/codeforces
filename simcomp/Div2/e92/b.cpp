#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n, k, z;
        cin >> n >> k >> z;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }
        vector<int> anslist;
        anslist.push_back(nlist.front());
        int sum = nlist.front();
        multiset<int> slist;
        slist.insert(nlist.front());
        for (int i = 1; i <= k; ++i)
        {
            anslist.push_back(nlist[i]);
            slist.insert(nlist.front());
            sum += nlist[i];
        }
        int sub = anslist.size() - 1;
        for (int i = 0; i < anslist.size(); ++i)
        {
            if (z == 0)
            {
                break;
            }
            else
            {
                if (sum - anslist[sub] + anslist[i] > sum)
                {
                    z--;
                }
            }
        }
    }
}
