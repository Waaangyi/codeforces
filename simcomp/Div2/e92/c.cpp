#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        vector<int> onelist(10, 0);
        for (int i = 0; i < s.size(); ++i)
        {
            ++onelist[s[i] - '0'];
        }
        int ans1 = s.length() - 2;
        for (int i = 0; i <= 9; ++i)
        {
            ans1 = min(ans1, (int)s.length() - onelist[i]);
        }
        if (ans1 == 0)
        {
            cout << ans1 << endl;
            continue;
        }
        int ans = 2e9;
        for (int i = 0; i <= 9; ++i)
        {
            for (int j = 0; j <= 9; ++j)
            {
                if (i == j)
                {
                    continue;
                }
                int match = 0;
                for (int k = 0; k < s.length(); ++k)
                {
                    if (s[k] - '0' == i)
                    {
                        match++;
                        while (s[k] - '0' != j && k < s.length())
                        {
                            k++;
                        }
                        if (k >= s.length() || s[k] - '0' != j)
                        {
                            match--;
                            break;
                        }
                        else
                        {
                            match++;
                        }
                    }
                }
                // cout << match << " " << i << j << endl;
                ans = max(min(ans, (int)s.length() - match), 0);
            }
        }
        cout << min(ans, ans1) << endl;
    }
}
