#include "bits/stdc++.h"

#define ll long long

using namespace std;
ll MOD = 1e9 + 7;
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--) {
        int n;
        cin >> n;
        set<int> conflicts;
        // conflicts.clear();
        vector<vector<int>> board(2, vector<int>(n));
        vector<int> board1(n + 1);
        vector<int> board2(n + 1);
        for (int i = 0; i < n; ++i) {
            cin >> board[0][i];
            board1[board[0][i]] = i;
        }
        for (int i = 0; i < n; ++i) {
            cin >> board[1][i];
            board2[board[1][i]] = i;
        }
        ll cnt = 0;
        for (int i = 0; i < n; ++i) {
            if (conflicts.count(i)) {
                continue;
            }
            int a, b;
            a = board[0][i];
            b = board[1][i];
            if (board1[b] == board2[a]) {
                ++cnt;
                assert(conflicts.insert(i).second == true);
                assert(conflicts.insert(board1[b]).second == true);
            } else {
                // update.clear();
                // cycle(board, board1, board2, i);
                vector<int> update;
                int temp = i;
                while (true) {
                    if (update.size() != 0 && update.front() == temp) {
                        break;
                    }
                    swap(board[0][temp], board[1][temp]);
                    update.push_back(temp);
                    temp = board1[board[0][temp]];
                }
                for (int i = 0; i < update.size(); ++i) {
                    assert(conflicts.insert(update[i]).second == true);
                    board1[board[0][update[i]]] = update[i];
                    board2[board[1][update[i]]] = update[i];
                }
                ++cnt;
            }

            if (conflicts.size() == n) {
                break;
            }
        }
        assert(conflicts.size() == n);
        ll ans = 1;
        for (int i = 0; i < cnt; ++i) {
            ans *= 2;
            ans %= MOD;
        }
        cout << ans << endl;
    }
}
