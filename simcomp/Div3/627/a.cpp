#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int len;
        cin >> len;

        int even = 0, odd = 0;
        for (int i = 0; i < len; i++)
        {
            int a;
            cin >> a;
            if (a % 2 == 0)
                even++;
            else
            {
                odd++;
            }
        }
        if (min(even, odd))
        {
            cout << "NO" << endl;
        }
        else
        {
            cout << "YES" << endl;
        }
    }
}