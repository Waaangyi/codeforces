#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        string s;
        cin >> s;
        int maxcontinuous = 0;
        int con = 0;
        for (int i = 0; i < s.length(); i++)
        {
            if (s[i] == 'L')
                con++;
            if (s[i] != 'L' || i == s.length() - 1)
            {
                maxcontinuous = max(maxcontinuous, con);
                con = 0;
            }
        }
        cout << maxcontinuous + 1 << endl;
    }
}