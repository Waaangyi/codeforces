#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    ll n;
    cin >> n;

    vector<pair<ll, ll>> t(n);
    for (ll i = 0; i < n; i++)
    {
        cin >> t[i].first;
    }
    for (ll i = 0; i < n; i++)
    {
        cin >> t[i].second;
    }
    vector<ll> differences(n);
    for (ll i = 0; i < n; i++)
    {
        differences[i] = t[i].first - t[i].second;
    }
    sort(differences.begin(), differences.end());
    ll j = n - 1;
    ll i = 0;
    ll ans = 0;
    while (i < n)
    {
        while (i < j && differences[i] + differences[j] > 0)
            j--;
        ans += n - max(i, j) - 1;
        i++;
    }

    cout << ans << endl;
}