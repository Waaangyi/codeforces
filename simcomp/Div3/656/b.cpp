#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(2 * n);
        for (int i = 0; i < 2 * n; ++i)
        {
            cin >> nlist[i];
        }

        int head = nlist.front();
        int pos = 0;
        vector<int> ans(n);
        ans.front() = head;
        vector<bool> used(55, 0);
        used[head] = 1;
        int full = 1;
        for (int i = 1; i < 2 * n; ++i)
        {
            if (nlist[i] == head)
            {
                pos = i;
            }
        }
        for (int i = 1; i < pos; ++i)
        {
            ans[i] = (nlist[i]);
            used[nlist[i]] = 1;
            ++full;
        }
        int j = 2 * n - 1;
        int p = n - 1;
        while (full != n)
        {
            if (used[nlist[j]] == 0)
            {
                ans[p] = nlist[j];
                used[nlist[j]] = 1;
                ++full;
                --p;
            }
            --j;
        }
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
