#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;

        vector<int> nlist(n);
        for (int i = 0; i < n; ++i)
        {
            cin >> nlist[i];
        }

        if (is_sorted(nlist.begin(), nlist.end()))
        {
            cout << 0 << endl;
            continue;
        }
        if (is_sorted(nlist.rbegin(), nlist.rend()))
        {
            cout << 0 << endl;
            continue;
        }

        int peakpos = n - 1;
        int peak = nlist.back();
        for (int i = peakpos - 1; i > 0; --i)
        {
            if (nlist[i] >= peak)
            {
                peak = nlist[i];
                peakpos = i;
                if (nlist[i - 1] < peak)
                {
                    break;
                }
            }
            else
                break;
        }
        int temp = peak;
        int leftpos = peakpos;
        for (int i = peakpos - 1; i >= 0; --i)
        {
            if (nlist[i] <= temp)
            {
                leftpos--;
                temp = nlist[i];
            }
            else
                break;
        }
        cout << leftpos << endl;
    }
}
