#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        int n;
        cin >> n;
        vector<bool> nlist(n);
        int one = 0, zero = 0;
        for (int i = 0; i < n; ++i)
        {
            char a;
            cin >> a;
            nlist[i] = a - '0';
            one += nlist[i];
            zero += 1 - nlist[i];
        }
        vector<bool> stat(n, 0);

        vector<int> ans(n);
        int temp = 1;
        for (int i = 0; i < n; ++i)
        {
            if (!stat[i])
            {
                bool need = nlist[i];
                stat[i] = 1;
                if (need == 1 && !zero)
                {
                    ans[i] = temp;
                    temp++;
                    one--;
                    continue;
                }
                if (need == 0 && !one)
                {
                    ans[i] = temp;
                    temp++;
                    zero--;
                    continue;
                }
                need = !need;
                ans[i] = temp;
                for (int j = i + 1; j < n; ++j)
                {
                    if (need == 0 && !zero)
                    {
                        break;
                    }
                    if (need == 1 && !one)
                    {
                        break;
                    }
                    if (stat[j] == 0 && nlist[j] == need)
                    {
                        need = !need;
                        stat[j] = 1;
                        ans[j] = temp;
                        zero -= 1 - nlist[j];
                        one -= nlist[j];
                    }
                }
                temp++;
            }
        }
        cout << temp - 1 << endl;
        for (auto i : ans)
        {
            cout << i << " ";
        }
        cout << endl;
    }
}
