#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;

    while (tc--)
    {
        ll n;
        cin >> n;

        bool ok = 0;
        while (n != 1)
        {
            if (n % 2 == 1)
            {
                ok = 1;
            }
            n /= 2;
        }
        if (ok)
        {
            cout << "YES" << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}
