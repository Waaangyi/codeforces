#include "bits/stdc++.h"

#define ll long long

using namespace std;

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);

    int tc;
    cin >> tc;
    while (tc--)
    {
        int a, b, k;
        cin >> a >> b >> k;
        vector<int> alist(k);
        vector<int> blist(k);
        for (int i = 0; i < k; ++i)
        {
            cin >> alist[i];
        }
        for (int i = 0; i < k; ++i)
        {
            cin >> blist[i];
        }
        // boys # + girls # - collisions
        unordered_map<int, int> boys;
        unordered_map<int, int> girls;
        map<pair<int, int>, int> cnt;
        for (int i = 0; i < k; ++i)
        {
            ++boys[alist[i]];
            ++girls[blist[i]];
            ++cnt[{alist[i], blist[i]}];
        }
        ll sum = 0;
        for (int i = 0; i < k; ++i)
        {
            sum += k - (boys[alist[i]] + girls[blist[i]] -
                        cnt[{alist[i], blist[i]}]);
        }
        cout << sum / 2 << endl;
    }
}
