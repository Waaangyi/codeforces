#!/bin/bash

mkdir -p $1
tmux has-session -t=codeforces 2>/dev/null
if [ $? = 0 ]; then
    tmux kill-session -t=codeforces
fi
# create a tmux session named codeforces 
tmux new-session -c $(pwd)/$1 -s codeforces -n nvim -d
tmux split-window -v -t codeforces
tmux resize-pane -t codeforces:0.0 -D 12
tmux send-keys -t codeforces:0.0 "nvim main.cpp" Enter
